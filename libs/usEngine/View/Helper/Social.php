<?php
class usEngine_View_Helper_Social extends Zend_View_Helper_Abstract
{
    protected $_html = '';
    
    public function social($names, $options = null)
    {
        if (is_array($names)) {
            $social = new usEngine_Social_Factory($names, $options);
        } else {
            $class = 'usEngine_Social_' . ucfirst(strtolower($names));
            $social = new $class($options);
        }
        return $social;
    }
}