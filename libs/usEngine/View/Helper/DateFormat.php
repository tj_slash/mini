<?php
class usEngine_View_Helper_DateFormat extends Zend_View_Helper_Abstract
{
    protected $_months =  array(
        'январь', 
        'февраль', 
        'март', 
        'апрель', 
        'май', 
        'июнь',
        'июль', 
        'август', 
        'сентябрь',
        'октябрь', 
        'ноябрь', 
        'декабрь'
    );
    
    protected $_monthr =  array(
        'января', 
        'февраля', 
        'марта', 
        'апреля', 
        'мая', 
        'июня',
        'июля', 
        'августа', 
        'сентября',
        'октября', 
        'ноября', 
        'декабря'
    );
    
    public function getMonths()
    {
        return $this->_months;
    }
    
    public function getMonthr()
    {
        return $this->_monthr;
    }
    
    public function dateFormat($timestamp, $addTime = true)
    {
        $timestamp = strtotime($timestamp);
        $monthr = $this->getMonthr();
        if ($addTime === false) 
            return date('d', $timestamp) . ' ' . $monthr[date('m', $timestamp) - 1];
        return date('d', $timestamp) . ' ' . $monthr[date('m', $timestamp) - 1] . ', ' . date('H', $timestamp) . ':' . date('i', $timestamp);
    }
}