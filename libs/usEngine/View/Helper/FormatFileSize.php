<?php
class usEngine_View_Helper_FormatFileSize extends Zend_View_Helper_Abstract
{
    public function formatFileSize($size, $precision = 2)
    {
        $base = log($size) / log(1024);
        $suffixes = array('б', 'кБ', 'МБ', 'ГБ', 'ТБ'); 
        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[floor($base)];
    }
}