<?php
class usEngine_View_Helper_Sorting extends Zend_View_Helper_Abstract
{
    protected $_html = '';

    protected $_sortUpHtml = '&#8593;';

    protected $_sortDownHtml = '&#8595;';

    /*
     * name - Имя в формировании запроса
     * sort - активное поле сортировки
     * dir - активное направление сортировки
     * page - активная страница
     */
    public function sorting($name, $sort, $dir, $page = 1)
    {
        $this->_html = '';
        $this->_html .= '<a class="sort-up ' . (($sort == $name && $dir == 'ASC') ? 'active' : '') . '" href="?sort=' . $name . '&dir=DESC&page=' . $page . '">' . $this->_sortUpHtml . '</a>';
        $this->_html .= '<a class="sort-down ' . (($sort == $name && $dir == 'DESC') ? 'active' : '') . '" href="?sort=' . $name . '&dir=ASC&page=' . $page . '">' . $this->_sortDownHtml . '</a>';
        return $this->_html . '&nbsp;';
    }
}