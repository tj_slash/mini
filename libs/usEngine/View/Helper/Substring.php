<?php
class usEngine_View_Helper_Substring extends Zend_View_Helper_Abstract
{
    public function substring($string, $lenght = 100)
    {
        $string = trim(strip_tags($string));
        if (strlen($string) < $lenght) return $string;
        if (substr_count($string, " ") > 1) {
            $lenght = strrpos(substr($string, 0, $lenght), ' ');
            return substr($string, 0, $lenght) . '...';
        }
        return $string;
    }
}