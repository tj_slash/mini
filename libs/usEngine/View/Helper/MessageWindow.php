<?php

/**
 * usEngine_View_Helper_MessageWindow
 * 
 * Хелпер для вывода сообщений
 * 
 * @category Inc
 * @package View
 * @subpackage Helper
 * @author Metelkin V. <joc.bro1@gmail.com>
 * 
 */
class usEngine_View_Helper_MessageWindow {

    /**
     * Представление, используемое для вывода объявлений
     * @var Zend_View
     */
    private $_view = null;

    /**
     * Возможные стили сообщений
     */
    protected $styles = array(
        'success' => 'alert-success',
        'error'   => 'alert-error',
        'info'    => 'alert-info'
    );

    public function messageWindow($type = false, $title = false, $message = false) 
    {
        if (!$type) return false;
        
        $this->_initView();
        
        $this->_view->title = $title;
        $this->_view->message = $message;
        if (isset($this->styles[$type]))
            $this->_view->type = $this->styles[$type];
        
        return $this->_renderView();
    }

    /**
     * Выбор папки скриптов для рендера 
     */
    private function _initView() {
        $this->_view = new Zend_View();
        $this->_view->setScriptPath(APPLICATION_PATH . '/modules/index/views/scripts/index/');
    }
    
    /**
     * Рендерит срипт
     * @return html
     */
    private function _renderView() {
        return $this->_view->render('message-window.phtml');
    }

}