<?php
/**
 * usEngine_View_Helper_SeparatePhone
 *
 * Хелпер, чтоб телефон разделять чёрточками
 *
 * @category usEngine
 * @package View
 * @subpackage Helper
 * @author Anisimov A. <anisalex@list.ru>
 **/
class usEngine_View_Helper_SeparatePhone extends Zend_View_Helper_Abstract
{    
    public function separatePhone($phone, $separator = '&ndash;')
    {
        if (strlen($phone) == 5) {
            $phone = substr($phone, 0, 2). '-' .substr($phone, -3);
            $phone = substr($phone, 0, 4). '-' .substr($phone, -2);
            $phone = str_replace('-', $separator, $phone);
        } elseif (strlen($phone) == 6) {
            $phone = substr($phone, 0, 2). '-' .substr($phone, -4);
            $phone = substr($phone, 0, 5). '-' .substr($phone, -2);
            $phone = str_replace('-', $separator, $phone);
        } elseif (strlen($phone) == 7) {
            $phone = substr($phone, 0, 3). '-' .substr($phone, -4);
            $phone = substr($phone, 0, 6). '-' .substr($phone, -2);
            $phone = str_replace('-', $separator, $phone);
        } elseif (strlen($phone) == 11) {
            $phone = substr($phone, 0, 1). '-' .substr($phone, -10);
            $phone = substr($phone, 0, 5). '-' .substr($phone, -7);
            $phone = substr($phone, 0, 9). '-' .substr($phone, -4);
            $phone = substr($phone, 0, 12). '-' .substr($phone, -2);
            $phone = str_replace('-', $separator, $phone);
        }

        return $phone;
    }
}
