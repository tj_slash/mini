<?php
class usEngine_View_Helper_Price extends Zend_View_Helper_Abstract
{
    public function price($price)
    {
        return number_format((float)$price, 2, '.', ' ');
    }
}