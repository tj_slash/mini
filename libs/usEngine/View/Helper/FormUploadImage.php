<?php
require_once 'Zend/View/Helper/FormElement.php';
/**
 * FormUploadImage form element
 *
 * @category   usEngine
 * @package    usEngine_View
 * @subpackage Helper
 * 
 * @author Vakylenko A. <vakylenkox@gmail.com>
 */
class usEngine_View_Helper_FormUploadImage extends Zend_View_Helper_FormElement
{
    /**
     * @param string|array $name Название элемента для параметра "name" тэга <input />
     * @param mixed $value Выбраное значение  по-умолчанию
     * @param array|string $attribs Html атрибуты для картинок
     * @param array $options массив содержащий значение и путь для каждой картинки
     * 
     * @return string html
     */
    public function formUploadImage($name, $value = null, $attribs = null, $options = null)
    {
        $info = $this->_getInfo($name, $value, $attribs, $options);
        extract($info); // name, value, attribs, options
        
        $options = (array) $options;
        
        if (empty($attribs['url'])) throw new Exception('Необходимо указать path!');
        if (empty($attribs['path'])) throw new Exception('Необходимо указать path!');
        
        if(!file_exists($attribs['path'])) {
            @mkdir($attribs['path'], 0777, true);
        }
        
        if ($value) {
            $image = '<img src="' . $value . '" id="' . $id . '-image" ' . $this->_htmlAttribs($attribs) . ' />';
        } else {
            $image = '<img src="" id="' . $id . '-image" ' . $this->_htmlAttribs($attribs) . '/>';
        }
        
        $xhtml = '';
        $xhtml .= 
<<<HTML
<div class="profileSetting">
    <div class="avartar" style="border:2px solid rgb(194, 227, 240);margin:0;text-align:center">
        $image
    </div>
    <div class="avartar" style="margin:0">
        <input type="file" name="$id-browse" id="$id-browse" class="fileupload">
        <p align="center"><a href="javascript::void();" id="$id-reset">Удалить</a></p>
        <input type="hidden" id="$id" name="$name" value="$value" />
    </div>
</div>        
<script type="text/javascript">
$(function(){
    $("#$id-browse").on("change", function(e) {
        e.preventDefault();
        var reader, name = this.files[0].name;
        try {
            reader = new FileReader();
        } catch (error) {
            return false;
        }
        reader.onload = function(e) {
            $('#$id').val(e.target.result);
            $("#$id-image").show().attr("src", e.target.result);
        };
        reader.readAsDataURL(this.files[0]);
        return false;
    });
    $("#$id-reset").click(function(){
        $('#$id').val('');
        $("#$id-image").hide().attr("src", '');
        return false;
    });
});
</script>
HTML;
        return $xhtml;
    }
}
