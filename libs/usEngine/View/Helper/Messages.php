<?php
class usEngine_View_Helper_Messages extends Zend_View_Helper_Abstract
{
    protected $_html = '';

    public function messages()
    {
        $message = usEngine_Message::getInstance()->getMessage();
        if (isset($message) && $message != '') {
            foreach($message as $class => $message) {
                $this->_html .= '<div class="alertMessage ' . $class . ' SE">' . $message . '</div>';
            }
        }
        return $this->_html;
    }
}