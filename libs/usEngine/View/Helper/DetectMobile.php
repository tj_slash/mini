<?php
/**
 * usEngine Detected Mobile
 *
 * Класс определения мобильных устройств
 *
 * @category usEngine
 * @package View
 * @todo New user-agents
 * @author Vakylenko A. <vakylenkox@gmail.com>
 **/
class usEngine_View_Helper_DetectMobile extends Zend_View_Helper_Abstract
{
    /**
     * Detect mobile
     *
     * @author Vakylenko A. <vakylenkox@gmail.com>
     * @example
     * $detector = new usEngine_View_Helper_DetectMobile();
     * $detector->detectMobile();
     * 
     * @return boolean
     **/
    public function detectMobile()
    {
        if(isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/(alcatel|amoi|android|avantgo|blackberry|benq|cell|cricket|docomo|elaine|htc|iemobile|iphone|ipad|ipaq|ipod|j2me|java|midp|mini|mmp|mobi|motorola|nec-|nokia|palm|panasonic|philips|phone|playbook|sagem|sharp|sie-|silk|smartphone|sony|symbian|t-mobile|telus|up\.browser|up\.link|vodafone|wap|webos|wireless|xda|xoom|zte)/i', $_SERVER['HTTP_USER_AGENT'])) return true;
        return false;
    }
}