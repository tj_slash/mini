<?php
class usEngine_View_Helper_HeadMeta extends Zend_View_Helper_HeadMeta
{
    protected function _isValid($item)
    {
        if ((!$item instanceof stdClass) || !isset($item->type) || !isset($item->modifiers)) {
            return false;
        }

        if (!isset($item->content) && (! $this->view->doctype()->isHtml5() || (! $this->view->doctype()->isHtml5() && $item->type !== 'charset'))) {
            return false;
        }

        // <meta property= ... /> is only supported with doctype RDFa and HTML5
        if (!$this->view->doctype()->isRdfa() && !$this->view->doctype()->isHtml5() && $item->type === 'property') {
            return false;
        }

        return true;
    }
}
