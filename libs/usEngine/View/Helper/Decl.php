<?php
class usEngine_View_Helper_Decl extends Zend_View_Helper_Abstract
{
    public function decl($number, $words = array())
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return sprintf($words[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]], $number);
    }
}