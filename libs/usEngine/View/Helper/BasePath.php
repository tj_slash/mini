<?php
class usEngine_View_Helper_BasePath extends Zend_View_Helper_Abstract {

    public function basePath() {
        $options = Zend_Registry::get('options');
        if (isset($options['basepath'])) return $options['basepath'];
        return;
    }

}