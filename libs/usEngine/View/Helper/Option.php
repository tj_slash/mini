<?php
class usEngine_View_Helper_Option extends Zend_View_Helper_Abstract
{
    public function option($alias)
    {
        list($setting, $module, $option) = explode('.', $alias);
        $options = Zend_Registry::get('options');
        if (!empty($options[$setting])) {
            if (!empty($options[$setting][$module])) {
                if (!empty($options[$setting][$module][$option])) {
                    return $options[$setting][$module][$option];
                }
            }
        }
        return '';
    }
}