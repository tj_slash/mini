<?php

class usEngine_View_Helper_ImgSrc extends Zend_View_Helper_Abstract
{

    //первый параметр - локейшн в виртуалхосте нжинкса, где описываются алиасы проекта, 
    //второй - сам алиас, 
    //третий - путь до картинки, 
    public static function imgSrc($location = null, $alias = null, $path = null)
    {
        $config = Zend_Registry::getInstance();
        if (isset($config["options"]["use_ngximage"]) && $config["options"]["use_ngximage"] == 1 && $location != null && $alias !=null && $path !=null) return "/ngximage/".$location."/".$alias.$path;
        else return Zend_Layout::getMvcInstance()->getView()->baseUrl() .$path;
    }

}
