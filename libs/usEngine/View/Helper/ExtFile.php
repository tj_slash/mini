<?php
class usEngine_View_Helper_ExtFile extends Zend_View_Helper_Abstract
{
    protected function getExt($filename)
    {
        $ext = explode('.', $filename);
        return $ext[count($ext) - 1];
    }

    public function extFile($filename)
    {
        return $this->getExt($filename);
    }
}