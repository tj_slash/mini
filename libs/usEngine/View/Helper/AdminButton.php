<?php
/**
 * usEngine_View_Helper_AdminButton
 *
 * Хелпер, чтоб кнопачки впиливать в админке над гридом
 *
 * @categiry 
 * @package
 * @link http://amur.local/obv_new/obv/admin-obv/list
 * @author Lyaskov I. <podstavnoy25@gmail.com>
 **/
class usEngine_View_Helper_AdminButton extends Zend_View_Helper_Abstract
{
    /**
     * formButton
     *
     * Формирует кнопачку отдельную
     *
     * @author Lyaskov I. <podstavnoy25@gamil.com> 
     * 
     * 
     * 
     *
     **/
    
    public function formButton($values, $param)
    {
        $url = $values[0];
        $link_text = $values[1];
        $show = $values[2];
        $z =Zend_Controller_Front::getInstance()->getParam('bootstrap');
        if(isset($z))
            $baseUrl = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view')->baseUrl();        
        else $baseUrl = '';
 
        $style = ($show == $param) ? 'style="background-color:white;"' : '';
        $html = '<div style="position:relative;float:left;" class="toolbar_buttons"><a href="'.$baseUrl. $url. '" title="'. $link_text  .'" rel="isNull" class="uibutton confirm"'.  $style .'>' . $link_text . '</a></div>';
        return $html;
    }
    
    public function adminButton($buttons, $param)
    {
        $html = '';
        foreach($buttons as $button){
            $html .= $this->formButton($button,$param);
        }
        return $html . '<br><br><br>';
    }
}
