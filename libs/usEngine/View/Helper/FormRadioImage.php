<?php
require_once 'Zend/View/Helper/FormElement.php';
/**
 * RadioImage form element
 *
 * @category   usEngine
 * @package    usEngine_View
 * @subpackage Helper
 * 
 * @author Vakylenko A. <vakylenkox@gmail.com>
 */
class usEngine_View_Helper_FormRadioImage extends Zend_View_Helper_FormElement
{
    /**
     * @param string|array $name Название элемента для параметра "name" тэга <input />
     * @param mixed $value Выбраное значение  по-умолчанию
     * @param array|string $attribs Html атрибуты для картинок
     * @param array $options массив содержащий значение и путь для каждой картинки
     * 
     * @return string html
     */
    public function formRadioImage($name, $value = null, $attribs = null, $options = null)
    {
        $info = $this->_getInfo($name, $value, $attribs, $options);
        extract($info); // name, value, attribs, options
        
        $options = (array) $options;
        $xhtml = '';
        $list  = array();
        $list[]  = '<input type="hidden" id="'.$id.'" name="'.$name.'" value="'.$value.'" />';
        
        require_once 'Zend/Filter/Alnum.php';
        $filter = new Zend_Filter_Alnum();
        
        $selectedClass = (isset($attribs['selectedClass']) && !empty ($attribs['selectedClass']))?$attribs['selectedClass']:'selected';
        $selectedClass = $filter->filter($selectedClass);
        
        if(!isset($attribs['class'])) $attribs['class'] = null;
        $classBck = $attribs['class'];
        
        foreach ($options as $optVal => $imgPath) {
            $imgId = $id . '-' . $filter->filter($optVal);
            if ($optVal == $value) $attribs['class'] .= " ".$selectedClass;
            $list[] = '<img src="'.$imgPath.'" id="'.$imgId.'" rel="'.$optVal.'" ' . $this->_htmlAttribs($attribs) . '/>';
            if(strstr($attribs['class'], $selectedClass)) $attribs['class'] = $classBck;
        }
        
        $list[]  = '<br /><a href=\"javascript;\" id="' . $id . '-reset">Reset Selection</a>'.PHP_EOL;
        $xhtml .= implode(PHP_EOL, $list);
        $xhtml .= 
<<<HTML
<script>
    $(function(){
       $("img[id*='$id']").click(function(){
            $('#$id').val($(this).attr('rel'));
            $("img[id*='$id']").removeClass('$selectedClass');
            $(this).addClass('$selectedClass');
        });
        $('#$id-reset').click(function(){
            $("img[id*='$id']").removeClass('$selectedClass');
            $('#$id').val('')
            return false;
        });
    });
</script>
<style>
img[id*='$id']{cursor:pointer;border:2px solid white}
img[id*='$id']:hover{border:2px solid rgb(194, 227, 240)}
img[id*='$id'].selected{border:2px solid rgb(115, 180, 206)!important}
</style>
HTML;
        return $xhtml;
    }
}
