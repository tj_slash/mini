<?php
class usEngine_Form_Form extends Zend_Form
{
    protected $_params = array();
    
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Description', array('tag' => 'span', 'class' => 'f_help', 'escape' => false)),
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('HtmlTag', array('tag' => 'div')),
        array('Label', array('separator' => ' ', 'escape' => false)),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'section'))
    );

    protected static $_inlineElementDecorator = array(
        'ViewHelper',
        array('Description', array('tag' => 'span', 'class' => 'f_help', 'escape' => false)),
        array('Errors', array('escape' => false)),
        //array('HtmlTag', array('tag' => 'div')),
        array('Label', array('separator' => ' ', 'escape' => false, 'style'=>'width:100%;')),
        array(array('elementDiv' => 'HtmlTag'), array(
            'tag' => 'div',
            'class' => 'section',
            'style' => 'border:none; float:left; width:25%; padding: 0 2% 0 0;',
        ))
    );
    protected static $_wideElementDecorator = array(
        'ViewHelper',
        array('Description', array('tag' => 'span', 'class' => 'f_help', 'escape' => false)),
        array('Errors', array('escape' => false)),
        //array('HtmlTag', array('tag' => 'div')),
        array('Label', array('separator' => ' ', 'escape' => false)),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'section'))
    );

    protected static $_fileElementDecorator = array(
        'File',
        array('Description', array('tag' => 'span', 'class' => 'f_help', 'escape' => false)),
        array(array('Value'=>'HtmlTag'), array('tag'=>'span','class'=>'value')),
        //array('Errors', array('escape' => false)),
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('HtmlTag', array('tag' => 'div')),
        'Label',
        array(array('Field'=>'HtmlTag'), array('tag'=>'div','class'=>'section')),
    );
    
    protected static $_checkboxDecorator = array(
        array('ViewHelper', array('class' => 'checker')),
        array('Description', array('tag' => 'span', 'class' => 'f_help', 'escape' => false)),
        array('Errors', array('escape' => false)),
        array('HtmlTag', array('tag' => 'div', 'class' => 'custom-checkbox')),
        array('Label', array('separator' => ' ', 'escape' => false)),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'section'))
    );

    protected static $_buttonDecorator = array(
        'ViewHelper',
        array('Description', array('tag' => 'span', 'class' => 'f_help', 'escape' => false)),
        array('Errors', array('escape' => false)),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'span', 'style' => 'position:relative;top:13px;'))
    );

    protected static $_formDecorator = array(
        'FormElements',
        array('HtmlTag', array('tag' => 'div', 'class' => 'zend_form')),
        'Form'
    );

    protected static function createPublishChekbox()
    {
        $item = new Zend_Form_Element_Checkbox('publish');
        $item->setLabel('Publish');
        $item->setDecorators(self::$_elementDecorator);
        $item->setRequired(true);
        return $item;
    }

    protected static function createSubmitInput()
    {
        $item = new Zend_Form_Element_Submit('submit');
        $item->setLabel('Save');
        $item->setAttrib('class', 'uibutton');
        $item->setAttrib('title', 'Сохранить и вернуться к списку');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    protected static function createSubmitContinueInput()
    {
        $item = new Zend_Form_Element_Submit('submit_continue');
        $item->setLabel('Save & Edit');
        $item->setAttrib('class', 'uibutton');
        $item->setAttrib('title', 'Сохранить и продолжить редактирование');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    protected static function createCopyAddInput()
    {
        $item = new Zend_Form_Element_Submit('submit_add');
        $item->setLabel('Save & Add');
        $item->setAttrib('class', 'uibutton');
        $item->setAttrib('title', 'Сохранить и добавить еще');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    protected static function createSubmitAddInput()
    {
        $item = new Zend_Form_Element_Submit('submit_copy');
        $item->setLabel('Добавить и скопировать');
        $item->setAttrib('class', 'uibutton');
        $item->setAttrib('title', 'Сохранить и скопировать');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }
    
    /**
     * Создает Zend_Form_Element_File с подменёнными Zend_File_Transfer_Adapter_Http и Zend_Validate_File_Upload для
     * корректной валидации при тестировании формы.
     * 
     * @param type $name Имя элемента формы
     * @return \Zend_Form_Element_File
     */
    protected static function createFileUploadInput($name = null)
    {
        if (!isset($name) || empty($name))
            throw new Exception('Не указано имя элемента');
        $item = new Zend_Form_Element_File($name);
        if (APPLICATION_ENV == 'testing') {
            $item->setTransferAdapter(new usEngine_Test_File_Transfer_Adapter_Http());
            $item->removeValidator('Zend_Validate_File_Upload');
            $item->addValidator(new usEngine_Test_Validate_File_Upload());
        }
        return $item;
    }
    
    /**
     * Метод для загрузки изображения с формы.
     * 
     * @param string    $form_element_name Имя элемента формы, хранящего информацию об изображении.
     * @param string    $relative_path Путь до папки изображений относительно $absolute_prefix. Не забываем про слэши в начале и в конце.
     * @param string    $new_file_name Имя создаваемого файла. Необязательный параметр. Если не передано, или передано пустое значение, новое имя генерируется автоматически на основе имени загруженного файла.
     * @param boolean   $create_dir Если TRUE, то метод автоматически создаст папку для загрузки, иначе - сгенерирует исключение.
     * @param string    $absolute_prefix Префикс $relative_path. Необязательный параметр. Если не передан, то используется путь до APPLICATION_PATH ../public.
     * 
     * @return boolean
     */
    public function uploadImage($form_element_name = null, $relative_path = null, $new_file_name = null, $create_dir = false, $absolute_prefix = null)
    {
        if (!isset($form_element_name))
            throw new Exception('Не указано имя элемента формы для обработки изображения!');
        if (!$this->getElement($form_element_name))
            throw new Exception('Неверно указано имя элемента формы для обработки изображения!');
        if (!isset($relative_path))
            throw new Exception('Не указан путь к папке изображений!');
        if (!isset($absolute_prefix))
            $absolute_prefix = APPLICATION_PATH . '/../public';
        $absolute_path = $absolute_prefix . $relative_path;
        if(!file_exists($absolute_path)) {
            if (!$create_dir)
                throw new Exception('Директория `' . $absolute_path . '` не существует');
            else
                mkdir($absolute_path, 0777, true);
        }
        $absolute_path = realpath($absolute_path) . '/';
        $info = $this->$form_element_name->getFileInfo();
        if (!empty($info[$form_element_name]['name'])) {
            $name = $this->_getImageName($absolute_path, $info[$form_element_name]['name'], $new_file_name);
            $this->$form_element_name->addFilter('Rename', $absolute_path . $name);
            $this->$form_element_name->receive();
            return $relative_path . $name;
        } else {
            return false;
        }
    }
    
    /**
     * Генерирует имя файла, уникального внутри директории $path.
     * 
     * @param string $path Абсолютный путь до папки изображений.
     * @param string $filename Имя файла с формы.
     * @param string $new_file_name Предполагаемое имя нового файла.
     * 
     * @return string Сгенерированное имя файла.
     */
    private function _getImageName($path, $filename, $new_file_name)
    {
        $ext = explode('.', $filename);
        $ext = $ext[count($ext) - 1];
        $result = null;
        if (isset($new_file_name) && !empty($new_file_name)) {
            $alias = usEngine_Text_Translit::getInstance()->correctString($new_file_name);
            $result = $alias . '-'. md5(time() . $alias) . '.' . $ext;
            $filename = $path . $result;
            while(file_exists($filename)) {
                $result = $alias . '-' . md5(time() . $alias) . '.' . $ext;
                $filename = $path . $result;
            }
            return $result;
        } else {
            return md5(time() . $filename) . '.' . $ext;
        }
    }
    
    /**
     * Set form param
     *
     * @param  string $key
     * @param  mixed $value
     * @return Zend_Form
     */
    public function setParam($key, $value)
    {
        $key = (string) $key;
        $this->_params[$key] = $value;
        return $this;
    }

    /**
     * Add multiple form params at once
     *
     * @param  array $params
     * @return Zend_Form
     */
    public function addParams(array $params)
    {
        foreach ($params as $key => $value) {
            $this->setParam($key, $value);
        }
        return $this;
    }

    /**
     * Set multiple form attributes at once
     *
     * Overwrites any previously set attributes.
     *
     * @param  array $params
     * @return Zend_Form
     */
    public function setParams(array $params)
    {
        $this->clearParams();
        return $this->addParams($params);
    }

    /**
     * Retrieve a single form param
     *
     * @param  string $key
     * @return mixed
     */
    public function getParam($key)
    {
        $key = (string) $key;
        if (!isset($this->_params[$key])) {
            return null;
        }

        return $this->_params[$key];
    }

    /**
     * Retrieve all form attributes/metadata
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * Remove attribute
     *
     * @param  string $key
     * @return bool
     */
    public function removeParams($key)
    {
        if (isset($this->_params[$key])) {
            unset($this->_params[$key]);
            return true;
        }

        return false;
    }

    /**
     * Clear all form params
     *
     * @return Zend_Form
     */
    public function clearParams()
    {
        $this->_params = array();
        return $this;
    }
    
    public function setOptions(array $options) {
        if (isset($options['params'])) {
            $this->setParams($options['params']);
            unset($options['params']);
        }
        return parent::setOptions($options);
    }
    
    /**
     * Magic method 
     * 
     * Метод, возвращающий массив опций со значениями
     * 
     * @return array 
     */
    protected function getForPopulate()
    {
        $class = get_class($this);
        $values = Index_Model_OptionsTable::getInstance()->getOptionsInAlias($class::$fields, $class::MODULE_NAME);
        $search = implode('.', array(Index_Model_OptionsTable::GLOBAL_ALIAS, $class::MODULE_NAME));
        $options = array();
        foreach ($values as $key => $option) {
            $options[str_replace("{$search}.", '', $option->alias)] = $option->value;
        }
        return $options;
    }
    
    /**
     * Populating setting form 
     */
    public function initSettings()
    {
        $this->populate($this->getForPopulate());
    }
    
    /**
     * Magic method 
     * 
     * Метод, возвращающий лэйблы
     * 
     * @return array 
     */
    public function getLabels()
    {
        $labels = array();
        $values = $this->getValues();
        if (!empty($values['option'])) {
            foreach ($values['option'] as $alias => $value) {
                $labels[$alias] = $this->getElement($alias)->getLabel();
            }
        }
        return $labels;
    }
    
    private static function createMetaKeywords()
    {
        $item = new Zend_Form_Element_Text('m_k');
        $item->setDescription('Ключевые слова');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(false);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private static function createMetaTitle()
    {
        $item = new Zend_Form_Element_Text('m_t');
        $item->setLabel('Метаданные');
        $item->setDescription('Заголовок');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(false);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private static function createMetaDesc()
    {
        $item = new Zend_Form_Element_Text('m_d');
        $item->setDescription('Описание');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(false);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function initMeta()
    {
        $this->addElement(self::createMetaKeywords());
        $this->addElement(self::createMetaTitle());
        $this->addElement(self::createMetaDesc());
        $this->addDisplayGroup(array('m_t','m_k','m_d'), 'meta_group');
    }
    
    public function initForm()
    {
        $this->addElement(self::createPublishChekbox());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->addElement(self::createSubmitAddInput());
        $this->addElement(self::createCopyAddInput());
        $this->setDecorators(self::$_formDecorator);
    }
}