<?php
class usEngine_Form_Element_Uploader extends Zend_Form_Element_Hidden
{
    protected $_options = array(
        'upload' => '/catalog/admin-image/upload',
        'delete' => '/catalog/admin-image/delete',
        'main' => '/catalog/admin-image/main',
        'path' => '/media/products/',
        'images' => false
    );
    
    public function render(Zend_View_Interface $view = null) 
    {
        $html = parent::render($view);
        return $html . $this->getHtml();
    }
    
    public function setOptions(array $options)
    {
        if (is_array($options)) {
            $this->_options = array_merge($this->_options, $options);
        }
        return $this;
    }
    
    public function getHtml()
    {
        $id = $this->getId();
        $upload = $this->_options['upload'];
        $delete = $this->_options['delete'];
        $images = $this->_options['images'];
        $path = $this->_options['path'];
        
        $html = <<<HTML
<link rel="stylesheet" type="text/css" href="/css/admin/uploadify/uploadify.css">
<script type="text/javascript" src="/js/admin/uploadify/jquery.uploadify-3.1.min.js"></script>
<style>
.uploadify{margin-left:35%;margin-top:10px}
.uploadify-queue{float:left}
#dataTable_$id{margin-left:35%}
#dataTable_$id .ui-state-default{width:100px;height:100px;border:none;float:left;margin:0 10px 10px 0;position:relative}
#dataTable_$id a[rel="delete"]{display:block;text-align:center;height:20px;line-height:20px;background:rgb(253, 192, 192);color:#fff;position:absolute;left:0;right:0;bottom:0}
#dataTable_$id a[rel="delete"]:hover{background:rgb(252, 104, 104)}
#dataTable_$id .ui-state-highlight{width:100px;height:100px;margin:0 0 20px 0;float:left;line-height:100px;background:#ccc;margin:0 10px 10px 0}
</style>
<input type="file" name="file_upload" id="uploader_$id" />
<ul id="dataTable_$id">
HTML;
        if ($images) {
            foreach($images as $image) {
                $html .= '<li class="ui-state-default">
                    <img src="' . $path . $image->image . '" width="100" height="100">
                    <a href="javascript:void(0)" data-id="' . $image->id . '" title="Удалить" rel="delete">Удалить</a>
                    <input type="hidden" name="image[]" value="' . $image->id . '">
                </li>';
            }
        }
        
        $html .= <<<HTML
</ul>
<div style="clear:both"></div>
<script type="text/javascript">
$(function() {
    var dataTable = $("#dataTable_$id");
    var elIcon = $("<img />", {src: '/images/admin/loadder/loader_green.gif', style: 'margin-left: 5px'}).addClass('preloader');
    $('#uploader_$id').after(elIcon.hide());
    $("#uploader_$id").uploadify({
            height          : 30,
            swf             : '/js/admin/uploadify/uploadify.swf',
            uploader        : "$upload",
            width           : 120,
            buttonText      : 'Обзор...',
            cancelImg       : '/images/admin/uploadify/uploadify-cancel.png',
            onUploadSuccess : function(file, data, response){
                var data = jQuery.parseJSON(data);
                var img = $('<img />').attr({src : '' + data.src, width : 100, height : 100});
                var deleteLink = $('<a />').attr({href : 'javascript:void(0)', 'data-id' : data.id, 'title':'Удалить', 'rel':'delete'}).text('Удалить');
                var id = $('<input />').attr({type : 'hidden', name : 'image[]', value : data.id});
                dataTable.append($('<li />').addClass('ui-state-default').append(img.after(deleteLink).after(id)));
            },
            onUploadError : function(file, errorCode, errorMsg, errorString) {
                console.log(errorString);
            }
    });
    $('a[rel="delete"]').live('click', function(){
        $(this).eq(0).parent().fadeOut(function(){
            $(this).remove();
        });
        $.ajax({
            url: "$delete/?id=" + $(this).data('id'),
            type: 'post',
            dataType: "json",
            success: function (data, textStatus) { 
                showSuccess(data.notif); 
            },
            complete: function() { elIcon.hide(); },
            beforeSend: function() { elIcon.show(); }
        });
    });    
    dataTable.sortable({placeholder: "ui-state-highlight"});
});
</script>
HTML;
        return $html;
    }
}
