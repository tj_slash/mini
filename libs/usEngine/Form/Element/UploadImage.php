<?php
require_once 'Zend/Form/Element/Xhtml.php';

/**
 * UploadImage form element
 *
 * @category   usEngine
 * @package    usEngine_Form
 * @subpackage Element
 * 
 * @author Vakylenko A. <vakylenkox@gmail.com>
 */
class usEngine_Form_Element_UploadImage extends Zend_Form_Element_Xhtml
{
    private $loadValue = false;
    
    /**
     * @var string
     */
    public $helper = 'FormUploadImage';
    
    public function getValue()
    {
        if (strpos($this->_value, ';base64') !== false) { 
            $image = $this->_value;
            $ext = '';
            if (strpos($image, 'data:image/png') !== false) {
                $ext = '.png'; 
                $image = str_replace('data:image/png;base64,', '', $image);
            } else if (strpos($image, 'data:image/vnd.microsoft.icon') !== false) {
                $ext = '.ico'; 
                $image = str_replace('data:image/vnd.microsoft.icon;base64,', '', $image);
            } else if (strpos($image, 'data:image/jpg') !== false) {
                $ext = '.jpg'; 
                $image = str_replace('data:image/jpg;base64,', '', $image);
            } else if (strpos($image, 'data:image/jpeg') !== false) {
                $ext = '.jpg'; 
                $image = str_replace('data:image/jpeg;base64,', '', $image);
            }
            $image = str_replace(' ', '+', $image);
            $data = base64_decode($image);
            $filename = uniqid() . $ext;
            $file = $this->getAttrib('path') . $filename;
            file_put_contents($file, $data);
            $this->_value = $this->getAttrib('url') . $filename;
        }
        return $this->_value;
    }
}