<?php
/**
 * Form_Element_Category
 *
 * @category iwslab
 * @package usEngine
 * @subpackage Category
 * @author Dyachkov D. <dyachkov1991@gmail.com>
 **/
class usEngine_Form_Element_Category extends Zend_Form_Element_Hidden
{
    /**
     * Element's options
     * @var array $_options
     **/
    protected $_options = array(
        'url' => '/obv/admin/get-category-ajax',
        'id' => 0
    );
    
    /**
     * Set element's options
     *
     * @param array $options Element's options
     * 
     * @access public
     *
     * @return usEngine_Form_Element_Category
     * 
     * @author Dyachkov D. <dyachkov1991@gmail.com>
     */
    public function setOptions(array $options)
    {
        if (is_array($options)) {
            $this->_options = array_merge($this->_options, $options);
        }
        return $this;
    }
    
    /**
     * Render element
     *
     * @param Zend_View_Interface $view Zend View
     * 
     * @access public
     *
     * @return string
     * 
     * @author Dyachkov D. <dyachkov1991@gmail.com>
     */
    public function render(Zend_View_Interface $view = null) 
    {
        $html = parent::render($view);
        
        return $html . $this->getJavaScript();
    }
    
    /**
     * Initialize JavaScript
     *
     * @access public
     *
     * @return string
     * 
     * @author Dyachkov D. <dyachkov1991@gmail.com>
     */
    public function getInit() 
    {
        $init = '';
        $stage = 0;
        $parents = $this->getParentNodes();
        
        foreach ($parents as $level) {
            if (count($level) > 0) {
                $buff = '<select id="level_' . $stage . '" style="padding: 6px; width: 400px; margin-right: 5px">';
                $buff .= '<option value="">Выберите категорию</option>';

                foreach ($level as $category) {
                    $buff .= '<option ' . ($category->getNode()->isLeaf() ? '' : 'style="color: #9CB611"') . ' value="' . $category->id . '">' . $category->title . '</option>';
                }

                $buff .= '</select>';
                $init .= 'level++;render($(' . Zend_Json::encode($buff) . '));';
                $stage++;
            }
        }
        
        foreach ($parents as $key => $value) {
            $init .= 'setSelectedValue(' . $key . ');';
        }
        
        return $init;
    }
    
    /**
     * Get parent nodes
     *
     * @access public
     *
     * @return array
     * 
     * @author Dyachkov D. <dyachkov1991@gmail.com>
     */
    public function getParentNodes() 
    {
        $return = array();
        $id = $this->_options['id'];
        $model = $this->_options['model'];
        
        $node = $model->findOneById($id);
        if ($node) {
            $parents = Doctrine_Query::create()
                ->from($model->getClassnameToReturn())
                ->andWhere('lft < ?', $node->lft)
                ->andWhere('rgt > ?', $node->rgt)
                ->execute();
        } else {
            $parents = Doctrine_Query::create()
                ->from($model->getClassnameToReturn())
                ->orderBy('lft')
                ->limit(1)
                ->execute();
        }
        
        $return[0] = array($parents[0]);
        
        foreach ($parents as $parent) {
            $return[$parent->id] = Doctrine_Query::create()
                ->from($this->_options['model']->getClassnameToReturn())
                ->where('level = ?', $parent->level + 1)
                ->andWhere('lft > ?', $parent->lft)
                ->andWhere('rgt < ?', $parent->rgt)
                ->andWhere('id <> ?', $id)
                ->orderBy('lft')
                ->execute();
        }
        
        return $return;
    }
    
    /**
     * Get JavaScript
     *
     * @access public
     *
     * @return string
     * 
     * @author Dyachkov D. <dyachkov1991@gmail.com>
     */
    public function getJavaScript() 
    {
        $js = '
            <script type="text/javascript">
                var leafStack = new Array();
                $("document").ready(function() {

                    var level = -1;
                    
                    var element = $("#' . $this->getId() . '");
                    
                    init();
                    
                    function init() {
                        ' . $this->getInit() . '
                    }

                    function createNextSelector(parentId) {
                        switchIcon();
                        $.ajax({
                            url: "' . $this->_options['url']. '",
                            data:{"level": level + 1, "parent_id": parentId, "no_id": ' . $this->_options['id'] . '},
                            type: "get",
                            dataType: "json",
                            success: function(data) {
                                switchIcon();
                                if (data.length > 0) {
                                    level++;
                                    var newSelect = creteSelectObject();
                                    $("<option />", {value: "", text: "Выберите категорию"}).appendTo(newSelect);
                                    for (i = 0 ; i < data.length; i++) {
                                        var color = "#9CB611";
                                        if (data[i].isLeaf) {
                                            leafStack.push(data[i].id);
                                            color = "#000000";
                                        }
                                        $("<option />", {value: data[i].id, text: data[i].title, style: "color:" + color}).appendTo(newSelect);
                                    }
                                    render(newSelect);
                                }
                            }
                        });
                    }
                    
                    function setSelectedValue(value) {
                        element.val($("select[id*=level_] option[value=" + value + "]").attr("selected", "selected").val());
                    }
                    
                    function removeLastSelector() {
                        $("#level_" + level).parent().remove();
                        $("#level_" + --level).removeAttr("disabled");
                        $("#level_" + level).parent().children(".delete").show();
                        $("#level_" + level + " :first").attr("selected", "selected");
                        resetValue();
                    }
                    
                    function creteSelectObject() {
                        return $("<select id=\"level_" + level  + "\" name=\"level_" + level + "\" style=\"padding: 6px; width: 400px; margin-right: 5px\" />");
                    }
                    
                    function render(object) {
                        $("select[id*=level]").attr("disabled", "disabled");
                        $(".delete").hide();
                        var del = level > 0 ? $("<img class=\"delete\" src=\"' . Zend_Controller_Front::getInstance()->getBaseUrl() . '/images/icons/delete.png\" >") : "";
                        var preloader = $("<img style=\"display: none;\" class=\"preloader\" src=\"' . Zend_Controller_Front::getInstance()->getBaseUrl() . '/images/admin/loadder/loader_green.gif\" >");
                        var container = $("<div>");
                        container.append(object).append(preloader);
                        container.append(del).appendTo(element.parent().parent());
                    }
                    
                    function switchIcon() {
                        $("#level_" + level).parent().children(".delete").toggle();
                        $("#level_" + level).parent().children(".preloader").toggle();
                    }
                    
                    function resetValue() {
                        element.val("");
                    }
                    
                    $("select[id*=level]").live("change", function() {
                        var value = $(this).val();
                        if (value) {
                            if ($.inArray(value, leafStack) == -1) {
                                createNextSelector(value);
                                resetValue();
                            }
                            element.val(value);
                        } else {
                            resetValue();
                        }
                    });
                    
                    $(".delete").live("click", function() {
                        removeLastSelector();
                    });
                    
                });
            </script>
        ';
        return $js;
    }
}
