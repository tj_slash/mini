<?php
require_once 'Zend/Form/Element/Multi.php';

/**
 * RadioImage form element
 *
 * @category   usEngine
 * @package    usEngine_Form
 * @subpackage Element
 * 
 * @author Vakylenko A. <vakylenkox@gmail.com>
 */
class usEngine_Form_Element_RadioImage extends Zend_Form_Element_Multi
{
    /**
     * @var string
     */
    public $helper = 'FormRadioImage';
}