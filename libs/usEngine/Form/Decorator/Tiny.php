<?php
/**
 * Created by JetBrains PhpStorm.
 * User: v25
 * Date: 19.07.13
 * Time: 11:29
 * To change this template use File | Settings | File Templates.
 */
class usEngine_Form_Decorator_Tiny extends Zend_Form_Decorator_Abstract
{
	public function render($content)
	{
            $is_xml_http_request = $this->getOption('is_xml_http_request');
            if (!$is_xml_http_request || !isset($is_xml_http_request)) {
		$add ='
			<script type="text/javascript" src="/s/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
			<script type="text/javascript" src="/s/js/jquery-1.6.4.min.js"></script>
			<script type="text/javascript">
				jQueryForTiny = jQuery.noConflict(true);
			</script>
			<script type="text/javascript" src="/s/js/blog-comment-editor.js"></script>'
			;
            } else {
                $add ='<script type="text/javascript" src="/s/js/blog-comment-editor.js"></script>';
            }
            return $add .$content;
	}
}
