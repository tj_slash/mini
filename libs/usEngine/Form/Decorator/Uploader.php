<?php
class usEngine_Form_Decorator_Uploader extends Zend_Form_Decorator_Abstract
{
    public function render($content)
    {
        $elementID = $this->getElement()->getId();
        /**
         * Actions
         */
        $buttonUploadText = $this->getOption('buttonUploadText');
        if(!$buttonUploadText) $buttonUploadText = 'Загрузить все';
        $buttonCancelText = $this->getOption('buttonCancelText');
        if(!$buttonCancelText) $buttonCancelText = 'Отменить все';
        $buttonUpload = '<button id="upload-all" class="submit long">' . $buttonUploadText . '</button>';
        $buttonCancel = '<button id="cancel-all" class="submit long">' . $buttonCancelText . '</button>';  
        $actions = '<div class="actions-uploader">' . $buttonUpload . $buttonCancel . '</div>';
        
        /**
         * Images Lists
         */
        $imagesList = '
            <div>
                <ul class="imglist" id="img-list">
                    <li style="text-align:center" id="img-container">
                        Перетащите сюда фото
                        <div><img src="/images/icons/icon_zoom.png" style="width:39px !important;height:39px !important;position:relative;"/></div>
                    </li>
                </ul>
            </div>';
        
        $debug = $this->getElement()->getDebug();
        $debugLog = '';
        if ($debug) $debugLog = '<div id="console"></div>';
        return $content . $imagesList . $actions . $debugLog;
    }
}
