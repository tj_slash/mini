<?php
/**
 * Created by JetBrains PhpStorm.
 * User: v25
 * Date: 27.09.13
 * Time: 16:44
 * To change this template use File | Settings | File Templates.
 */
if(!class_exists('Zend_Form_Decorator_Abstract'))
    require_once 'Zend/Form/Decorator/Abstract.php';

/**
 * ZendExtra_Form_Decorator_Element_HtmlCode
 *
 * Декоратор, позволяющий выводить произвольный код в Zend_Form
 * @author Tokarchuk Andrey (http://tokarchuk.ru) netandreus@gmail.com
 *
 * @category   ZendExtra
 * @package    ZendExtra_Form
 * @subpackage Decorator
 */
class usEngine_Form_Decorator_HtmlCode extends Zend_Form_Decorator_Abstract
{
    /**
     * Placement; default to surround content
     * @var string
     */
    protected $_placement = null;

    /**
     * HTML tag to use
     * @var string
     */
    protected $_tag;

    /**
     * @var Zend_Filter
     */
    protected $_tagFilter;

    /**
     * Convert options to tag attributes
     *
     * @return string
     */
    protected function _htmlAttribs(array $attribs)
    {
        $xhtml = '';
        foreach ((array) $attribs as $key => $val) {
            $key = htmlspecialchars($key, ENT_COMPAT, 'UTF-8');
            if (is_array($val)) {
                $val = implode(' ', $val);
            }
            $val    = htmlspecialchars($val, ENT_COMPAT, 'UTF-8');
            $xhtml .= " $key=\"$val\"";
        }
        return $xhtml;
    }

    /**
     * Normalize tag
     *
     * Ensures tag is alphanumeric characters only, and all lowercase.
     *
     * @param  string $tag
     * @return string
     */
    public function normalizeTag($tag)
    {
        return $tag;
    }

    /**
     * Set tag to use
     *
     * @param  string $tag
     * @return Zend_Form_Decorator_HtmlTag
     */
    public function setTag($tag)
    {
        $this->_tag = $this->normalizeTag($tag);
        return $this;
    }

    /**
     * Get tag
     *
     * If no tag is registered, either via setTag() or as an option, uses 'div'.
     *
     * @return string
     */
    public function getTag()
    {
        if (null === $this->_tag) {
            if (null === ($tag = $this->getOption('tag'))) {
                $this->setTag('div');
            } else {
                $this->setTag($tag);
                $this->removeOption('tag');
            }
        }

        return $this->_tag;
    }

    /**
     * Get the formatted open tag
     *
     * @param  string $tag
     * @param  array $attribs
     * @return string
     */
    protected function _getOpenTag($tag, array $attribs = null)
    {
        $html = $tag;
        return $html;
    }

    /**
     * Get formatted closing tag
     *
     * @param  string $tag
     * @return string
     */
    protected function _getCloseTag($tag)
    {
        return '';
    }

    /**
     * Render content wrapped in an HTML tag
     *
     * @param  string $content
     * @return string
     */
    public function render($content)
    {
        $tag       = $this->getTag();
        $placement = $this->getPlacement();
        $noAttribs = $this->getOption('noAttribs');
        $openOnly  = $this->getOption('openOnly');
        $closeOnly = $this->getOption('closeOnly');
        $this->removeOption('noAttribs');
        $this->removeOption('openOnly');
        $this->removeOption('closeOnly');

        $attribs = null;
        if (!$noAttribs) {
            $attribs = $this->getOptions();
        }

        switch ($placement) {
            case self::APPEND:
                if ($closeOnly) {
                    return $content . $this->_getCloseTag($tag);
                }
                if ($openOnly) {
                    return $content . $this->_getOpenTag($tag, $attribs);
                }
                return $content
                . $this->_getOpenTag($tag, $attribs)
                . $this->_getCloseTag($tag);
            case self::PREPEND:
                if ($closeOnly) {
                    return $this->_getCloseTag($tag) . $content;
                }
                if ($openOnly) {
                    return $this->_getOpenTag($tag, $attribs) . $content;
                }
                return $this->_getOpenTag($tag, $attribs)
                . $this->_getCloseTag($tag)
                . $content;
            default:
                return (($openOnly || !$closeOnly) ? $this->_getOpenTag($tag, $attribs) : '')
                . $content
                . (($closeOnly || !$openOnly) ? $this->_getCloseTag($tag) : '');
        }
    }
}
