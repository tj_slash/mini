<?php
class usEngine_Form_Setting extends usEngine_Form_Form
{    
    const ELS_NAME = 'option';
    
    /**
     * Magic method 
     * 
     * Метод, возвращающий массив опций со значениями
     * 
     * @return array 
     */
    protected function getForPopulate()
    {
        $class = get_class($this);
        $formdata = array();
        $options = Zend_Registry::get('options');
        if (!empty($options['setting'])) {
            if (!empty($options['setting'][$class::MODULE_NAME])) {
                foreach ($options['setting'][$class::MODULE_NAME] as $alias => $value) {
                    if ($this->is_serial($value)) {
                        $formdata[$alias] = unserialize($value);
                        
                    } else {
                        $formdata[$alias] = $value;
                    }
                }
            }
        }
        return $formdata;
    }
    
    private function is_serial($string) {
        return (@unserialize($string) !== false);
    }
    
    /**
     * Populating setting form 
     */
    public function init()
    {
        $this->populate($this->getForPopulate());
    }
    
    /**
     * Magic method 
     * 
     * Метод, возвращающий лэйблы
     * 
     * @return array 
     */
    public function getLabels()
    {
        $labels = array();
        $values = $this->getValues();
        if (!empty($values[self::ELS_NAME])) {
            foreach ($values[self::ELS_NAME] as $alias => $value) {
                $labels[$alias] = $this->getElement($alias)->getLabel();
            }
        }
        return $labels;
    }
}