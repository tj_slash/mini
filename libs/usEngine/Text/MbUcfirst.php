<?php
class usEngine_Text_MbUcfirst
{
    public static function mb_ucfirst($string, $encode = 'UTF-8')
    {
        return mb_strtoupper(
            mb_substr($string, 0, 1, $encode), $encode
        ).mb_substr(
            $string, 1, mb_strlen($string, $encode), $encode
        );
    }
}