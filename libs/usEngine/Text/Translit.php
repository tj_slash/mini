<?php
class usEngine_Text_Translit
{

    private static $_instance = null;

    protected static $translate = array(
        'А' => 'a', 'Б' => 'b', 'В' => 'v', 'Г' => 'g', 'Д' => 'd', 'Е' => 'e', 'Ё' => 'e', 'Ж' => 'zh', 'З' => 'z', 'И' => 'i', 'Й' => 'y',
        'К' => 'k', 'Л' => 'l', 'М' => 'm', 'Н' => 'n', 'О' => 'o', 'П' => 'p', 'Р' => 'r', 'С' => 's', 'Т' => 't', 'У' => 'u', 'Ф' => 'f',
	'Х' => 'h', 'Ц' => 'ts', 'Ч' => 'ch', 'Ш' => 'sh', 'Щ' => 'sch', 'Ъ' => '', 'Ы' => 'yi', 'Ь' => '', 'Э' => 'e', 'Ю' => 'yu', 'Я' => 'ya',
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y',
	'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
        'х' => 'h', 'ц' => 'ts', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'yi', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
        'я' => 'ya',  ', ' => '-', ' ' => '-', ':' => '-', '(' => '', ')' => '', '/' => '-', '\\'=> '-' );

    static public function getInstance()
    {
        if (self::$_instance == null)
            self::$_instance = new self;
        return self::$_instance;
    }

    public function translit($string)
    {
        return $this->correctString($string);
    }
    
    public function translitUnique($string, $table = null, $field = null, $id=0)
    {
        $validator = new usEngine_Validate_Unique($table, $field, $id);
        $result = $this->correctString($string);
        $count = 2;
        $result_str = $result;
        while(!$validator->isValid($result_str)) {
            $result_str = $result . '-' . $count;
            $count ++;
        }
        $result = $result_str;
        return $result;
    }
    
    public static function correctString($string)
    {
        $string = trim($string);
        $string = preg_replace('/  +/', ' ', $string);
        $result = strtr($string, self::$translate);
        $result = preg_replace('/[^\-a-z0-9]+/i', "-", $result);
        $result = preg_replace('/--+/', '-', $result);

        if(isset($result[0]) && $result[0] == "-")
            $result = substr($result, 1);
        if (substr($result, -1) == "-")
                $result = substr($result, 0, -1);
        
        return $result;
    }
    
    public static function sluggableString($string)
    {
        $string = trim($string);
        $string = preg_replace('/  +/', ' ', $string);
        $result = strtr($string, self::$translate);
        $result = preg_replace('/[^\-a-z0-9]+/i', "-", $result);
        $result = preg_replace('/--+/', '-', $result);
        
        if($result[0] == "-")
            $result = substr($result, 1);
        if (substr($result, -1) == "-")
                $result = substr($result, 0, -1);
        
        return $result;
    }
}
