<?php
class usEngine_Image_Imagick
{
    protected $image;
    protected $width;
    protected $height;
    protected $bestfit = true;
    protected $path;
    protected $name;
    protected $extension = '.jpg';
    protected $prefix = '';
    protected $round = 10;

    public function __construct($image_path = false)
    {
        if ($image_path) $this->setImage($image_path);
        $this->generateName();
    }

    public function setImage($image_path)
    {
        $this->image = new Imagick();
        $this->image->readImage($image_path);
    }

    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    public function setBestfit($bestfit = true)
    {
        $this->bestfit = $bestfit;
        return $this;
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    public function setRound($round)
    {
        $this->round = $round;
        return $this;
    }

    public function setName($name = false)
    {
        if ($name) $this->name = $name;
        else $this->generateName();
        return $this;
    }

    public function setExtension($extension = '.jpg')
    {
        $this->extension = $extension;
        return $this;
    }

    public function scaleImage()
    {
        $this->image->scaleImage(
            $this->width,
            $this->height,
            $this->bestfit
        );
        return $this;
    }

    public function saveImage()
    {
        $this->image->writeImage($this->path . $this->prefix . $this->name . $this->extension);
        return $this->prefix . $this->name . $this->extension;
    }

    public function destroy()
    {
        $this->image->clear();
        $this->image->destroy();
    }

    protected function generateName($length = 32)
    {
        if ($length > 32 || $length < 8) $length = 32;
        $this->name = substr(md5(microtime().md5(mt_rand(-100, -1)).strrev(microtime())),0,$length);
    }

    public function thumbImage($size)
    {
        $this->image->cropThumbnailImage($size, $size);
    }


    public function resizeImage()
    {
        $geometry = $this->image->getImageGeometry();
        if(($geometry['width'] / $this->width) < ($geometry['height'] / $this->height)) {
            $this->image->cropImage(
                $geometry['width'],
                floor($this->height * $geometry['width'] / $this->width),
                0,
                (($geometry['height'] - ($this->height * $geometry['width'] / $this->width)) / 2)
            );
        } else {
            $this->image->cropImage(
                ceil($this->width * $geometry['height'] / $this->height),
                $geometry['height'],
                (($geometry['width'] - ($this->width * $geometry['height'] / $this->height)) / 2),
                0
            );
        }
        $this->image->ThumbnailImage($this->width, $this->height, true);
        return $this;
    }

    public function roundCorners()
    {
        $canvas = new Imagick();
        $canvas->newImage($this->width, $this->height, new ImagickPixel("white"));

        $this->image->roundCorners($this->round, $this->round);

        $canvas->compositeImage($this->image, $this->image->getImageCompose(),  0,  0);

        $this->image = $canvas;
        return $this;
    }
}