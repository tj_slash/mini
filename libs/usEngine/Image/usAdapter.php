<?php
class usEngine_Image_usAdapter
{
    protected $file = '';
    protected $type = '';
    protected $destination;

    /**
     * $adapter = new usEngine_Image_usAdapter('/images/photo.jpg', '/images/');
     * $adapter->resizeFile(200, 100, 'thumb_');
     */
    function __construct($file = '', $destination = '/')
    {
//        Zend_Debug::dump($file . '---' . $destination); die();
	$this->file = $file;
	$this->type = strtolower(substr($this->file, 1 + strrpos($this->file, ".")));
	if ($this->type == 'jpg') $this->type = 'jpeg';
	$this->destination = $destination;
    }

    public function resizeRectangle($w = 150, $h = 300, $quality = 100, $prefix = 'thumb_')
    {
	$function = 'imagecreatefrom' . $this->type;
	$src = $function($this->destination . $this->file);
	$width = imagesx($src);
	$height = imagesy($src);
	if (isset($w)) {
	    $factor = (float)$w / (float)$width;
	    $w = $factor * $height;
	} else if (isset($h)) {
	    $factor = (float)$h / (float)$height;
	    $h = $factor * $width;
	}
	$dest = imagecreatetruecolor($w, $h);
	imagecopyresampled($dest, $src, 0, 0, 0, 0, $w, $h, $width, $height);
	imageinterlace($dest, 1);
	imagejpeg($dest, $this->destination . $prefix . $this->file, $quality);
	@chmod($this->destination . $prefix . $this->file, 0777);
	imagedestroy($dest);
	imagedestroy($src);
    }

    public function resizeRectangleWidth($w = 150, $quality = 100, $prefix = 'thumb_')
    {
	$function = 'imagecreatefrom' . $this->type;
	$src = $function($this->destination . $this->file);
	$w_src = imagesx($src);
	$h_src = imagesy($src);
	if (($w_src != $w) || ($h_src != $w))
	{
	    $ratio = $w_src/$w;
	    $w_dest = round($w_src/$ratio);
	    $h_dest = round($h_src/$ratio);
	    $dest = imagecreatetruecolor($w_dest, $h_dest);
	    imagecopyresized($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
	    imagejpeg($dest, $this->destination . $prefix . $this->file, $quality);
	    @chmod($this->destination . $prefix . $this->file, 0777);
	    imagedestroy($dest);
	    imagedestroy($src);
	}
    }

    function crop($prefix='') {
	list($w_i, $h_i, $type) = getimagesize($this->destination . $this->file);
        if (!$w_i || !$h_i) {
            die('Невозможно получить длину и ширину изображения');
            return;
        }
        $types = array('','gif','jpeg','png');
        $ext = $types[$type];
        if ($ext) {
            $func = 'imagecreatefrom'.$ext;
            $img = $func($this->destination . $this->file);
        } else {
            die('Некорректный формат файла');
            return;
        }
        if ($w_i > $h_i) {
            $x_o = ($w_i - $h_i) / 2;
            $min = $h_i;
        } else {
            $y_o = ($h_i - $w_i) / 2;
            $min = $w_i;
        }
        $w_o = $h_o = $min;
        $img_o = imagecreatetruecolor($w_o, $h_o);
        imagecopy($img_o, $img, 0, 0, 0, 0, $w_o, $h_o);
        if ($type == 2) {
            imagejpeg($img_o, $this->destination . $this->file,100);
        } else {
            $func = 'image'.$ext;
            $func($img_o, $this->destination . $this->file);
        }
    }

    public function resizeSquare($w_o = 150, $h_o = 150, $quality = 100, $prefix = 'thumb_')
    {
//        $this->crop($prefix);
        list($w_i, $h_i, $type) = getimagesize($this->destination . $this->file);
        if (!$w_i || !$h_i) {
            die('Невозможно получить длину и ширину изображения');
            return;
        }
        $func = 'imagecreatefrom'.$this->type;
    	$img = $func($this->destination . $this->file);
	if (!$h_o) $h_o = $w_o/($w_i/$h_i);
	if (!$w_o) $w_o = $h_o/($h_i/$w_i);
	$img_o = imagecreatetruecolor($w_o, $h_o);
	imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
	if ($this->type == 'jpeg') imagejpeg($img_o,$this->destination . $prefix . $this->file,100);
	else $func = 'image'.$this->type;
	$func($this->destination . $prefix . $this->file);
        @chmod($this->destination . $prefix . $this->file, 0777);
//        imagedestroy($img_o);
//        imagedestroy($img_o);

//	$function = 'imagecreatefrom' . $this->type;
//	$src = $function($this->destination . $this->file);
//	$w_src = imagesx($src);
//	$h_src = imagesy($src);
//	if (($w_src != $w) || ($h_src != $w))
//	{
//	    $dest = imagecreatetruecolor($w, $w);
//	    if ($w_src > $h_src) {
//		imagecopyresized($dest, $src, 0, 0, round((max($w_src, $h_src) - min($w_src, $h_src))/2), 0, $w, $w, min($w_src, $h_src), min($w_src, $h_src));
//	    }
//	    if ($w_src < $h_src) {
//		imagecopyresized($dest, $src, 0, 0, 0, round((max ($w_src, $h_src) - min($w_src, $h_src))/2), $w, $w, min($w_src, $h_src), min($w_src, $h_src));
//	    }
//	    if ($w_src == $h_src) {
//		imagecopyresized($dest, $src, 0, 0, 0, 0, $w, $w, $w_src, $w_src);
//	    }
//	    imagejpeg($dest, $this->destination . $prefix . $this->file , $quality);
//	    @chmod($this->destination . $prefix . $this->file, 0777);
//	    imagedestroy($dest);
//	    imagedestroy($src);
//	}
    }
}
