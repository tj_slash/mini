<?php
class usEngine_Acl_Acl extends Zend_Acl
{
    private static $_config = null;

    public function __construct()
    {
        static::$_config = APPLICATION_PATH . '/configs/acl.ini';
        $this->_addRoles();
        $this->_addResources();
    }

    public function _addResource($role, $resource, $privelege)
    {
        if (!$this->has($resource)) $this->add(new Zend_Acl_Resource($resource));
        $this->allow($role, $resource, $privelege);
    }

    public function _addResources()
    {
        $options = new Zend_Config_Ini(static::$_config, 'acl');
        foreach ($options as $module => $controllers) {
            foreach ($controllers as $controller => $actions) {
                foreach ($actions as $action => $roles) {
                    foreach ($roles as $role => $permissions) {
                        if (!$this->has($module . ':' . $controller)) $this->add(new Zend_Acl_Resource($module . ':' . $controller));
                        if ($permissions == 'allow') $this->allow($role, $module . ':' . $controller, $action);
                        if ($permissions == 'deny') $this->deny($role, $module . ':' . $controller, $action);
                    }
                }
            }
        }
    }

    public function _addRoles()
    {
        $groups = User_Model_GroupTable::getInstance()->getGroups();
        foreach ($groups as $group) {
            if ($group->parent_id > 0) $this->addRole(new Zend_Acl_Role($group->alias), $group->GroupParent->alias);
            else $this->addRole(new Zend_Acl_Role($group->alias));
        }
    }
}