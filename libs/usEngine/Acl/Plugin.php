<?php
class usEngine_Acl_Plugin extends Zend_Controller_Plugin_Abstract
{
    private $_acl = null;

    public function __construct(Zend_Acl $acl)
    {
        $this->_acl = $acl;
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $resource = $request->getModuleName() . ':' . $request->getControllerName();        
        if(!$this->_acl->isAllowed(self::getRole(), $resource, $request->getActionName())) {
            if ($request->getModuleName() === 'admin' || strpos($request->getControllerName(), 'admin') !== false) {
                $request->setModuleName('admin')->setControllerName('index')->setActionName('login');
                return;
            }
            $request->setModuleName('user')->setControllerName('user')->setActionName('denied');
        }
    }

    public static function getRole()
    {
        $key = md5(__METHOD__);
        if (!Zend_Registry::isRegistered($key)) {
            $auth = Zend_Auth::getInstance();
            if($auth->hasIdentity()) {
                $group = User_Model_GroupTable::getInstance()->findOneById($auth->getIdentity()->group_id);
                $role = $group->alias;
            } else {
                $role = 'guest';
            }
            if (APPLICATION_ENV != 'testing') Zend_Registry::set($key, $role);
        } else {
            $role = Zend_Registry::get($key);
        }
        return $role;
    }
}