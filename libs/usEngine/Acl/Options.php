<?php
class usEngine_Acl_Options
{
    private static $instance;

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    protected function initForm()
    {
        $form = new Zend_Form();
        
        $form->setAction("")->setMethod("POST");

        $groups = User_Model_GroupTable::getInstance()->findAll();
        $group_id = new Zend_Form_Element_Select('group_id');
        $group_id->setLabel('Уровень доступа')->setAttrib('size', count($groups)+1);
        $group_id->addMultiOption(null, 'Общий доступ');
        if ($groups) {
            foreach ($groups as $c) {
                $groupid = User_Model_UserTable::getInstance()->findOneByUserId(Zend_Auth::getInstance()->getIdentity()->id)
                    ->UserGroup->id;
                if ($c['id'] > $groupid) continue;
                $group_id->addMultiOption(
                        $c['name'], 
                        html_entity_decode('&nbsp;&nbsp;', ENT_COMPAT, 'UTF-8') . 
                        str_repeat(html_entity_decode('&nbsp;&nbsp;', ENT_COMPAT, 'UTF-8'), $c['parent_id']) . 
                        $c['title']
                );
            }
        }
        $form->addElement($group_id);
        $submit = new Zend_Form_Element_Submit('submit_options');
        $submit->setLabel('Применить')->setAttrib('class', 'submit mid');
        $form->addElement($submit);
        
        return $form;
    }

    /**
     *
     * $this->view->form_options = Page_Model_PageOptions::getInstance()->logicForm('/path/to/config.ini', 'page:' . $id);
     * 
     * @param $configPath
     * @param type $resource
     * @return type 
     */
    public function logicForm($configPath = null, $resource = null)
    {
        $form = $this->initForm();
        $config = new Zend_Config_Ini($configPath, null, array('allowModifications' => true));
        if (Zend_Controller_Front::getInstance()->getRequest()->isPost()) {
            $formdata = Zend_Controller_Front::getInstance()->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                if (!$resource) continue;
                $formdata = $form->getValues();
                if (isset($formdata['group_id'])) {
                    $config->acl->$resource = $formdata['group_id'];
                    @chmod($configPath, 0777);
                    $writer = new Zend_Config_Writer_Ini(array('config' => $config,'filename' => $configPath));
                    $writer->write();
                    @chmod($configPath, 0644);
                }
            }
        }
        if (isset($resource)) {
            if (isset($config->acl->$resource)) {
                $form->getElement('group_id')->setValue($config->acl->$resource);
            }
        }
        return $form;
    }
    
    public function isAllow($configPath = null, $resource = null)
    {
        $config = new Zend_Config_Ini($configPath);
        if (isset($config->acl->$resource)) {
            if((Zend_Registry::isRegistered('Acl'))){
                $acl = Zend_Registry::get('Acl');
                $acl->_addResource($config->acl->$resource, $resource, 'block');
                if(!$acl->isAllowed(usEngine_Acl_Plugin::getRole(), $resource, 'block')) {
                    $request = Zend_Controller_Front::getInstance()->getRequest();
                    $request->setModuleName('user')->setControllerName('user')->setActionName('denied');
                }
            }
        }        
    }
}
