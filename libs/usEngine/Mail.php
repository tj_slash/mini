<?php
/* 
 * Отправка писем в зависимости от APPLICATION_ENV 
 * и опции mail.send в application.ini
 * 
 * Пример: mail.send = true
 * 
 * @category   usEngine
 * @package    usEngine_Mail
 */
class usEngine_Mail extends Zend_Mail
{
    /**
     * Enable or disable send mail
     * @var bool
     */
    protected $_enable = null;
    
    /**
     * Public constructor
     *
     * @param  string $charset
     * @return void
     */
    public function __construct($charset = null) 
    {
        if ($this->_enable === null) {
            $options = Zend_Registry::get('options');
            
            if ((defined('APPLICATION_ENV') && APPLICATION_ENV == 'production')) {
                $this->setEnable(true);
            } elseif (isset($options['mail']['enabled'])) {
                $this->setEnable($options['mail']['enabled']);
            } else {
                $this->setEnable(false);
            }
        }
        parent::__construct($charset);
    }
    
    /**
     * Sends this email using the given transport or a previously
     * set DefaultTransport or the internal mail function if no
     * default transport had been set.
     *
     * @param  Zend_Mail_Transport_Abstract $transport
     * @return Zend_Mail                    Provides fluent interface
     */
    public function send($transport = null) 
    {
        if ($this->isEnable())
            parent::send($transport);
    }
    
    /**
     * set enable or disable send mail
     * 
     * @param bool $value
     */
    public function setEnable($value)
    {
        $this->_enable = (bool) $value;
    }
    
    /**
     * is enabled or disabled send mail
     * 
     * @return bool
     */
    private function isEnable()
    {
        return $this->_enable;
    }
}