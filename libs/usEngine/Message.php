<?php
class usEngine_Message
{
    const SUCCESS_ADD = 'Успешно сохранено!';
    
    const SUCCESS_DELETE = 'Успешно удалено!';
    
    const SUCCESS_EDIT = 'Успешно отредактированно!';
    
    const SUCCESS_PUBLISH = 'Успешно опубликованно / распубликованно!';
    
    const SUCCESS_BLOCK = 'Успешно заблокировано / разблокировано!';
    
    const ERROR_CANT_DELETE_CAT = 'Невозможно удалить категорию! Удалите сначала все ее подкатегории.';
    
    const ERROR_OPERATION = 'Ошибка операции!';
    
    protected static $_instance; 
    
    private function __construct(){}
    
    private function __clone()    {}
    
    private function __wakeup()   {}
    
    public static function getInstance() 
    {
        if (is_null(self::$_instance)) self::$_instance = new self;
        return self::$_instance;
    }
    
    public function setMessage($type = '', $message = '') 
    {
        // type: success - green, error - red, warning - yellow, anyone else - blue
        
        $session = new Zend_Session_Namespace('messages');
        $this->clearMessage();
        if ($type) {
            switch ($type) {
                case 'success_add':
                    $message = self::SUCCESS_ADD;
                    $type = 'success';
                    break;
                case 'success_delete':
                    $message = self::SUCCESS_DELETE;
                    $type = 'success';
                    break;
                case 'success_edit':
                    $message = self::SUCCESS_EDIT;
                    $type = 'success';
                    break;
                case 'success_block':
                    $message = self::SUCCESS_BLOCK;
                    $type = 'success';
                    break;
                case 'success_publish':
                    $message = self::SUCCESS_PUBLISH;
                    $type = 'success';
                    break;
                case 'error_cant_delete_cat':
                    $message = self::ERROR_CANT_DELETE_CAT;
                    $type = 'warning';
                    break;
                
                case 'error_operation':
                    $message = self::ERROR_OPERATION;
                    $type = 'error';
                    break;
            }
        }
        $session->type = $type;
        $session->message = $message;
    }
    
    public function getMessage() 
    {
        $session = new Zend_Session_Namespace('messages');
        if (isset($session->type) && isset($session->message)) {
            $message = array($session->type => $session->message);
            $this->clearMessage();
            return $message;
        }
        return '';
    }
    
    public function clearMessage() 
    {
        $session = new Zend_Session_Namespace('messages');
        if (isset($session->type) && isset($session->message)) {
            $session->__unset('message');
            $session->__unset('type');
        }
    }
}
