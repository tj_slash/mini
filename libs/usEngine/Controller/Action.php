<?php
class usEngine_Controller_Action extends Zend_Controller_Action
{    
    /*
     * Abstarct method for publish
     */
    public function publishAction()
    {
        $id = $this->getRequest()->getParam('id');
        $this->_model->publish($id);
        usEngine_Message::getInstance()->setMessage('success_publish');
        if (isset($_SERVER['HTTP_REFERER'])) $this->_redirect($_SERVER['HTTP_REFERER']);
        else $this->_redirect($this->view->baseUrl() . '/admin');
    }

    /*
     * Abstarct method for archive
     */
    public function archiveAction()
    {
        $id = $this->getRequest()->getParam('id');        
        $this->_model->archive($id);
        usEngine_Message::getInstance()->setMessage('success_delete');
        if (!empty($_SERVER['HTTP_REFERER'])) $this->_redirect($_SERVER['HTTP_REFERER']);
        else $this->_redirect($this->view->baseUrl() . '/admin');
    }

    /*
     * Abstarct method for delete
     */
    public function deleteAction()
    {        
        $id = $this->getRequest()->getParam('id');
        $this->_model->delete($id);
        usEngine_Message::getInstance()->setMessage('success_delete');
        if (!empty($_SERVER['HTTP_REFERER'])) $this->_redirect($_SERVER['HTTP_REFERER']);
        else $this->_redirect($this->view->baseUrl() . '/admin');
    }
    
    public function formatYesNo($yes)
    {
        return $yes ? 'Да' : 'Нет';
    }
    
    public function listArchiveAction()
    {
        $this->_setParam('archive', true);
        $this->_forward('list');
    }
    
    public function editAction()
    {
        $this->_setParam('edit', true);
        $this->_forward('add');
    }
    
    protected function formAction(Zend_Form $form)
    {
        $isEdit = $this->getRequest()->getParam('edit', false);
        
        $formdata = array();
        
        if ($isEdit) {
            $id = $this->getRequest()->getParam('id', null);
            
            if (!$id) {
                usEngine_Message::getInstance()->setMessage('error', 'Неверно указан идентификатор');
                if (!empty($_SERVER['HTTP_REFERER'])) $this->_redirect($_SERVER['HTTP_REFERER']);
                else $this->_redirect($this->view->baseUrl() . '/admin');
            }

            $formdataObject = $this->_model->findOneById($id);
            if (!$formdataObject) {
                if (!empty($_SERVER['HTTP_REFERER'])) $this->_redirect($_SERVER['HTTP_REFERER']);
                else $this->_redirect($this->view->baseUrl() . '/admin');
            }
            
            $formdata = $formdataObject->toArray();
        } elseif($id = $this->getRequest()->getParam('copy_id', null))  {
            if (!$id) {
                usEngine_Message::getInstance()->setMessage('error', 'Неверно указан идентификатор');
                if (!empty($_SERVER['HTTP_REFERER'])) $this->_redirect($_SERVER['HTTP_REFERER']);
                else $this->_redirect($this->view->baseUrl() . '/admin');
            }

            $formdataObject = $this->_model->findOneById($id);
            if (!$formdataObject) {
                if (!empty($_SERVER['HTTP_REFERER'])) $this->_redirect($_SERVER['HTTP_REFERER']);
                else $this->_redirect($this->view->baseUrl() . '/admin');
            }
            
            $formdata = $formdataObject->toArray();
        }
        
        if (method_exists($form, 'preRender')) {
            $formdata = $form->preRender($formdata);
        }
        
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                $data = array();
                
                if (method_exists($form, 'preSave')) {
                    $data = $form->preSave($formdata);
                }
                
                if ($isEdit) {
                    $data['id'] = $id;
                }
                
                $data = array_merge($form->getValues(), $data);
                
                if (method_exists($form, 'unsetSave')) {
                    $data = $form->unsetSave($data);
                }
                
                if ($isEdit) {
                    $item = $this->_model->edit($data);
                } else {
                    $item = $this->_model->add($data);
                }
                
                if (method_exists($form, 'postSave')) {
                    $form->postSave($item, $form);
                }
                
                $this->_redirector($item);
            }
            usEngine_Message::getInstance()->setMessage('error', 'При произошла ошибка!');
            $form->populate($formdata);
        }
        
        $form->populate($formdata);
        $this->view->form = $form;
        $this->_helper->viewRenderer->setRender('add');
    }
    
    protected function formSetting(Zend_Form $form)
    {
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                Index_Model_OptionsTable::getInstance()->updateOptions($form);
                usEngine_Message::getInstance()->setMessage('success_add');
                $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));
            }
            usEngine_Message::getInstance()->setMessage('error', 'При сохранении произошла ошибка!');
            $form->populate($formdata);
        }
        $this->view->form = $form;
        $this->renderScript('admin/add.phtml');
    }
    
    protected function _redirector($item)
    {
        $class = explode('_', str_replace('Controller', '', get_class($this)));
        $link[] = strtolower($class[0]);
        
        $controller = preg_split("/(?=[A-Z])/", $class[1]);
        foreach ($controller as $cntrl) if ($cntrl) $array[] = strtolower($cntrl);
        $link[] =  implode('-', $array);
        
        usEngine_Message::getInstance()->setMessage('success_add');
        
        $front = Zend_Controller_Front::getInstance();
        $params = $front->getRequest()->getParams();
        
        if (isset($params['submit'])) $this->_redirect('/' . implode('/', $link) . '/list');
        else if (isset($params['submit_continue'])) $this->_redirect('/' . implode('/', $link) . '/edit/?id=' . $item->id);
        else if (isset($params['submit_add'])) $this->_redirect('/' . implode('/', $link) . '/add');
        else if (isset($params['submit_copy'])) $this->_redirect('/' . implode('/', $link) . '/add?copy_id=' . $item->id);
    }
}
