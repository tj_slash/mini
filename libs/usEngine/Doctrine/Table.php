<?php
class usEngine_Doctrine_Table extends Doctrine_Table
{    
    public function getTableClass()
    {
        $class = get_class($this);
        return $class::getInstance();
    }

    public function add($formdata)
    {
        $class = str_replace('Table', '', get_class($this));
        $table = new $class();
        foreach(self::getTableClass()->getColumns() as $key => $value) {
            if (isset($formdata[$key]))
                $table->$key = str_replace(array('\'', '\"'), array('&apos;', '&quot;'), $formdata[$key]);
        }
        $table->save();
        return $table;
    }

    public function edit($formdata)
    {
        $table = self::getTableClass()->findOneById($formdata['id']);
        foreach(self::getTableClass()->getColumns() as $key => $value) {
            if (isset($formdata[$key]))
                $table->$key = str_replace(array('\'', '\"'), array('&apos;', '&quot;'), $formdata[$key]);
        }
        $table->save();
        return $table;
    }

    public function adminListing()
    {
        return Doctrine_Query::create()->from(str_replace('Table', '', get_class($this)));
    }

    public function publish($id)
    {
        Doctrine_Query::create()->update(str_replace('Table', '', get_class($this)))->whereIn('id', $id)->set('publish', '? - publish', 1)->execute();
    }

    public function archive($id)
    {
        Doctrine_Query::create()->update(str_replace('Table', '', get_class($this)))->whereIn('id', $id)->set('archive', '? - archive', 1)->execute();
    }
    
    public function delete($id)
    {
        Doctrine_Query::create()->delete(str_replace('Table', '', get_class($this)))->whereIn('id', $id)->execute();
    }

    public function clear()
    {
        Doctrine_Query::create()->delete(str_replace('Table', '', get_class($this)))->execute();
    }

    public function findAllShown()
    {
        return Doctrine_Query::create()
            ->from(str_replace('Table', '', get_class($this)))
            ->where('publish = ?', 1)
            ->andWhere('archive = ?', 0);
    }
}
