<?php
class usEngine_Doctrine_TreeTable extends usEngine_Doctrine_Table
{
    public function getTableClass()
    {
        $class = get_class($this);
        return $class::getInstance();
    }

    public function add($formdata, $show = true)
    {
        $class = str_replace('Table', '', get_class($this));
        $table = new $class();
        if (!empty($formdata['parent_id'])) {
            $root = $this->find($formdata['parent_id']);
            foreach(self::getTableClass()->getColumns() as $key => $value) {
                if (isset($formdata[$key])) $table->$key = $formdata[$key];
            }
            $table->getNode()->insertAsLastChildOf($root);
            $table->save();
        } else {
            foreach(self::getTableClass()->getColumns() as $key => $value) {
                if (isset($formdata[$key])) $table->$key = $formdata[$key];
            }
            $table->save();
            Doctrine_Core::getTable($class)->getTree()->createRoot($table);
        }
        return $table;
    }

    public function edit($formdata, $show = true, $logged = true)
    {
        $parent_id = (isset($formdata['parent_id']) ? $formdata['parent_id'] : null);
        unset($formdata['parent_id']);
        $table = self::getTableClass()->findOneById($formdata['id']);
        foreach(self::getTableClass()->getColumns() as $key => $value) {
            if (isset($formdata[$key]))
                $table->$key = str_replace(array('\'', '\"'), array('&apos;', '&quot;'), $formdata[$key]);
        }
        $table->save();
        if ($parent_id) {
            $current_parentid = $this->getParentNode($formdata['id']);
            if($parent_id != $current_parentid){
                $root = self::getTableClass()->findOneById($parent_id);
                $child = self::getTableClass()->findOneById($formdata['id']); 
                $child->getNode()->moveAsFirstChildOf($root);
                $child->save();
            }
        }
        return $table;
    }

    public function delete($id, $show = true)
    {
        self::getTableClass()->findOneById($id)->getNode()->delete();
        $this->cleanCache();
    }

}