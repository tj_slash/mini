<?php
class usEngine_1c_Catalog
{
    const S_LAST_PRODUCT = 'last_1c_imported_product_num';
    
    const S_LAST_VARIANT = 'last_1c_imported_variant_num';
    
    const S_CATEGORY_MAP = 'categories_mapping';
    
    protected static $instance;
    
    private $_request = null;
    
    private $_start_time = null;
    
    private $_dir = '';
    
    private $_handlerImportCategory = null;
    
    private $_handlerImportProduct = null;
    
    public function __construct() 
    {
        $this->_start_time = microtime(true);
        $max = min(30, @ini_get("max_execution_time"));
        $this->_max_time = !empty($max) ? $max : 30;
        $this->_request = Zend_Controller_Front::getInstance()->getRequest();
    }

    /**
     * @return usEngine_1c_Catalog 
     */
    public static function getInstance() 
    {
        if (is_null(self::$instance)) self::$instance = new self;
        return self::$instance;
    }
    
    public function setDir($dir)
    {
        $this->_dir = $dir;
        return $this;
    }
    
    public function getDir()
    {
        return $this->_dir;
    }
    
    public function setHandlerCategory($handler)
    {
        $this->_handlerImportCategory = $handler;
        return $this;
    }
    
    public function setHandlerProduct($handler)
    {
        $this->_handlerImportProduct = $handler;
        return $this;
    }
    
    public function exchange()
    {
        if ($this->_request->getParam('type') === 'catalog') {
            if ($this->_request->isGet()) {
                $this->checkAuth();
                $this->checkInit();
                $this->checkImport();
            }
            if ($this->_request->isPost()) {
                $this->checkFile();
            }
        }
    }
    
    private function checkAuth()
    {
        if ($this->_request->getParam('mode') === 'checkauth') {
            print "success\n";
            print session_name() . "\n";
            print session_id();
        }
    }
    
    private function checkInit()
    {
        if ($this->_request->getParam('mode') === 'init') {
            $tmp_files = glob($this->_dir . '*.*');
            if (is_array($tmp_files)) {
                foreach ($tmp_files as $tmp_file) {
                    unlink($tmp_file);
                }
            }
            unset($_SESSION[self::S_LAST_PRODUCT]);
            unset($_SESSION[self::S_LAST_VARIANT]); 
            print "zip=no\n";
            print "file_limit=1000000\n";
        }
    }
    
    private function checkFile()
    {
        if ($this->_request->getParam('mode') === 'file' && ($filename = $this->_request->getParam('filename'))) {
            $filename = basename($filename);
            $file = fopen($this->_dir . $filename, 'ab');
            if ($file) {
                $input = file_get_contents('php://input');
                if ($input) {
                    $write = fwrite($file, $input);
                    if ($write) {
                        print "success\n";
                    }
                }
                fclose($file);
            }
        }
    }
    
    private function checkImport()
    {
        if ($this->_request->getParam('mode') === 'import' && ($filename = $this->_request->getParam('filename'))) {
            $filename = basename($filename);
            if ($filename === 'import.xml') {
                Catalog_Model_ProductTable::getInstance()->unpublishAll();
                if (!isset($_SESSION[self::S_LAST_PRODUCT])) {
                    $commerceML = new XMLReader;
                    $commerceML->open($this->_dir . $filename);
                    while ($commerceML->read() && $commerceML->name !== 'Классификатор');
                    $xml = new SimpleXMLElement($commerceML->readOuterXML());
                    $commerceML->close();
                    $this->importCategories($xml);
                }
                $commerceML = new XMLReader;
                $commerceML->open($this->_dir . $filename);
                while ($commerceML->read() && $commerceML->name !== 'Товар');
                $last_product_num = $current_product_num = 0;
                if (isset($_SESSION[self::S_LAST_PRODUCT])) $last_product_num = $_SESSION[self::S_LAST_PRODUCT];
                while ($commerceML->name === 'Товар') {
                    if ($current_product_num >= $last_product_num) {
                        $xml = new SimpleXMLElement($commerceML->readOuterXML());
                        $this->importProduct($xml);
                        $exec_time = microtime(true) - $this->_start_time;
                        if ($exec_time + 1 >= $this->_max_time) {
                            header("Content-type: text/xml; charset=utf-8");
                            print "\xEF\xBB\xBF";
                            print "progress\r\n";
                            print "Выгружено товаров: $current_product_num\r\n";
                            $_SESSION[self::S_LAST_PRODUCT] = $current_product_num;
                            exit();
                        }
                    }
                    $commerceML->next('Товар');
                    $current_product_num++;
                }
                Catalog_Model_BrandTable::getInstance()->countProducts();
                $commerceML->close();
                print "success";
                unset($_SESSION[self::S_LAST_PRODUCT]);
            } elseif($filename === 'offers.xml') {
                $commerceML = new XMLReader;
                $commerceML->open($this->_dir . $filename);
                
                while ($commerceML->read() && $commerceML->name !== 'Предложение');
                
                $last_variant_num = $current_variant_num = 0;
                if (isset($_SESSION[self::S_LAST_VARIANT])) $last_variant_num = $_SESSION[self::S_LAST_VARIANT];
                
                while ($commerceML->name === 'Предложение') {
                    if ($current_variant_num >= $last_variant_num) {
                        $xml = new SimpleXMLElement($commerceML->readOuterXML());
                        //заполняем цену
                        $c_id = $xml->Ид;
                        if($product = Catalog_Model_ProductTable::getInstance()->findOneByC_id($c_id)) {
                            $product->price = $xml->Цены->Цена->ЦенаЗаЕдиницу;
                            $product->save();
                        }
                        $exec_time = microtime(true) - $this->_start_time;
                        if ($exec_time + 1 >= $this->_max_time) {
                            header("Content-type: text/xml; charset=utf-8");
                            print "\xEF\xBB\xBF";
                            print "progress\r\n";
                            print "Выгружено ценовых предложений: $current_variant_num\r\n";
                            $_SESSION[self::S_LAST_VARIANT] = $current_variant_num;
                            exit();
                        }
                    }
                    $commerceML->next('Предложение');
                    $current_variant_num++;
                }
                $commerceML->close();
                print "success";
                unset($_SESSION[self::S_LAST_VARIANT]);
            }
        }
    }
    
    private function importCategories($xml)
    {
        call_user_func_array($this->_handlerImportCategory, array($xml));
    }
    
    private function importProduct($xml)
    {
        call_user_func_array($this->_handlerImportProduct, array($xml));
    }
}