<?php
class usEngine_1c_Exchange 
{
    protected $_dir = null;
    
    protected $_handlerImportCategory = null;
    
    protected $_handlerImportProduct = null;
    
    protected $_handlerOrder = null;
    
    protected $_handlerQuery = null;
    
    public function __construct($dir) 
    {
        $this->setDir($dir);
    }
    
    public function exchange()
    {
        usEngine_1c_Catalog::getInstance()
            ->setDir($this->_dir)
            ->setHandlerCategory($this->_handlerImportCategory)
            ->setHandlerProduct($this->_handlerImportProduct)
            ->exchange();
        usEngine_1c_Sale::getInstance()
            ->setDir($this->_dir)
            ->setHandlerOrder($this->_handlerOrder)
            ->setHandlerQuery($this->_handlerQuery)
            ->exchange();
    }
    
    public function setDir($dir)
    {
        $this->_dir = $dir;
        return $this;
    }
    
    public function setHandlerCategory($handler)
    {
        $this->_handlerImportCategory = $handler;
        return $this;
    }
    
    public function setHandlerProduct($handler)
    {
        $this->_handlerImportProduct = $handler;
        return $this;
    }
    
    public function setHandlerOrder($handler)
    {
        $this->_handlerOrder = $handler;
        return $this;
    }
    
    public function setHandlerQuery($handler)
    {
        $this->_handlerQuery = $handler;
        return $this;
    }
}