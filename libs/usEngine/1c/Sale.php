<?php
class usEngine_1c_Sale
{
    const S_LAST_PRODUCT = 'last_1c_imported_product_num';
    
    const S_LAST_VARIANT = 'last_1c_imported_variant_num';
    
    const S_CATEGORY_MAP = 'categories_mapping';
    
    protected static $instance;
    
    /**
     *
     * @var Zend_Controller_Request_Abstract 
     */
    private $_request = null;
    
    private $_start_time = null;
    
    private $_dir = '';
    
    private $_handlerOrder = null;
    
    private $_handlerQuery = null;
    
    public function __construct() 
    {
        $this->_start_time = microtime(true);
        $max = min(30, @ini_get("max_execution_time"));
        $this->_max_time = !empty($max) ? $max : 30;
        $this->_request = Zend_Controller_Front::getInstance()->getRequest();
//        $log = new Index_Model_Log();
//        $log->message = ('construct Sale '. Zend_Debug::dump($this->_request));
//        $log->save();
    }

    /**
     * @return usEngine_1c_Sale 
     */
    public static function getInstance() 
    {
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    
    public function setDir($dir)
    {
        $this->_dir = $dir;
        return $this;
    }
    
    public function getDir()
    {
        return $this->_dir;
    }
    
    public function setHandlerOrder($handler)
    {
        $this->_handlerOrder = $handler;
        return $this;
    }
    
    public function setHandlerQuery($handler)
    {
        $this->_handlerQuery = $handler;
        return $this;
    }
    
    public function exchange()
    {
//        $log = new Index_Model_Log();
//        $log->message = (' in exchange here ');
//        $log->save();
        if ($this->_request->getParam('type') === 'sale') {
            if ($this->_request->isGet()) {
//                $log = new Index_Model_Log();
//                $log->message = (' is get');
//                $log->save();
                $this->checkAuth();
                $this->checkInit();
                $this->checkQuery();
                $this->checkSuccess();
            }
            if ($this->_request->isPost()) {
//                $log = new Index_Model_Log();
//                $log->message = (' is post');
//                $log->save();
                $this->checkFile();
            }
        }
//        $log = new Index_Model_Log();
//        $log->message = (' in exchange after all');
//        $log->save();
    }
    
    private function checkAuth()
    {
        if ($this->_request->getParam('mode') === 'checkauth') {
            print "success\n";
            print session_name() . "\n";
            print session_id();
        }
    }
    
    private function checkInit()
    {
        if ($this->_request->getParam('mode') === 'init') {
            $tmp_files = glob($this->_dir . '*.*');
            if (is_array($tmp_files)) {
                foreach ($tmp_files as $tmp_file) {
                    unlink($tmp_file);
                }
            }
            print "zip=no\n";
            print "file_limit=1000000\n";
        }
    }
    
    private function checkFile()
    {
        if ($this->_request->getParam('mode') === 'file' && ($filename = $this->_request->getParam('filename'))) {
            $filename = basename($filename);
            $file = fopen($this->_dir . $filename, 'ab');
//            $log = new Index_Model_Log();
//            $log->message = ('filename '.$filename);
//            $log->save();
            if ($file) {
                $input = file_get_contents('php://input');
                if ($input) {
                    $write = fwrite($file, $input);
                    if ($write) {
                        fclose($file);
                        $this->newOrder(simplexml_load_file($this->_dir . $filename));
                    }
                } else {
                    fclose($file);
                }
            }
        }
    }
    
    private function checkQuery()
    {
//        $log = new Index_Model_Log();
//        $log->message = (' in check');
//        $log->save();
        if ( $this->_request->getParam('type') === 'sale' && $this->_request->getParam('mode') === 'query') {
            $result = call_user_func_array($this->_handlerQuery, array());

//        Zend_Debug::dump($result);die("dump \$result");
            if (count($result) >= 1) {
                //$this -> view -> records = $result;

                $docpref="newsletterexport";
                $docDate=date('y-m-d');
                $ext=".xml";
                $docname=$docpref.$docDate.$ext;

                // create XML document

                // create root element
    //            $root = $dom->createElementNS('http://extaz','extaz:document');
    //            $dom->appendChild($root);

                // convert to SimpleXML
                $now = date_create();
                $now = $now->format('Y-m-d');


//                $start = new SimpleXMLElement('<?xml version="1.0" encoding="windosw-1251"'
//                    .'<КоммерческаяИнформация></КоммерческаяИнформация>');

                $xml = simplexml_load_string('<КоммерческаяИнформация></КоммерческаяИнформация>');
                $xml->addAttribute('ВерсияСхемы','11.1.2.25');
                $xml->addAttribute('ДатаФормирования', $now);

                // add resultset elements
                foreach($result as $r){
                    $r = $r[0];
    //                Zend_Debug::dump($r);die("dump \$r");
                    $document = $xml->addChild('Документ');
                    $document->addChild('Ид', $r['id']);
                    $document->addChild('Номер', $r['id']);
                    $r_date = date_create($r['date']);
                    $document->addChild('Дата', $r_date->format('Y-m-d'));
//                    $document->addChild('Дата', '2014-05-06');
                    $document->addChild('ХозОперация', 'Заказ товара');
                    $document->addChild('Роль', 'Продавец');
                    $document->addChild('Валюта', 'руб');
                    $document->addChild('Курс', '1');
                    $document->addChild('Сумма', $r['summ']);
                    $conteragents = $document->addChild('Контрагенты');
                    $conteragent = $conteragents->addChild('Контрагент');
                    $conteragent->addChild('Ид','Розничный покупатель');
                    $conteragent->addChild('Наименование','Розничный покупатель');
                    $conteragent->addChild('ПолноеНаименование','Розничный покупатель');
                    $conteragent->addChild('Роль','Покупатель');

                    // Доп параметры
                    $addr = $conteragent->addChild ('АдресРегистрации');
                    $addr->addChild ( 'Представление', 'Представление');
                    $addrField = $addr->addChild ( 'АдресноеПоле' );
                    $addrField->addChild ( 'Тип', 'Страна' );
                    $addrField->addChild ( 'Значение', 'RU' );
                    $addrField = $addr->addChild ( 'АдресноеПоле' );
                    $addrField->addChild ( 'Тип', 'Регион' );
                    $addrField->addChild ( 'Значение', 'Значение' );

                    $contacts = $conteragent->addChild ( 'Контакты' );
                    $cont = $contacts->addChild ( 'Контакт' );
                    $cont->addChild ( 'Тип', 'Телефон' );
                    $cont->addChild ( 'Значение', 'Значение');
                    $cont = $contacts->addChild ( 'Контакт' );
                    $cont->addChild ( 'Тип', 'Почта' );
                    $cont->addChild ( 'Значение', 'Значение');

                    $now = date_create();
                    $now = $now->format('H:i:s');
                    $document->addChild('Время', $now);
                    $document->addChild('Комментарий');
                    $records = $document->addChild('Товары');
    //                Zend_Debug::dump($r);die("dump \$r");
                    foreach($r['items'] as $item) {
                        $record = $records->addChild('Товар');
        //                Zend_Debug::dump($record);die("dump \$record");
                        $record->addChild('Ид',$item['c_id']);
                        $record->Наименование = $item['title'];
                        $baseEdinica = $record->addChild('БазоваяЕдиница','шт');
                        $record->addChild('ЦенаЗаЕдиницу',$item['price']);
                        $record->addChild('Количество',$item['amount']);
                        $record->addChild('Сумма',$item['price'] * $item['amount']);
                        $requisits = $record->addChild('ЗначенияРеквизитов');
                        $requsit_vid = $requisits->addChild('ЗначениеРеквизита');
                        $requsit_vid->addChild('Наименование','ВидНоменклатуры');
                        $requsit_vid->addChild('Значение','Товар');
                        $requsit_type = $requisits->addChild('ЗначениеРеквизита');
                        $requsit_type->addChild('Наименование','ТипНоменклатуры');
                        $requsit_type->addChild('Значение','Товар');
                    }
                    $doc_requisits = $document->addChild('ЗначенияРеквизитов');
                    $req_method = $doc_requisits->addChild('ЗначениеРеквизита');
                    $req_method->addChild('Наименование','Метод оплаты');
                    $req_method->addChild('Значение','Наличный расчет');
                    $req_is_payed = $doc_requisits->addChild('ЗначениеРеквизита');
                    $req_is_payed->addChild('Наименование','Заказ оплачен');
                    $req_is_payed->addChild('Значение',$r['status'] == 1 ? 'true' : 'false');
                    $req_delivery_allowed = $doc_requisits->addChild('ЗначениеРеквизита');
                    $req_delivery_allowed->addChild('Наименование','Доставка разрешена');
                    $req_delivery_allowed->addChild('Значение',$r['status'] == 1 ? 'true' : 'false');
                    $req_aborted = $doc_requisits->addChild('ЗначениеРеквизита');
                    $req_aborted->addChild('Наименование','Отменен');
                    $req_aborted->addChild('Значение',$r['status'] == 3 ? 'true' : 'false');
                    $req_final = $doc_requisits->addChild('ЗначениеРеквизита');
                    $req_final->addChild('Наименование','Финальный статус');
                    $req_final->addChild('Значение',$r['status'] == 2 ? 'true' : 'false');
                    $req_status = $doc_requisits->addChild('ЗначениеРеквизита');
                    $req_status->addChild('Наименование','Статус заказа');
                    $req_status->addChild('Значение',$r['status']);
                    $req_status_change_date = $doc_requisits->addChild('ЗначениеРеквизита');
                    $req_status_change_date->addChild('Наименование','Дата изменения статуса');
                    $req_status_change_date->addChild('Значение',$r['updated']);
                }//end of foreach
                // saave document
    //            Zend_Debug::dump($xml->saveXML());die("dump ");

//                $xml_string = $xml->saveXML();

//                $index = strpos($xml_string, 'version="1.0 "');

//                $newstring = substr_replace($xml_string, ' encoding="windows-1251" ', 19, 0);
//                echo $newstring;

                header ( "Content-type: text/xml; charset=utf-8" );
                print "\xEF\xBB\xBF";

                print $xml->asXML ();

//                $log = new Index_Model_Log();
//                $log->message = (' after check ');
//                $log->save();
    //            $dom->save('D:/reports/exports/'.$docname.'');
            }
        }
    }
    
    private function checkSuccess()
    {
        if ($this->_request->getParam('mode') === 'success') {
//            $log = new Index_Model_Log();
//            $log->message = (' in success');
//            $log->save();
            $option = Index_Model_OptionsTable::getInstance()->findOneByAlias('last_1c');
            if (empty($option)) {
                $option = new Index_Model_Options();
                $option->alias = 'last_1c';
                $option->title = 'Время последнего обновления заказов 1с';
            }
            $option->value = date("Y-m-d H:i:s");
            $option->save();
        }
    }
    
    private function newOrder($xml)
    {
//        $log = new Index_Model_Log();
//        $log->message = (' in newOrder xml'. $xml);
//        $log->save();
        call_user_func_array($this->_handlerOrder, array($xml));
    }
}
