<?php

/**
 * usEngine_Sender_Transport_Smtp
 * 
 * Класс, реализующий функции передачи электронных писем посредством SMTP-протокола.
 * 
 * @category usEngine
 * @package usEngine_Sender
 * @subpackage Transport
 * @author Anisimov A. <anisalex@list.ru>
 */
class usEngine_Sender_Transport_Smtp extends usEngine_Sender_Transport_Abstract {

    /**
     * Конструктор класса.
     * @param array $options 
     */
    public function __construct(array $options) {
        parent::__construct($options);
    }

    /**
     * Функция передачи письма через протокол SMTP.
     * @param array $config Массив параметров письма. Обязательные поля: mailTo - адресат для отправки, subject - тема письма, emailFrom - адрес отправителя, from - имя отправителя, body - текст письма. В случае отсутствия какого-либо из этих полей выбрасывается Exception'ы.
     */
    public function send(array $config) {
        if(!isset($config['emailTo'])) throw new Zend_Exception ('Не указано поле emailTo для отправки письма');
        if(!isset($config['subject'])) throw new Zend_Exception ('Не указано поле subject для отправки письма');
        if(!isset($config['emailFrom'])) throw new Zend_Exception ('Не указано поле emailFrom для отправки письма');
        if(!isset($config['from'])) throw new Zend_Exception ('Не указано поле from для отправки письма');
        if(!isset($config['body'])) throw new Zend_Exception ('Не указано поле body для отправки письма');
        
        $mail = new Zend_Mail('utf-8');
        $mail->addTo($config['emailTo']);
        $mail->setSubject($config['subject']);
        $mail->setFrom($config['emailFrom'], $config['from']);
        $mail->setBodyHtml($config['body']);
        $mail->send();
    }

    /**
     * Инициализация Zend_Mail_Transport_Smtp и передача его в Zend_Mail. Если application.ini проекта содержит секцию smtp, происходит переопределение стандартного транспорта для Zend_Mail.
     */
    protected function _init() {
        $options = Zend_Registry::get('options');
        if (isset($options['smtp']) && !empty($options['smtp'])) {
            $mailTransport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $options['smtp']);
            Zend_Mail::setDefaultTransport($mailTransport);
        }
    }

}

?>
