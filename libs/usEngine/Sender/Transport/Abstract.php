<?php

/**
 * usEngine_Sender_Transport_Abstract
 * 
 * Абстрактный класс, реализующий функции передачи электронных писем.
 * 
 * @category usEngine
 * @package usEngine_Sender
 * @subpackage Transport
 * @author Anisimov A. <anisalex@list.ru>
 */
abstract class usEngine_Sender_Transport_Abstract {
    
    /**
     * Конструктор класса.
     * @param array $options 
     */
    public function __construct(array $options) {
        $this->_init();
    }
    
    /**
     * Абстрактный метод передачи письма.
     */
    abstract public function send(array $config);
    
    /**
     * Абстрактный метод инициализации параметров класса.
     */
    abstract protected function _init();
}

?>
