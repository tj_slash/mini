<?php
class usEngine_Navigation_Plugin
{
    private static $_instance = null;

    static public function getInstance()
    {
        if (self::$_instance == null)
            self::$_instance = new self;
        return self::$_instance;
    }
    
    public function getPages($menu) {
        $options = new Zend_Config_Ini(APPLICATION_PATH . '/configs/'.$menu.'.ini', 'pages');
        foreach ($options->resource as $module => $controllers) {
            foreach ($controllers as $controller => $actions) {
                foreach ($actions as $action => $label) {
                    $this->_pages[] = array(
                        'module' => $module,
                        'controller' => $controller,
                        'action' => $action,
                        'label' => _($label)
                    );
                }
            }
        }
        return $this->_pages;
    }

}