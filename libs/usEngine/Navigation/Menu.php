<?php
class usEngine_Navigation_Menu extends Zend_Acl
{
    private function addPageInMenu($value)
    {
        $subPages = array();
        foreach ($value as $module => $controllers) {
            foreach ($controllers as $controller => $actions) {
                foreach ($actions as $action => $options) {
                    $subPage['module'] = $module;
                    $subPage['controller'] = $controller;
                    $subPage['action'] = $action;
                    $subPage['resource'] = $module . ':' . $controller;
                    $subPage['privilege'] = $action;
                    foreach($options as $option => $value) {
                        if ($option != 'pages') {
                            $subPage[$option] = $value;
                        } else {
                            $subPage['pages'] = $this->addPageInMenu($value);
                        }
                    }
                    $subPages[] = new Zend_Navigation_Page_Mvc($subPage);
                    unset($subPage);
                }
            }
        }
        return $subPages;
    }

    public function addPages($options = null)
    {
        $pages = array();
        foreach ($options as $module => $controllers) {
            foreach ($controllers as $controller => $actions) {
                foreach ($actions as $action => $options) {
                    $page['module'] = $module;
                    $page['controller'] = $controller;
                    $page['action'] = $action;

                    $page['resource'] = $module . ':' . $controller;
                    $page['privilege'] = $action;
                    foreach($options as $option => $value) {
                        if ($option == 'params') {
                            foreach($value as $k => $v) {
                                $page['params'][$k] = $v;
                                
                            }
                        } else {
                            if ($option != 'pages') {
                                $page[$option] = $value;
                            } else {
                                $page['pages'] = $this->addPageInMenu($value);
                            }
                        }
                    }
                    $pages[] = new Zend_Navigation_Page_Mvc($page);
                    unset($page);
                }
            }
        }
        return new Zend_Navigation($pages);
    }
}