<?php

/**
 * usEngine_Sender
 * 
 * Класс, определяющий транспорт для отправки писем.
 * 
 * @category usEngine
 * @package usEngine_Sender
 * @author Anisimov A. <anisalex@list.ru>
 */
class usEngine_Sender {
    /**
     * Массив параметров класса
     * 
     * @var array $_options
     */
    protected $_options = null;
    
    /**
     * Переменная, хранящая экземпляр класса передачи писем.
     * 
     * @var $_transport
     */
    protected $_transport = null;
    
    /**
     * Инициализация переменных.
     * 
     * @param array $options 
     */
    public function __construct(array $options) {
        $this->_setOptions($options);
        $this->_initTransportClass();
    }
    
    /**
     * Метод вызова пользовательских функций объекта $this->_transport.
     * 
     * @param string $name Имя вызываемой функции.
     * @param array $arguments Параметры вызываемой функции.
     * 
     * @example $s = new usEngine_Sender($options); $s->send($send_params);
     */
    public function __call($name, $arguments) {
        if(method_exists($this->_transport, $name)) call_user_func_array (array($this->_transport, $name), $arguments);
    }
    
    /**
     * Метод, используемый для установки параметров класса
     * @param array $options 
     */
    protected function _setOptions($options) {
        $this->_options = $options;
    }
    
    /**
     * Метод, используемый для инициализации объекта $this->_transport. В случае отсутствия необходимых параметров, вызывает соответствующие Exception'ы.
     */
    protected function _initTransportClass() {
        if(!isset($this->_options['transport'])) throw new Exception('Не указан параметр transport');
        if(!isset($this->_options['transport']['name'])) throw new Exception('Не указан параметр transport.name');
        $name = ucfirst($this->_options['transport']['name']);
        $name = 'usEngine_Sender_Transport_' . $name;
        if(!class_exists($name)) throw new Exception('Не найден класс ' . $name);
        
        $options = array();
        if(isset($this->_options['transport']['options']) && is_array($this->_options['transport']['options'])) {
            $options = $this->_options['transport']['options'];
        }
        $this->_transport = new $name($options);
    }

}

?>
