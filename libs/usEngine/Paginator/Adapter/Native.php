<?php
/**
 * Paginatro Json adapter
 *
 * @author Vakylenko Artem (vakylenkox@gmail.com)
 * @package us2gis
 * @version 1.0
 */
class usEngine_Paginator_Adapter_Native implements Zend_Paginator_Adapter_Interface
{
    /**
     * Items
     *
     * @var array
     */
    protected $_data = null;

    /**
     * Constructor
     *
     * @param array $data
     */
    public function __construct($query)
    {
        $this->_query = $query;
    }

    /**
     * Get offset items
     *
     * @param integer $offset
     * @param integer $itemsPerPage
     * @return array
     */
    public function getItems($offset = null, $itemsPerPage = null)
    {
        if ($itemsPerPage !== null) $this->_query .= 'LIMIT ' . $itemsPerPage . ' ';
        if ($offset !== null) $this->_query .= 'OFFSET ' . $offset . ' ';
        return Doctrine_Manager::getInstance()->connection()->execute($this->_query)->fetchAll();
    }

    /**
     * Count items
     *
     * @return array
     */
    public function count()
    {
        $results = Doctrine_Manager::getInstance()->connection()->execute($this->_query)->fetchAll();
        return count($results);
    }
}