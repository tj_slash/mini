<?php
class usEngine_Validate_Unique extends Zend_Validate_Abstract
{
	const DUPLICATE = 'duplicateValue';

	public $_table;
	public $_field;
        public $_id;

	protected $_messageTemplates = array(
		self::DUPLICATE => "'%value%' уже существует. Попробуйте другое значение."
        );

	public function  __construct($table, $field, $id=0)
	{
		$this->_table = $table;
		$this->_field = $field;
        $this->_id = $id;
	}

	protected function duplicate()
	{
            $method = 'findOneBy' . ucfirst(strtolower($this->_field));
            $table = $this->_table . 'Table';
            $obj = $table::getInstance()->$method($this->_value);
            return $obj != null && $obj->id != $this->_id;
	}

	public function isValid($value)
        {
            $this->_setValue($value);
            
            if ($this->duplicate()) {
                $this->_error(self::DUPLICATE);
                return false;
            }
            return true;
        }
}