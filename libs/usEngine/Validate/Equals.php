<?php
class usEngine_Validate_Equals extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'notMatch';

    private $pwd1;

    private $pwd2;

    protected $_messageTemplates = array(
        self::NOT_MATCH => '"%value%" не совпадает'
    );

    function __construct($password1, $password2)
    {
        $this->pwd1 = $password1;
        $this->pwd2 = $password2;
    }

    public function isValid($value, $context = null)
    {
        if ($context[$this->pwd1] == $context[$this->pwd2]) return true;
        $this->_messageTemplates = array(
            //self::NOT_MATCH => sprintf('"%s" не совпадает с "%s"', $context[$this->pwd1], $context[$this->pwd2])
            self::NOT_MATCH => sprintf('Введенные значения не совпадают')
        );
        $this->_error(self::NOT_MATCH);
        return false;
    }
}