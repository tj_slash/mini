<?php
/**
 * Admin controller for aliases
 *
 * @author Metelkin V. <joc.bro1@gmail.com>
 **/
class usEngine_Validate_Routes extends Zend_Validate_Abstract
{
    const MSG_EXIST = 'msgExist';

    public $_table;
    public $_field;
    public $_id;
    protected $_messageTemplates = array(
        self::MSG_EXIST => "Недопустимый алиас '%value%'. Попробуйте другое значение"
    );

    public function __construct($id = 0) 
    {
        $this->_id = $id;
    }
    
    protected function exists($value) 
    {
        $router = Zend_Controller_Front::getInstance()->getRouter()->getRoutes();
        foreach ($router as $key => $values) {
            $arr[$key] = Zend_Controller_Front::getInstance()->getRouter()->getRoute($key)->getDefaults();
            if (isset($arr[$key]['alias']) && $arr[$key]['alias'] == $value) {
                if ($this->_id != 0) {
                    $pages = Page_Model_PageTable::getInstance()->findOneByAlias($value);
                    if ($pages && $pages->id == $this->_id)
                        return false;
                    else {
                        $category = Obv_Model_CategoryTable::getInstance()->findOneByAlias($value);
                        if ($category && $category->id == $this->_id)
                            return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function isValid($value) {
        $this->_setValue($value);
        if ($this->exists($value)) {
            $this->_error(self::MSG_EXIST);
            return false;
        }
        return true;
    }
}