<?php
class usEngine_Validate_MobilePhone extends Zend_Validate_Abstract
{
    const WRONG_PHONE = 'wrongPhonne';

    protected $_messageTemplates = array(
        self::WRONG_PHONE => "'%value%' - неверный номер телефона"
    );

    public function  __construct()
    {
        
    }

    /**
     * Format mobile phone number
     *
     * @param string $value
     * @return string $phone
     */
    public function formatPhone($value)
    {
        $value = preg_replace("/[^0-9]/", "", $value);
        return preg_replace("/^(8|7)/", "+7", $value);
    }

    /**
     * Valid mobile phone number
     *
     * @param string $value
     * @return boolean
     */
    public function isValid($value)
    {
        $this->_setValue($value);
        if (preg_match('/^\+79[0-9]{9}$/', $this->formatPhone($value))) return true;
        $this->_error(self::WRONG_PHONE);
        return false;
    }
}