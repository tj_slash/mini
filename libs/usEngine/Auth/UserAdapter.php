<?php
class usEngine_Auth_UserAdapter implements Zend_Auth_Adapter_Interface
{
    const USER_BANNED = "Пользователь забаннен или неактивирован";
    
    const ANOTHER_ERROR_MSG = "Неизвестная ошибка";

    const BAD_PW_AND_LOGIN_MSG = "Неверный логин или пароль";

    protected $_user;

    protected $_email = null;

    protected $_password = null;
    
    protected $_remember = null;

    protected $_model = null;
    
    public function  __construct($data)
    {
	$this->_email = $data['email'];
	$this->_password = $data['password'];
        $this->_remember = !empty($data['remember']) ? $data['remember'] : true;
        $this->_model = User_Model_UserTable::getInstance();
    }

    public function authenticate()
    {
	try {
	    $this->_user = $this->_model->getUserForAutentificate($this->_email, $this->_password);
	    $result = new Zend_Auth_Result(Zend_Auth_result::SUCCESS, $this->_user);
            $session = new Zend_Session_Namespace();
            if ($this->_remember) {
                $session->setExpirationSeconds(31*24*3600);
                Zend_Session::rememberMe(31*24*3600);
            }
	    return $this->createResult(Zend_Auth_Result::SUCCESS);
	} catch (Exception $ex) {
	    if ($ex->getMessage() == User_Model_UserTable::USER_BANNED)
                return $this->createResult(Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND, array(self::USER_BANNED));
            elseif ($ex->getMessage() == User_Model_UserTable::WRONG_PW_AND_LOGIN)
                return $this->createResult(Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND, array(self::BAD_PW_AND_LOGIN_MSG));
            else
                return $this->createResult(Zend_Auth_Result::FAILURE, array(self::ANOTHER_ERROR_MSG));
	}
    }

    private function createResult($code, $messages = array())
    {
	return new Zend_Auth_Result($code, $this->_user, $messages);
    }
}
