<?php
class usEngine_Plugin_Acl extends Zend_Controller_Plugin_Abstract 
{
    public function preDispatch(Zend_Controller_Request_Abstract $request) 
    {
        if (Zend_Registry::isRegistered('Acl')) {
            $params = $request->getParams();
            if (!empty($params['module'])) {
                $modulename = $params['module'];
                $config = APPLICATION_PATH . "/modules/{$modulename}/configs/acl.ini";
                if (file_exists($config)) {
                    $backend = new Zend_Config_Ini($config, 'backend');
                    $this->addResources($backend);
                    
                    $frontend = new Zend_Config_Ini($config, 'frontend');
                    $this->addResources($frontend);
                }
            }            
        }
    }
    
    private function addResources($options)
    {
        $acl = Zend_Registry::get('Acl');
        foreach ($options as $module => $controllers) {
            foreach ($controllers as $controller => $actions) {
                foreach ($actions as $action => $roles) {
                    foreach ($roles as $role => $permissions) {
                        if (!$acl->has($module . ':' . $controller)) $acl->add(new Zend_Acl_Resource($module . ':' . $controller));
                        if ($permissions == 'allow') $acl->allow($role, $module . ':' . $controller, $action);
                        if ($permissions == 'deny') $acl->deny($role, $module . ':' . $controller, $action);
                    }
                }
            }
        }
    }
}