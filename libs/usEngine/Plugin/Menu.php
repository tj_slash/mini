<?php
class usEngine_Plugin_Menu extends Zend_Controller_Plugin_Abstract 
{
    protected $_menuLabel = null;
    
    protected $_menuSection = null;

    public function __construct($menuLabel, $menuSection) 
    {
        $this->_menuLabel = $menuLabel;
        $this->_menuSection = $menuSection;
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request) 
    {
        if (Zend_Registry::isRegistered($this->_menuLabel) && !Zend_Registry::isRegistered(__METHOD__)) {
            $front = Zend_Controller_Front::getInstance();
            $modules = $front->getControllerDirectory();
            foreach ($modules as $modulename => $modulepath) {
                $file = APPLICATION_PATH . "/modules/{$modulename}/configs/menu.ini";
                if (file_exists($file)) {
                    $container = Zend_Registry::get($this->_menuLabel);
                    $config = new Zend_Config_Ini($file, $this->_menuSection);
                    $adminPage = $container->findOneBy('id', 'admin');
                    $adminPage->addPages($config);
                    Zend_Registry::set($this->_menuLabel, $container);
                }
            }
            Zend_Registry::set(__METHOD__, true);
        }
    }
}