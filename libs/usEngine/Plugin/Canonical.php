<?php
// В бутстрап
//protected function _initCanonical()
//{
//    $this->getResource('Frontcontroller')->registerPlugin(new usEngine_Plugin_Canonical());
//}
class usEngine_Plugin_Canonical extends Zend_Controller_Plugin_Abstract {

    public function preDispatch(Zend_Controller_Request_Abstract $request) {
        if (!Zend_Registry::getInstance()->isRegistered('canonical_linked')) {
            if (!isset($_SERVER['HTTP_HOST'])) return '';
            //находим каноническую ссылку
            $fc = Zend_Controller_Front::getInstance();
            $router = $fc->getRouter();
	        $this->view = Zend_Layout::getMvcInstance()->getView();

            $request_params = $this->getRequest()->getParams();
//		var_dump($request_params);die(' dump request params');
            // убираем дефолт, форычим на соответствие, возвращаем
            $default_route = null;
            if ($router->hasRoute('default'))
            {
                $default_route = $router->getRoute('default');
                $router->removeRoute('default');
            }
            $routes = $router->getRoutes();

            // установка соответствия роуту
            foreach ($routes as $route) {
                //var_dump($route->getVariables()); continue;
                $defaults = $route->getDefaults();
                $variables = $route->getVariables();
                $get = $this->getRequest()->getQuery();
                //это массив имен параметров, отправленных гетом
                $get = array_keys($get);
                if (isset($request_params['module']) && isset($defaults['module']) && ($request_params['module'] != $defaults['module']))
                    continue;
                if (isset($request_params['controller']) && isset($defaults['controller']) && ($request_params['controller'] != $defaults['controller']))
                    continue;
                if (isset($request_params['action']) && isset($defaults['action']) && ($request_params['action'] != $defaults['action']))
                    continue;
                $have_variable = true;
                //определение пришел ли запрос с параметрами и проверка есть ли эти параметры в переменных роута
                $get_params = array();

                foreach ($request_params as $param => $value) {
                    if ($param != 'module' && $param != 'controller' && $param != 'action') {
                        //для того, чтобы роут НЕ подошел нужно, чтобы в запросе БЫЛ параметр, который пришел не ГЕТОМ, но не находитяс в списке переменных роута
                        if (!in_array($param, $get) && !in_array($param, $variables)) {
                            $get_params[$param] = $value;
                            $have_variable = false;
                            break;
                        }
                    }
                }

                if ($have_variable && !isset($this->view->canonical_url)) {
                    //var_dump(http_build_query($get_params));die;
                    //следующие две строчки нужны для того, чтобы в случае, если роут асембит пустую строку, путь не заканчивался слешем
                    //$get = !empty($get_params) ? '/?'.http_build_query($get_params):'';
                    $tail = $route->assemble($request_params);
                    $tail = (empty($tail)) ? '' : '/' . $tail; //.$get;
                    $this->view->canonical_url = 'http://' . $_SERVER['HTTP_HOST'] . $this->view->baseUrl() . $tail.$this->getGetTail();
                }
            }
            //если путь нее соответствует ни одному роуту, то роутится дефолтным роутом
            $router->addRoute('default',$default_route);
            //этот асембл нужен для того, чтобы убирался в конце слеш например...
            if (!isset($this->view->canonical_url))
                $this->view->canonical_url = 'http://' . $_SERVER['HTTP_HOST'] . @$router->assemble($request_params).$this->getGetTail();

            //$this->view->headLink()->headLink(array('rel' => 'canonical', 'href' => $this->view->canonical_url), 'PREPEND');
        }
    }

    public function postDispatch(Zend_Controller_Request_Abstract $request) {
        //каноникал линк пишется в башку в постдиспатче для того, чтобы в экшоне его можно было поменять
        if (!Zend_Registry::getInstance()->isRegistered('canonical_linked')) {
	        $this->view = Zend_Layout::getMvcInstance()->getView();
            $this->view->headLink(array('rel' => 'canonical', 'href' => $this->view->canonical_url), 'PREPEND');
            Zend_Registry::getInstance()->set('canonical_linked', true);
        }
    }

    private function getGetTail()
    {
        $get = $this->getRequest()->getQuery();
        foreach($get as $param => $value)
        {
            if($param == 'page')
                return ($value != 1) ? '/?page='.$value : '';
        }
        return '';
    }
}