<?php
class usEngine_Plugin_Router extends Zend_Controller_Plugin_Abstract 
{
    protected $_section = null;

    public function __construct($section) 
    {
        $this->_section = $section;
    }

    public function setRoutes($router) 
    {
        if (!Zend_Registry::isRegistered(__METHOD__)) {
            $front = Zend_Controller_Front::getInstance();
            $modules = $front->getControllerDirectory();
            foreach ($modules as $modulename => $modulepath) {
                $file = APPLICATION_PATH . "/modules/{$modulename}/configs/routes.ini";
                if (file_exists($file)) {
                    $config = new Zend_Config_Ini($file, $this->_section);
                    if (count($config) > 0) {
                        $routes = $config->routes->toArray();
                        foreach ($routes as $routename => $routeparams) {
                            $defaults = array();
                            foreach ($routeparams['defaults'] as $key => $value) {
                                $defaults[$key] = $value === '' ? null : $value;
                            }
                            $route = new Zend_Controller_Router_Route($routeparams['route'], $defaults);
                            $router->addRoute($routename, $route);
                        }
                    }
                }
            }
            Zend_Registry::set(__METHOD__, true);
        }
    }
}