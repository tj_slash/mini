<?php
class usEngine_Robokassa_Log_Redis extends Zend_Log_Writer_Abstract
{
    const KEY = 'uERLR';
    
    const LIMIT = 3;
    
    private $_redis = null;
    
    public function __construct($redis = null) 
    {
        if (!empty($redis)) {
            $this->_redis = $redis;
        }
    }
    
    public function setRedis($redis)
    {
        $this->_redis = $redis;
        return $this;
    }
    
    public function getRedis()
    {
        return $this->_redis;
    }

    protected function _write($event) 
    {
        $log = new Rediska_Key_List(self::KEY);
        $log->prepend($event);
        $log->truncate(0, self::LIMIT);
    }

    public static function factory($config) 
    {
        if ($config instanceof Zend_Config) {
            $config = $config->toArray();
        }
        
        if (!is_array($config)) {
            throw new Exception(
                'factory expects an array or Zend_Config instance'
            );
        }
        
        $writer = new self();
        if (!empty($config['redis'])) {
            $writer->setRedis($config['redis']);
        }
        
        return $writer;
    }
}