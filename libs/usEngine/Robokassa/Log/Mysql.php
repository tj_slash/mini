<?php
class usEngine_Robokassa_Log_Mysql extends Zend_Log_Writer_Abstract
{
    private $_table = null;
    
    public function __construct($table = null) 
    {
        if (!empty($table)) {
            $this->_table = $table;
        }
    }
    
    public function setTable($table)
    {
        $this->_table = $table;
        return $this;
    }
    
    public function getTable()
    {
        return $this->_table;
    }

    protected function _write($event) 
    {
        $log = new $this->_table;
//        Zend_Debug::dump($event);die("dump \$event");
        $log->date = $event['timestamp'];
        unset($event['timestamp']);
        $message = '';
        foreach($event as $key => $value) {
            $message .= ' '. $key . ':'. $value;

        }
        $log->message = $message;
        $log->save();
    }

    public static function factory($config) 
    {
        if ($config instanceof Zend_Config) {
            $config = $config->toArray();
        }
        
        if (!is_array($config)) {
            throw new Exception(
                'factory expects an array or Zend_Config instance'
            );
        }
        
        $writer = new self();
        if (!empty($config['table'])) {
            $writer->setTable($config['table']);
        }
        
        return $writer;
    }
}
