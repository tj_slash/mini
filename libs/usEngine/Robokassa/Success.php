<?php
/**
 * Use merchant password 1
 */
class usEngine_Robokassa_Success extends usEngine_Robokassa_Client_Abstract
{
    protected $_params = null;
    
    public function proccess()
    {
        $front = Zend_Controller_Front::getInstance();        
        $this->_params = $front->getRequest()->getParams();
        try {
            if ($this->isValid()) {
                /**
                 * Payment process is successful
                 */
                return true;
            }
        } catch (Exception_Validate $e) {
            $this->_errors = $e->getMessages();
            return false;
        } catch (Exception $e) {
            $this->_errors = array($e->getMessage());
            return false;
        }
    }
    
    private function isValid()
    {
        $errors = array();
        foreach (array('password', 'summ', 'id', 'signatures') as $field) {
            $method = 'isValid' . ucfirst($field);
            if (method_exists($this, $method)) {                        
                if (($error = $this->$method()) !== true) {
                    $errors[$field] = $error;
                }
            }
        }
        if (!empty($errors)) {
            throw new Exception_Validate($errors);
            return false;
        }
        return true;
    }
    
    private function isValidSumm()
    {
        if (!empty($this->_params['OutSum'])) return true;
        return array('OutSum' => array('isEmpty' => 'Value is required and cant be empty'));
    }
    
    private function isValidId()
    {
        if (!empty($this->_params['InvId'])) return true;
        return array('InvId' => array('isEmpty' => 'Value is required and cant be empty'));
    }
    
    private function isValidSignatures()
    {
        if (empty($this->_params['SignatureValue'])) 
            return array('SignatureValue' => array('isEmpty' => 'Value is required and cant be empty'));
        $signature = strtoupper(md5("{$this->_params['OutSum']}:{$this->_params['InvId']}:{$this->_password}"));
        $validators = new Zend_Validate();
        $validators->addValidator(new Zend_Validate_Identical(array('token' => $signature)));
        if ($validators->isValid(strtoupper($this->_params['SignatureValue']))) return true;
        return $validators->getMessages();
    }
}