<?php
/**
 * Use merchant password 1
 */
class usEngine_Robokassa_Client extends usEngine_Robokassa_Client_Abstract
{
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }
    
    public function getId()
    {
        return $this->_id;
    }
    
    public function setSumm($summ)
    {
        $this->_summ = $summ;
        return $this;
    }
    
    public function getSumm()
    {
        return $this->_summ;
    }
    
    public function setDescription($description)
    {
        $this->_description = $description;
        return $this;
    }
    
    public function getDescription()
    {
        return $this->_description;
    }
    
    protected function getSignature()
    {
        return md5("{$this->_login}:{$this->_summ}:{$this->_id}:{$this->_password}");
    }
    
    private function isValidSumm()
    {
        $validators = new Zend_Validate();
        $validators->addValidator(new Zend_Validate_Float());
        $validators->addValidator(new Zend_Validate_NotEmpty());
        if ($validators->isValid($this->_summ)) return true;
        return $validators->getMessages();
    }
    
    private function isValidId()
    {
        $validators = new Zend_Validate();
        $validators->addValidator(new Zend_Validate_NotEmpty());
        $validators->addValidator(new Zend_Validate_Int());
        $validators->addValidator(new Zend_Validate_Between(array('min' => 1, 'max' => 2147483647)));
        if ($validators->isValid($this->_id)) return true;
        return $validators->getMessages();
    }
    
    private function isValidDescription()
    {
        if (!empty($this->_description)) {
            $validators = new Zend_Validate();
            $validators->addValidator(new Zend_Validate_Regex('/^[\a-zA-Zа-яА-Я0-9ёЁ]+$/u'));
            $validators->addValidator(new Zend_Validate_StringLength(array('max' => 100, 'encode' => 'UTF-8')));
            if ($validators->isValid($this->_description)) return true;
            return $validators->getMessages();
        }
        return true;
    }
    
    private function isValid()
    {
        $errors = array();
        foreach (array('login', 'password', 'summ', 'id', 'description') as $field) {
            $method = 'isValid' . ucfirst($field);
            if (method_exists($this, $method)) {                        
                if (($error = $this->$method()) !== true) {
                    $errors[$field] = $error;
                }
            }
        }
        if (!empty($errors)) {
            throw new Exception_Validate($errors);
            return false;
        }
        return true;
    }
    
    public function getPayUrl()
    {
        try {
            if ($this->isValid()) {
                $params = array(
                    'MrchLogin' =>      $this->_login,
                    'OutSum' =>         $this->_summ,
                    'InvId' =>          $this->_id,
                    'Desc' =>           $this->_description,
                    'SignatureValue' => $this->getSignature()
                );
                $query = http_build_query($params);
                $url = "https://auth.robokassa.ru/Merchant/Index.aspx?{$query}";
//                $url = "http://test.robokassa.ru/Index.aspx?{$query}";

                return $url;
            }
        } catch (Exception_Validate $e) {
            $this->_errors = $e->getMessages();
            return false;
        } catch (Exception $e) {
            $this->_errors = array($e->getMessage());
            return false;
        }
    }
}
