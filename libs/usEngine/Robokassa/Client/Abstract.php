<?php
abstract class usEngine_Robokassa_Client_Abstract
{
    protected $_login = null;
    
    protected $_password = null;
    
    protected $_description = '';
    
    protected $_summ = null;
    
    protected $_id = null;
    
    protected $_log = null;
    
    protected $_errors = null;

    public function __construct($login, $password) 
    {
        $this->_login = $login;
        $this->_password = $password;
    }
    
    public function setLogin($login)
    {
        $this->_login = $login;
        return $this;
    }
    
    public function getLogin()
    {
        return $this->_login;
    }
    
    public function setPassword($password)
    {
        $this->_password = $password;
        return $this;
    }
    
    public function getPassword()
    {
        return $this->_password;
    }
    
    public function setLog($log)
    {
        $this->_log = new Zend_Log($log);
        return $this;
    }
    
    public function getLog()
    {
        return $this->_log;
    }
    
    public function getErrors()
    {
        return $this->_errors;
    }
    
    protected function isValidLogin()
    {
        $validators = new Zend_Validate();
        $validators->addValidator(new Zend_Validate_NotEmpty());
        if ($validators->isValid($this->_login)) return true;
        return $validators->getMessages();
    }
    
    protected function isValidPassword()
    {
        $validators = new Zend_Validate();
        $validators->addValidator(new Zend_Validate_NotEmpty());
        if ($validators->isValid($this->_password)) return true;
        return $validators->getMessages();
    }
}

class Exception_Validate extends Exception 
{
    /**
     * Errors messages
     * @var array 
     */
    protected $messages = null;
    
    /**
     * Constructor
     * @param array $messages 
     */
    public function __construct($messages) 
    {
        $this->messages = $messages;
    }
    
    /**
     * Get errors messages
     * @return array 
     */
    public function getMessages()
    {
        return $this->messages;
    }
}