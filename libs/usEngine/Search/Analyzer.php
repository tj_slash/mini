<?php
class usEngine_Search_Analyzer implements Doctrine_Search_Analyzer_Interface
{
    public function analyze($text, $encoding = null)
    {
        $text = trim(strip_tags($text));
        
        $encoding = 'utf-8';
        
        if (strcasecmp($encoding, 'utf-8') != 0 && strcasecmp($encoding, 'utf8') != 0) {
            $text = iconv($encoding, 'UTF-8', $text);
        }

        $text = preg_replace('/[^\p{L}\p{N}]+/u', ' ', $text);
        $text = str_replace('  ', ' ', $text);

        $terms = explode(' ', $text);
        
        $ret = array();
        if ( ! empty($terms)) {
            foreach ($terms as $i => $term) {
                if (empty($term)) continue;
                $lower = mb_strtolower(trim($term), 'UTF-8');
                $ret[$i] = $lower;
            }
        }
        return $ret;
    }
}
