<?php
class usTable_Plugin extends DirectoryIterator
{
    protected $_plugins = array();

    public function getPlugins()
    {
        if (empty($this->_plugins)) {
            $this->loadRecursive();
        }
        return $this->_plugins;
    }

    public function loadRecursive()
    {
        foreach ($this as $file) {
            if ($file->isFile()) {
                $order = 0;
                $classname = 'usTable_Plugin_' . str_replace('.php', '', $file->getFilename());
                if (defined("{$classname}::ORDER")) {
                    $order = constant("{$classname}::ORDER");
                }
                $this->_plugins[$classname] = $order;
            }
        } 
        asort($this->_plugins);
        $i = 0;
        foreach ($this->_plugins as $classname => $order) {
            $this->_plugins[$classname] = $i++;
        }
        $this->_plugins = array_flip($this->_plugins);
    }
}