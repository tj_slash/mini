<?php
class usTable_View_Helper_Tbody extends Zend_View_Helper_Abstract
{
    public function tbody(usTable_Table $table)
    {
        $columns = $table->getColumns();
        $rows = $table->getData();
        $formats = $table->getFormats();
        
        $td = '';
        if(!empty($rows) && count($rows) > 0) {
            foreach($rows as $row) {
                $td .= '<tr>';
                $td .= $table->checkbox($table, $row);
                if ($row instanceof Doctrine_Record) $rowArray = $row->getData();
                if (!empty($columns)) {
                    foreach ($columns as $column => $label) {
                        if (isset($rowArray[$column])) {
                            $td .= '<td>' . $table->format($table, $row, $column) . '</td>';
                        } elseif(isset($formats[$column])) {
                            $td .= '<td>' . $table->format($table, $row, $column) . '</td>';
                        }
                    }
                } else {
                    if ($row instanceof Doctrine_Record) {
                        foreach($rowArray as $key => $value) {
                            if ($table->checkColumn($key)) {
                                $td .= '<td>' . $table->format($table, $row, $key) . '</td>';
                            }
                        }
                    } elseif(is_array($row)) {
                        foreach($row as $key => $value) {
                            if ($table->checkColumn($key)) {
                                $td .= '<td>' . $table->format($table, $row, $key) . '</td>';
                            }
                        }
                    }
                }                
                $td .= $table->action($table, $row);
                $td .= '</tr>';
            }
        }
        return "<tbody>{$td}</tbody>";
    }
}