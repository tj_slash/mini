<?php
class usTable_View_Helper_Tfoot extends Zend_View_Helper_Abstract
{
    public function tfoot(usTable_Table $table)
    {        
        $countColumns = $table->getCountColumns();
        $actions = $table->getActions();
        $checkbox = $table->getCheckbox();
        if (!empty($actions) && count($actions) > 0) {
            $countColumns++;
        }
        if ($checkbox) {
            $countColumns++;
        }
        
        $paginator = $table->paginator($table);
        
        $tf = "<tfoot><tr><td colspan='{$countColumns}'>";
        $tf .= "{$paginator}";
        $tf .= "</td></tr></tfoot>";
        $tf .= "<input type='submit' style='opacity:0'>";
        return $tf;
    }
}