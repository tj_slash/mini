<?php
class usTable_View_Helper_Toolbar extends Zend_View_Helper_Abstract
{
    public function toolbar(usTable_Table $table)
    {        
        return $table->toolbar($table);
    }
}