<?php
class usTable_View_Helper_Thead extends Zend_View_Helper_Abstract
{
    public function thead(usTable_Table $table)
    {
        $columns = $table->getColumns();
        $checkbox = $table->getCheckbox();
        $rows = $table->getData();
        $formats = $table->getFormats();
        $th = '';
        
        if ($checkbox) {
            $th .= "<th><input type='checkbox' class='checkboxAll'></th>";
        }
        
        if(!empty($rows) && count($rows) > 0 && !empty($rows[0]) && $row = $rows[0]) {
            if ($row instanceof Doctrine_Record) $row = $row->getData();
            if (!empty($columns)) {
                foreach ($columns as $column => $label) {
                    if (isset($row[$column])) {
                        $th .= '<th>';
                        $th .= ucfirst($label);
                        $th .= $table->sortable($column);
                        $th .= '</th>';
                    } elseif(isset($formats[$column])) {
                        $th .= '<th>';
                        $th .= ucfirst($label);
                        $th .= '</th>';
                    } else {
                        $th .= '<th>';
                        $th .= ucfirst($label);
                        $th .= '</th>';
                    }
                }
            } else {
                foreach($row as $key => $value) {
                    if ($columnName = $table->checkColumn($key)) {
                        $th .= '<th>';
                        $th .= ucfirst($columnName);
                        $th .= $table->sortable($key);
                        $th .= '</th>';
                    }
                }
            }
        }
        
        $actions = $table->getActions();
        if (!empty($actions) && count($actions) > 0) {
            $th .= '<th style="width:81px">Действия</th>';
        }
        
        $thead = "<thead>";
        $thead .= "<tr>{$th}</tr>";
        //$thead .= $table->filter($table);
        $thead .= "</thead>";
        
        return $thead;
    }
}