<?php
class usTable_View_Helper_Caption extends Zend_View_Helper_Abstract
{
    protected $_caption = null;

    public function caption(usTable_Table $table)
    {
        $caption = $table->getCaption();
        if (!empty($caption)) {
            $this->_caption = $caption;
        } else {
            $rows = $table->getData();
            if(!empty($rows) && $rows instanceof Doctrine_Collection && !empty($rows[0]) && $row = $rows[0]) {
                if ($row instanceof Doctrine_Record) {
                    $this->_caption = get_class($row);
                }
            }
        }
        return "<caption>{$this->_caption}</caption>";
    }
}