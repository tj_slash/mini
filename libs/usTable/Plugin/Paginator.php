<?php
class usTable_Plugin_Paginator
{
    const ORDER = 100;
    
    protected $_paginator = null;

    public static function getRequest()
    {
        return Zend_Controller_Front::getInstance()->getRequest();
    }
    
    public static function getPage()
    {
        return self::getRequest()->getParam('page', 1);
    }
    
    public static function getOffset()
    {
        return self::getRequest()->getParam('offset', 30);
    }
    
    public static function getParams()
    {
        $params = self::getRequest()->getParams();
        if (!empty($params['module'])) unset($params['module']);
        if (!empty($params['controller'])) unset($params['controller']);
        if (!empty($params['action'])) unset($params['action']);
        if (!empty($params['sort'])) unset($params['sort']);
        if (!empty($params['dir'])) unset($params['dir']);
        return $params;
    }
    
    public function execute($data)
    {
        $page = self::getPage();
        $offset = self::getOffset();
        
        if ($data instanceof Doctrine_Query) {
            $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($data);
        } elseif (is_array($data)) {
            $adapter = new Zend_Paginator_Adapter_Array($data);
        }
        
        $this->_paginator = new Zend_Paginator($adapter);
        $this->_paginator->setCurrentPageNumber($page);
        $this->_paginator->setDefaultItemCountPerPage($offset);
        return $this->_paginator;
    }
    
    public function paginator(usTable_Table $table)
    {        
        $transport = $table->getTransport();
        $paginator = $transport->getInputData();
        $paginatorInfo = $this->getPaginatorInfo($paginator);        
        $paginatorOffset = $this->paginatorOffset($paginator);        
        $paginatorPaginator = $this->paginatorPaginator($paginator, $table);        
        return "{$paginatorInfo}{$paginatorOffset}{$paginatorPaginator}";
    }
    
    protected function getPaginatorInfo($paginator)
    {
        $page = self::getPage();
        $count = $paginator->count();
        return "<div>Страница <strong>{$page}</strong> из {$count}</div>";
    }
    
    protected function paginatorOffset($paginator)
    {
        $offset = self::getOffset();
        $html = '<select name="offset" id="offset">';
        foreach (array(1,2,3,5,15,30) as $num) {
            $sel = $num == $offset ? 'selected="selected"' : '';
            $html .= '<option value="' . $num . '" ' . $sel . '>' . $num . '</option>';
        }
        $html .= '</select>';
        return $html;
    }
    
    protected function paginatorPaginator($paginator, usTable_Table $table)
    {
        $view = $table->getView();
        return $view->paginationControl($paginator, 'Sliding', 'Paginator/sliding.phtml');
    }
}