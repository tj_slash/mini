<?php
class usTable_Plugin_Checkbox
{
    const ORDER = 0;
    
    protected $_isCheckBox = true;

    public function checkbox(usTable_Table $table, $row)
    {
        $this->_isCheckBox = $table->getCheckbox();     
        $input = '';
        if ($this->_isCheckBox) {
            $id = false;
            if ($row instanceof Doctrine_Record && !empty($row->id)) $id = $row->id;
            elseif(is_array($row) && !empty($row['id'])) $id = $row['id'];
            if ($id) {
                $input .= "<td>";
                $input .= "<input type='checkbox' name='id[]' class='checkbox' value='{$id}' >";
                $input .= "</td>";
            }
        }   
        return $input;
    }
}