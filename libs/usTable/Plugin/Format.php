<?php
class usTable_Plugin_Format
{
    const ORDER = 0;
    
    protected $_formats = array();

    public function format(usTable_Table $table, $row, $key)
    {
        if (file_exists(realpath(__DIR__) . '/../Format/' . ucfirst($key) . '.php')) {
            if (class_exists('usTable_Format_' . ucfirst($key))) {
                $classname = 'usTable_Format_' . ucfirst($key);
                $class = new $classname;
                return $class->format($row, $key);
            }
        }
        
        $this->_formats = $table->getFormats();
        if (!empty($this->_formats[$key])) {
            if (is_string($this->_formats[$key])) {
                $class = new $this->_formats[$key];
            } elseif (is_array($this->_formats[$key])) {
                list($classname, $methodname) = $this->_formats[$key];
                if (is_object($classname)) {
                    if (method_exists($classname, $methodname)) {
                        return call_user_func_array(array($classname, $methodname), array($row, $key));
                    }
                } elseif (class_exists($classname)) {
                    if (method_exists($classname, $methodname)) {
                        return call_user_func_array(array($classname, $methodname), array($row, $key));
                    }
                }
                $class = $this->_formats[$key];
            } else {
                $class = $this->_formats[$key];
            }
            $value = $class->format($row, $key);
        } else {
            if ($row instanceof Doctrine_Record) {
                $value = $row->$key;
            } elseif(is_array($row)) {
                $value = $row[$key];
            }
        }
        return $value;
    }
}