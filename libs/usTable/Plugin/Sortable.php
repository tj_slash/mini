<?php
class usTable_Plugin_Sortable
{
    const ORDER = 0;
    
    public static function getRequest()
    {
        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        return $request;
    }
    
    public static function getSort()
    {
        return self::getRequest()->getParam('sort', 'id');
    }
    
    public static function getDir()
    {
        return self::getRequest()->getParam('dir', 'DESC');
    }
    
    public static function getParams()
    {
        $params = self::getRequest()->getParams();
        if (!empty($params['module'])) unset($params['module']);
        if (!empty($params['controller'])) unset($params['controller']);
        if (!empty($params['action'])) unset($params['action']);
        if (!empty($params['sort'])) unset($params['sort']);
        if (!empty($params['dir'])) unset($params['dir']);
        return $params;
    }

    public function execute($data)
    {
        $sort = self::getSort();
        $dir = self::getDir();
        if ($data instanceof Doctrine_Query) {
            return $data->orderBy("{$sort} {$dir}");
        } elseif ($data instanceof Zend_Paginator) {
            return $data->orderBy("{$sort} {$dir}");
        } elseif (is_array($data)) {
            foreach ($data as $key => $row) {
                if (!empty($row[$sort])) $sort_column[$key]  = $row[$sort];
            }
            if (!empty($sort_column)) {
                array_multisort($sort_column, ($dir == 'DESC') ? SORT_DESC : SORT_ASC, $data);
            }
            return $data;
        }
        return $data;
    }
    
    public function sortable($key)
    {        
        $sort = self::getSort();
        $dir = self::getDir();
        $params = self::getParams();        
        $params['sort'] = $key;
        
        $class = '';
        $href = http_build_query(array_merge($params, array('dir' => 'DESC')));
        if ($sort === $key) {
            if ($dir === 'ASC') {
                $class = 'desc';
            }
            if ($dir === 'DESC') {
                $href = http_build_query(array_merge($params, array('dir' => 'ASC')));
                $class = 'asc';
            }
        }
        
        return "<a href='?{$href}' class='{$class} sort'>Up</a>";
    }
}