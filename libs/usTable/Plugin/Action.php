<?php
class usTable_Plugin_Action
{
    const ORDER = 0;
    
    const EDIT = 'edit';
    
    const PUBLISH = 'publish';
    
    const UNPUBLISH = 'unpublish';
    
    const ARCHIVE = 'archive';
    
    const DELETE = 'delete';
    
    const RESTORE = 'restore';
    
    const LISTS = 'list';
    
    const LISTS_ARCHIVE = 'list-archive';
    
    protected $_labels = array(
        self::EDIT => 'Редактировать',
        self::PUBLISH => 'Публиковтаь',
        self::UNPUBLISH => 'Распубликовать',
        self::ARCHIVE => 'Архивировать',
        self::DELETE => 'Удалить',
        self::RESTORE => 'Восстановить',
        self::LISTS => 'Список',
        self::LISTS_ARCHIVE => 'Архив'
    );
    
    protected $_actions = array();
    
    public static function getRequest()
    {
        return Zend_Controller_Front::getInstance()->getRequest();
    }

    public function action(usTable_Table $table, $row)
    {
        $this->_actions = $table->getActions();
        $request = self::getRequest();
        
        $actions = array();
        
        if (!empty($this->_actions) && is_array($this->_actions) && count($this->_actions) > 0) {
            foreach($this->_actions as $key => $value) {
                if (is_string($key)) {
                    $module = !empty($value['module']) ? $value['module'] : $request->getModuleName();
                    $controller = !empty($value['controller']) ? $value['controller'] : $request->getControllerName();
                    
                    $params = '';
                    if (!empty($value['params'])) {
                        if ($row instanceof Doctrine_Record) {
                            foreach ($value['params'] as $paramname => $paramvalue) {
                                if (!empty($row->$paramvalue)) {
                                    $params = http_build_query(array($paramname => $row->$paramvalue));
                                }
                            }
                        } elseif(is_array($row)) {
                            foreach ($value['params'] as $paramname => $paramvalue) {
                                if (!empty($row[$paramvalue])) {
                                    $params = http_build_query(array($paramname => $row[$paramvalue]));
                                }
                            }
                        }
                    } else {
                        if ($row instanceof Doctrine_Record) {
                            if (!empty($row->id)) $params = http_build_query(array('id' => $row->id));
                        } elseif(is_array($row)) {
                            if (!empty($row['id'])) $params = http_build_query(array('id' => $row['id']));
                        }
                    }
                    
                    
                    switch ($key) {
                        case self::EDIT: 
                        case self::PUBLISH: 
                        case self::UNPUBLISH: 
                        case self::ARCHIVE: 
                        case self::DELETE: 
                        case self::RESTORE: 
                        case self::LISTS: 
                        case self::LISTS_ARCHIVE: 
                            $action = $key;
                            $title = $this->_labels[$key];
                            break;
                        default:
                            $action = !empty($value['action']) ? $value['action'] : $request->getActionName();
                            $title = $key;
                            break;
                    } 
                    
                    switch ($key) {
                        case self::EDIT: 
                            $class = 'green edit';
                            break;
                        case self::RESTORE: 
                            $class = 'green restore';
                            break;
                        case self::LISTS: 
                            $class = 'blue list';
                            break;
                        case self::LISTS_ARCHIVE: 
                            $class = 'blue list-archive';
                            break;
                        case self::PUBLISH: 
                            $class = 'blue publish';
                            break;
                        case self::UNPUBLISH: 
                            $class = 'blue unpublish';
                            break;
                        case self::ARCHIVE:
                            $class = 'red archive';
                            break;
                        case self::DELETE:
                            $class = 'red delete'; 
                            break;
                        default:
                            $class = 'green';
                            break;
                    } 
                    
                    if (!empty($value['class'])) {
                        $class = $value['class'];
                    }
                    
                    $url = "/{$module}/{$controller}/{$action}";
                    
                    if (!empty($value['icon'])) {
                        $icon = "<img src='/images/admin/{$value['icon']}' alt='{$title}' />";
                    } else {
                        $icon = $title;
                    }
                    
                    $actions[] = "<a href='{$url}?{$params}' title='{$title}' class='{$class}'>{$icon}</a>";
                }
            }
        }
        
        if (count($actions) > 0) {
            $actions = implode('', $actions);
            return "<td>{$actions}</td>";
        }
        
        return '';
    }
}