<?php
class usTable_Plugin_Toolbar
{
    const ORDER = 0;
    
    const ADD = 'add';
    
    const PUBLISH = 'publish';
    
    const UNPUBLISH = 'unpublish';
    
    const ARCHIVE = 'archive';
    
    const DELETE = 'delete';
    
    const RESTORE = 'restore';
    
    const LISTS = 'list';
    
    const LISTS_ARCHIVE = 'list-archive';
    
    protected $_labels = array(
        self::ADD => 'Добавить',
        self::PUBLISH => 'Публиковать',
        self::UNPUBLISH => 'Распубликовать',
        self::ARCHIVE => 'Архивировать',
        self::DELETE => 'Удалить',
        self::RESTORE => 'Восстановить',
        self::LISTS => 'Список',
        self::LISTS_ARCHIVE => 'Архив'
    );
    
    protected $_toolbars = array();
    
    public static function getRequest()
    {
        return Zend_Controller_Front::getInstance()->getRequest();
    }

    public function toolbar(usTable_Table $table)
    {
        $this->_toolbars = $table->getToolbars();
        $request = self::getRequest();
        $toolbars = array();        
        if (!empty($this->_toolbars) && is_array($this->_toolbars) && count($this->_toolbars) > 0) {
            foreach($this->_toolbars as $key => $value) {
                if (is_string($key)) {
                    $module = !empty($value['module']) ? $value['module'] : $request->getModuleName();
                    $controller = !empty($value['controller']) ? $value['controller'] : $request->getControllerName();
                    
                    switch ($key) {
                        case self::ADD: 
                        case self::PUBLISH: 
                        case self::UNPUBLISH: 
                        case self::ARCHIVE: 
                        case self::DELETE: 
                        case self::RESTORE: 
                        case self::LISTS: 
                        case self::LISTS_ARCHIVE: 
                            $action = $key;
                            $title = $this->_labels[$key];
                            break;
                        default:
                            $action = !empty($value['action']) ? $value['action'] : $request->getActionName();
                            $title = $key;
                            break;
                    } 
                    
                    switch ($key) {
                        case self::LISTS: 
                        case self::LISTS_ARCHIVE: 
                            $class = 'blue';
                            break;
                        case self::ADD: 
                        case self::RESTORE: 
                            $class = 'green';
                            break;
                        case self::PUBLISH: 
                        case self::UNPUBLISH: 
                            $class = 'green';
                            break;
                        case self::ARCHIVE:
                        case self::DELETE:
                            $class = 'red'; 
                            $action = $key;
                            $title = $this->_labels[$key];
                            break;
                        default:
                            $class = 'green';
                            break;
                    }                   
                    
                    $url = "/{$module}/{$controller}/{$action}";
                    
                    if (!empty($value['icon'])) {
                        $title = "<img src='/images/admin/{$value['icon']}' alt='{$title}' />";
                    }
                    
                    $toolbars[] = "<li><a href='{$url}' title='{$title}' class='{$class}'>{$title}</a></li>";
                }
            }
        }
        $toolbars = implode('', $toolbars);        
        return "<ul class='usTableToolbar'>{$toolbars}</ul>";
    }
}