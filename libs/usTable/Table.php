<?php
class usTable_Table 
{
    protected $_transport = null;
    
    protected $_data = array();
    
    protected $_view = null;
    
    protected $_caption = null;
    
    protected $_columns = null;
    
    protected $_plugins = array();
    
    protected $_countColumns = 0;
    
    protected $_formats = array();
    
    protected $_filters = array();
    
    protected $_isCheckBox = true;
    
    protected $_actions = array(
        usTable_Plugin_Action::EDIT => array(),
        usTable_Plugin_Action::PUBLISH => array(),
        usTable_Plugin_Action::ARCHIVE => array()
    );
    
    protected $_toolbars = array(
        usTable_Plugin_Toolbar::ADD => array(),
        usTable_Plugin_Toolbar::PUBLISH => array(),
        usTable_Plugin_Toolbar::ARCHIVE => array(),
        usTable_Plugin_Toolbar::DELETE => array(),
        usTable_Plugin_Toolbar::LISTS => array(),
        usTable_Plugin_Toolbar::LISTS_ARCHIVE => array(),
    );

    public function __construct($data)
    {
        $view = $this->setView();
        $view->addHelperPath('usTable/View/Helper', 'usTable_View_Helper');
        $view->addScriptPath(realpath(__DIR__) . '/Templates/');
        $view->headLink()->appendStylesheet($view->baseUrl() . '/css/admin/usTable.css');
        $view->headScript()->prependFile($view->baseUrl() . '/js/admin/usTable.js');
        
        $iterator = new usTable_Plugin(realpath(__DIR__) . '/Plugin/');
        $this->_plugins = $iterator->getPlugins();        
        
        if (is_array($this->_plugins) && count($this->_plugins) > 0) {
            foreach ($this->_plugins as $classname) {
                if (class_exists($classname)) {
                    if (method_exists($classname, 'execute')) {
                        $data = call_user_func_array(array(new $classname, 'execute'), array($data));
                    }
                }
            }
        }
        try {
            if ($data instanceof Doctrine_Query) {
                $this->setTransport(new usTable_Transport_Doctrine($data));
            } else if ($data instanceof Zend_Paginator) {
                $this->setTransport(new usTable_Transport_ZendPaginator($data));
            } else if (is_array($data)) {
                $this->setTransport(new usTable_Transport_Array($data));
            }
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    public function setTransport($transport)
    {
        $this->_transport = $transport;
        return $this;
    }
    
    public function getTransport()
    {
        return $this->_transport;
    }
    
    public function setData($data)
    {
        $this->_data = $data;
        return $this;
    }
    
    public function getData()
    {
        return $this->_data;
    }
    
    public function setCaption($caption)
    {
        $this->_caption = $caption;
        return $this;
    }
    
    public function getCaption()
    {
        return $this->_caption;
    }
    
    public function setView()
    {
        return $this->_view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
    }
    
    public function getView()
    {
        return $this->_view;
    }
    
    public function setColumns($columns)
    {
        $this->_columns = $columns;
        return $this;
    }
    
    public function setColumn($alias, $title)
    {
        $this->_columns[$alias] = $title;
        return $this;
    }
    
    public function getColumns()
    {
        return $this->_columns;
    }
    
    public function getActions()
    {
        return $this->_actions;
    }
    
    public function setAction($label, $options = array())
    {
        $this->_actions[$label] = $options;
        return $this;
    }
    
    public function setActions($flag = true)
    {
        if ($flag == false) $this->_actions = array();
        return $this;
    }
    
    public function removeActions($keys)
    {
        foreach ($keys as $key) {
            unset($this->_actions[$key]);
        }
        return $this;
    }
    
    public function getToolbars()
    {
        return $this->_toolbars;
    }
    
    public function setToolbar($key, $title)
    {
        $this->_toolbars[$key] = $title;
        return $this;
    }
    
    public function setToolbars($flag = true)
    {
        if ($flag == false) $this->_toolbars = array();
        return $this;
    }
    
    public function removeToolbar($key)
    {
        unset($this->_toolbars[$key]);
        return $this;
    }
    
    public function removeToolbars($keys)
    {
        foreach ($keys as $key) {
            unset($this->_toolbars[$key]);
        }
        return $this;
    }
    
    public function getCountColumns()
    {
        return $this->_countColumns;
    }
    
    public function setCountColumns($countColumns) 
    {
        $this->_countColumns = $countColumns;
        return $this;
    }
    
    public function setFormat($columns, $format = null)
    {
        if (is_array($columns)) {
            foreach($columns as $column) {
                list($columnname, $format) = $column;
                $this->_formats[$columnname] = $format;
            }
        } elseif (is_string($columns) && !empty($format)) {
            $this->_formats[$columns] = $format;
        }
        return $this;
    }
    
    public function getFormats()
    {
        return $this->_formats;
    }
    
    public function setFilter($columns, $format = null)
    {
        if (is_array($columns)) {
            foreach($columns as $column) {
                list($columnname, $format) = $column;
                $this->_filters[$columnname] = $format;
            }
        } elseif (is_string($columns) && !empty($format)) {
            $this->_filters[$columns] = $format;
        }
        return $this;
    }
    
    public function getFilters()
    {
        return $this->_filters;
    }
    
    public function setCheckbox($isCheckBox = true)
    {
        $this->_isCheckBox = $isCheckBox;
        return $this;
    }
    
    public function getCheckbox()
    {
        return $this->_isCheckBox;
    }
    
    public function checkColumn($key)
    {
        if (!empty($this->_columns) && is_array($this->_columns)) {
            if (!empty($this->_columns[$key])) return $this->_columns[$key];
            return false;
        }
        return $key;
    }

    public function render()
    {
        $this->setData($this->_transport->getData());        
        
        if (!empty($this->_columns)) {
            $this->setCountColumns(count($this->_columns));
        } else {
            $data = $this->getData();
            if (!empty($data) && !empty($data[0]) && $row = $data[0]) {
                $this->setCountColumns(count($row));
            } 
        }
        
        try {
            return $this->getView()->partial('usTable.phtml', array('table' => $this));
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }        
    }
    
    public function __call($name, $arguments) 
    {
        if (method_exists($this, $name)) {
            return call_user_func_array(array($this, $name), $arguments);
        } else {
            $classname = 'usTable_Plugin_' . ucfirst($name);
            if (class_exists($classname)) {
                if (method_exists($classname, $name)) {
                    return call_user_func_array(array(new $classname, $name), $arguments);
                }
            }
        }
        return false;
    }
    
    public function __toString() 
    {
        return $this->render();
    }
}