<?php
class usTable_Transport_Doctrine 
{
    protected $_inputData = null;

    public function __construct($inputData)
    {
        $this->setInputData($inputData);
    }
    
    public function setInputData($inputData)
    {
        $this->_inputData = $inputData;
    }
    
    public function getData()
    {
        return $this->_inputData->execute();
    }
}