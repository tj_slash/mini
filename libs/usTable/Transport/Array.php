<?php
class usTable_Transport_Array
{
    protected $_inputData = null;

    public function __construct($inputData)
    {
        $this->setInputData($inputData);
    }
    
    public function setInputData($inputData)
    {
        $this->_inputData = $inputData;
    }
    
    public function getInputData()
    {
        return $this->_inputData;
    }
    
    public function getData()
    {
        
        
        return $this->_inputData;
    }
}