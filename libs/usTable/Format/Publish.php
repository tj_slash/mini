<?php
class usTable_Format_Publish
{
    public function format($row, $key)
    {
        if ($row instanceof Doctrine_Record) $row = $row->getData();
        $color = 'red';
        $title = 'Не опубликованно';
        if ($row[$key] == 1) {
            $color = 'green';
            $title = 'Опубликованно';
        }
        return "<font color='{$color}'>{$title}</font>";
    }
}