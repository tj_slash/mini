<?php
class usTable_Format_Date
{
    public function format($row, $key)
    {
        $date = $key;
        if ($row instanceof Doctrine_Record) {
            $date = $row->$key;
        } elseif(is_array($row)) {
            $date = $row[$key];
        }
        $dateFormat = new usEngine_View_Helper_DateFormat();
        return $dateFormat->dateFormat($date);
    }
}