<?php
class usTable_Format_Substring
{
    public function format($row, $key)
    {
        $string = $key;
        if ($row instanceof Doctrine_Record) $row->getData();
        if (isset($row[$key])) $string = $row[$key];
        $substringFormat = new usEngine_View_Helper_Substring();
        return $substringFormat->substring($string);
    }
}