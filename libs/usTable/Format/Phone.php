<?php
class usTable_Format_Phone
{
    public function format($row, $key)
    {        
        $phone = $key;
        if ($row instanceof Doctrine_Record) {
            $phone = $row->$key;
        } elseif(is_array($row)) {
            $phone = $row[$key];
        }
        return "<a href='tel:{$phone}' target='_blank'>{$phone}</a>";
    }
}