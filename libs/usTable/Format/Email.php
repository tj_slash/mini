<?php
class usTable_Format_Email
{
    public function format($row, $key)
    {
        $email = $key;
        if ($row instanceof Doctrine_Record) {
            $email = $row->$key;
        } elseif(is_array($row)) {
            $email = $row[$key];
        }
        return "<a href='mailto:{$email}' target='_blank'>{$email}</a>";
    }
}