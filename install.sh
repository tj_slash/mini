#!/bin/bash

echo 'U are using Nginx or Apache web server? (a/N):'
read answer

DIR=$(pwd)

#Create Virtual Host
echo "Create Virtual Host"

if [ "$answer" != "a" ]; then
    request_filename='$request_filename'
    document_root='$document_root'
    fastcgi_script_name='$fastcgi_script_name'

    cd /etc/nginx/sites-enabled/
    cat <<EOF >> "$1"
    server {
        listen *:80;
        server_name $1;
        root $DIR/public;

        access_log /var/log/nginx/$1-acc.log;
        error_log /var/log/nginx/$1-err.log;

        location / {
            if (!-f $request_filename) {
                 rewrite ^.*$ /index.php last;
            }
        }

        location ~ \.php$ {
            fastcgi_pass unix:/var/run/php5-fpm.sock;
            fastcgi_index index.php;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }
    }
EOF
fi

if [ "$answer" = "a" ]; then
    cat <<EOF >> "$1"
    <VirtualHost *:80>
        ServerAdmin webmaster@localhost
        ServerName $1
        ServerAlias www.$1
        DocumentRoot "$DIR/public"

        <Directory />
            Options All
            AllowOverride All
        </Directory>

        <Directory "$DIR/public">
            Options All
            AllowOverride All
            Order allow,deny
            allow from all
        </Directory>

        ErrorLog /var/log/apache2/$1-err.log
        LogLevel warn
        CustomLog /var/log/apache2/$1-acc.log combined
        ServerSignature On
    </VirtualHost>
EOF
fi

#Edit /etc/hosts
echo "Edit /etc/hosts"
cat <<EOF >> "/etc/hosts"
127.0.0.1 $1 #Automatic script by Vakylenkox
EOF

if [ "$answer" != "a" ]; then
    #Restart Nginx
    echo "Restart Nginx"
    nginx -s reload
fi

if [ "$answer" = "a" ]; then
    #Restart Apache
    echo "Restart Apache"
    /etc/init.d/apache2 restart
fi

echo "Web address: http://$1"