<?php
require_once 'Zend/Test/PHPUnit/ControllerTestCase.php';

abstract class ControllerTestCase extends Zend_Test_PHPUnit_ControllerTestCase
{
    protected $application;
    
    protected static $_config = null;

    protected static $_user = null;

    protected static $_beforeClass = false;

    public static function _setUpBeforeClass()
    {
        self::$_config = new Zend_Config_Ini(APPLICATION_PATH . '/../tests/configs/configs.ini');
        
        try {
            ZFDoctrine_Core::dropDatabases();
        } catch (Exception $exc) {
            
        }
        ZFDoctrine_Core::createDatabases();
        ZFDoctrine_Core::loadModels(APPLICATION_PATH . '/modules/');
        ZFDoctrine_Core::createTablesFromModels();
        ZFDoctrine_Core::loadData(APPLICATION_PATH . '/../tests/fixtures/');
        
        $fc = Zend_Controller_Front::getInstance();
        $acl = new usEngine_Acl_Acl();
        Zend_Registry::set('Acl', $acl);
        $fc->registerPlugin(new usEngine_Plugin_Acl());
        $fc->registerPlugin(new usEngine_Acl_Plugin($acl));
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole(usEngine_Acl_Plugin::getRole());
        
        parent::setUpBeforeClass();
    }
    
    public function setUp()
    {
        if (self::$_beforeClass === false) {
            self::$_beforeClass = true;
            self::_setUpBeforeClass();
        }
        $this->bootstrap = array($this, 'appBootstrap');
        parent::setUp();
    }
    
    public function tearDown()
    {
        parent::tearDown();
    }
    
    public static function tearDownAfterClass()
    {
        parent::tearDownAfterClass();
    }

    public function appBootstrap()
    {
        $this->application = new Zend_Application(
            APPLICATION_ENV,
            array(
                'config' => array(
                    APPLICATION_PATH . '/configs/application.ini',
                    APPLICATION_PATH . '/configs/settings.ini'
                )
            )
        );
        $this->application->bootstrap();
    }
}