<?php
error_reporting(E_ALL | E_STRICT);

defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing'));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../lib'),
    realpath('/usr/share/php'),
    realpath(APPLICATION_PATH . '/..'),
    get_include_path(),
)));

require_once 'Zend/Session.php';
require_once 'Zend/Application.php';
require_once 'Zend/Config/Ini.php';
require_once 'ControllerTestCase.php';

//Zend_Session::start();

$application = new Zend_Application(APPLICATION_ENV,APPLICATION_PATH . '/configs/application.ini');
$application->bootstrap();