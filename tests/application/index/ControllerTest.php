<?php
class Index_ControllerTest extends ControllerTestCase
{
    public function testIndexAction()
    {
        $this->dispatch('/');
        $this->assertModule('index');
        $this->assertController('index');
        $this->assertAction('index');
    }
}