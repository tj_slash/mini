<?php
class News_ControllerTest extends ControllerTestCase
{
    public function testListAction()
    {
        $this->dispatch('/news/news/list');
        $this->assertModule('news');
        $this->assertController('news');
        $this->assertAction('list');
    }
    
    public function testViewAction()
    {
        $this->dispatch('/news/news/view?alias=news-1');
        echo $this->getResponse()->getBody(); 
        $this->assertModule('news');
        $this->assertController('news');
        $this->assertAction('view');
    }
    
}