$(document).ready(function(){
    $('.usTableToolbar a.red').click(function(){
        var table = $(this).parents('form').find('.usTable'), countChecked = 0,
            checkboxes = table.find('input.checkbox'), _this = $(this);
        for (var i=0; i < checkboxes.length; i++) {
            if (checkboxes[i].checked) countChecked++;
        }
        if (countChecked < 1) {
            alert('Необходимо выбрать хотя бы одно значение');
            return false;
        } else {
            if (confirm(_this.text() + ' элемент?')) {
                var action = $(this).attr('href');
                table.parents('form').attr({'action':action}).submit();
                return false;
            }
        }
        return false;
    });
    
    $('.usTable tbody a.archive,.usTable tbody a.delete').click(function(){        
        return confirm($(this).text() + ' элемент?');
    });
    
    $('.checkboxAll').live('click',function(){
        var table = $(this).parents('table'), checkedStatus = this.checked;
        table.find('input.checkbox').each(function() {
            this.checked = checkedStatus;
            if (this.checked) {
                $(this).attr('checked', checkedStatus);
            }else{
                $(this).attr('checked', checkedStatus);
            }
        });	 
    });
    
    var lastChecked = false;
    $('input.checkbox').live('click',function(e){
        if(!lastChecked) {
            lastChecked = this;
            return;
        }
        if(e.shiftKey) {
            var start = $('input.checkbox').index(this);
            var end = $('input.checkbox').index(lastChecked);
            $('input.checkbox').slice(Math.min(start,end), Math.max(start,end)+ 1).attr('checked', lastChecked.checked);
        }
        
        lastChecked = this;	 
    });
    
    $(function(){
        $('#offset').bind('change', function(){
            location.href=window.location.pathname + '/admin/catalog/products/list?offset=' + jQuery(this).val();
        });
    });
});