tinyMCE.addI18n('ru.youtube_dlg',{
    title: 'Вставить/редактировать видео YouTube',
    url_field: 'Ссылка на видео YouTube или код:',
    url_example1: 'Пример URL',
    url_example2: 'Пример кода',
    choose_size: 'Выберите размер',
    custom: 'Пользовательские',
    Width: 'Ширина',
    Height: 'Высота',
    iframe: 'Новый iFrame стиль',
    embed: 'Старый Embeded стиль'
});
