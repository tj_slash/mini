// @name scripts
// @author Dyachkov Dmitriy <dyachkov1991@gmail.com>

tinyMCEPopup.requireLangPack();

var GalleryDialog = {
    elm: false,
    xhr: false,
    step: 0,
    rel: false,
    init: function () {
        if ($(tinyMCEPopup.editor.selection.getNode()).hasClass("gallery-container")) {
            this.elm = $(tinyMCEPopup.editor.selection.getNode());
            $(".gallery-container").html(this.elm.html());
            $(".gallery-container").sortable();
            var rel = $(".gallery-container li a").attr("rel");
            if (!rel) {
                rel = "pp_view[" + Math.random() + "]";
                $(".gallery-container li img").each(function() {
                    $(this).parent("li").append($("<a/>", {rel: rel, href: $(this).attr("src"), target: "_blank"}).append(this));
                });
            }
            this.rel = rel;
        } else {
            this.rel = "pp_view[" + Math.random() + "]";
        }
        $(".gallery-container li").live({
            mouseenter: function() {
                $(this).append($("<span>удалить</span>"));
            }, 
            mouseleave: function() {
                $(this).find("span").remove();
            }
        });
        $(".gallery-container li span").live({
            click: function() {
                $(this).parent().fadeOut("slow", function() {
                    $(this).remove();
                });
            }
        });
    },
    upload: function () {
        var fileList = $("#files").prop("files");
        if (fileList.length == 0)
            return false;
        var file = fileList[this.step];
        var formData = new FormData();
        var parent = this;
        $(".preloader").show();
        formData.append("img_file", file);
        formData.append("alias", "gallery");
        this.xhr = $.ajax({
            url: tinyMCEPopup.editor.getParam("gallery_backend_url"),
            type: "POST",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data) {
            setTimeout(function () {
                parent.step++;
                try {
                    data = eval("(" + data + ")");
                } catch (err) {
                    parent.step = 0;
                    $(".preloader").hide();
                    return false;
                }
                if (data.status == "UPLOADED")  {
                    if($("img[src='" + data.ngx_src + "']").length > 0) {
                        parent.step--;
                    } else {
                        $(".gallery-container").append($("<li/>").append($("<a/>", {rel: parent.rel, href: data.src, target: "_blank"}).append($("<img/>", {src: data.ngx_src}))).hide().fadeIn("slow"));
                        $(".gallery-container").sortable();
                    }
                }
                if (parent.step == fileList.length) {
                    $(".preloader").hide();
                    parent.step = 0;
                    return false;
                }
                parent.upload();
            }, 500);
        });
    },
    insert: function() {
        if (this.elm) 
            this.elm.remove();
        tinyMCEPopup.editor.execCommand('mceInsertContent', false, "<br>" + $('<div>').append($('ul.gallery-container').clone()).remove().html() + "<br>");
        tinyMCEPopup.close();
    },
    close: function() {
        if (this.xhr)
            xhr.abort();
        tinyMCEPopup.close();
    }
};
tinyMCEPopup.onInit.add(GalleryDialog.init, GalleryDialog);


