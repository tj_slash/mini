/**
 * Released under LGPL License.
 *
 * @name editor_plugin
 * @author Dyachkov Dmitriy <dyachkov1991@gmail.com>
 */

(function () {
    tinymce.create('tinymce.plugins.GalleryPlugin', {
        init: function (ed, url) {
            ed.addCommand('mceGallery', function () {
                ed.windowManager.open({
                    file: url + '/index.html',
                    width: 650,
                    height: 350,
                    inline: 1
                }, {
                    plugin_url: url,
                    some_custom_arg: 'custom arg'
                });
            });
            
            ed.addButton('gallery', {
                title: 'gallery.desc',
                cmd: 'mceGallery',
                image: url + '/img/gallery-icon.png'
            });
            
            ed.onNodeChange.add(function (ed, cm, n) {
                var active = false;
                if (n.nodeName == 'LI') {
                    if ($(n).parents().find("ul.gallery-container").length > 0) {
                        active = true;
                    }
                }
                cm.setActive('gallery', active);
            });
        },
        createControl: function (n, cm) {
            return null;
        },
        getInfo: function () {
            return {
                longname: 'Gallery PlugIn',
                author: 'Dyachkov Dmitriy',
                infourl: 'dyachkov1991@gmail.com',
                version: "0.1"
            };
        }
    });
    
    tinymce.PluginManager.add('gallery', tinymce.plugins.GalleryPlugin);
})();