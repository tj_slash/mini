#!/bin/bash
echo 'You want dump your data now? (y/N):'
read answer
if [ "$answer" = "y" ]; then
    zf dump-data doctrine true
    rm application/configs/fixtures/News_Model_NewsIndex.yml
    rm application/configs/fixtures/Catalog_Model_ProductIndex.yml
fi

echo 'You want recreate database `lvk_mini`? (Y/n):'
read answer
if [ "$answer" != "n" ]; then
    mysql -e"DROP DATABASE lvk_mini" -u root -p"qwerty6"
    mysql -e"CREATE DATABASE lvk_mini DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci" -u root -p"qwerty6"
fi

zf generate-models-from-yaml doctrine
zf create-tables doctrine

if [ "$answer" != "n" ]; then
    zf load-data doctrine
fi