<?php
header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL | E_STRICT);

print "Starting...\n";

defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'console'));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath( APPLICATION_PATH . '/../lib' ),
    realpath( APPLICATION_PATH . '/../libs' ),
    realpath( APPLICATION_PATH . '/..' ),
    get_include_path()
)));

require_once 'Zend/Application.php';
$application = new Zend_Application(
    APPLICATION_ENV,
    array(
        'config' => array(
            APPLICATION_PATH . '/configs/application.ini'
        )
    )
);
$application->bootstrap();

News_Model_NewsTable::getInstance()->indexSearch();
Catalog_Model_ProductTable::getInstance()->indexSearch();