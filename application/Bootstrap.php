<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected function _initCache()
    {
        $options = $this->getOptions();
        if (!empty($options['cache'])) {
            $fOptions = $options['cache']['frontend'];
            $bOptions = $options['cache']['backend'];
            $cache = Zend_Cache::factory('Core', 'File', $fOptions, $bOptions);
            Zend_Registry::set('cache', $cache);
        }
    }
    
    protected function _initResources()
    {
        $loader = new Zend_Loader_Autoloader_Resource(array(
            'basePath' => APPLICATION_PATH . '/modules/index/',
            'namespace' => 'Index'
        ));
        $loader->addResourceTypes(array(
            'forms' => array(
                'path' => 'forms',
                'namespace' => 'Forms'
            )
        ));
    }

    protected function _initModuleLoaders()
    {
        $this->bootstrap('Frontcontroller');
        $fc = $this->getResource('Frontcontroller');
        $modules = $fc->getControllerDirectory();
        foreach ($modules AS $module => $dir) {
            $moduleName = strtolower($module);
            $moduleName = str_replace(array('-', '.'), ' ', $moduleName);
            $moduleName = ucwords($moduleName);
            $moduleName = str_replace(' ', '', $moduleName);
            $loader = new Zend_Application_Module_Autoloader(array(
                'namespace' => $moduleName,
                'basePath' => realpath($dir . "/../"),
            ));
        }
    }
    
    private function stringToArray($config, $key, $value) {
        if (strpos($key, '.') !== false) {
            $pieces = explode('.', $key, 2);
            if (strlen($pieces[0]) && strlen($pieces[1])) {
                if (!isset($config[$pieces[0]])) $config[$pieces[0]] = array();
            }
            $config[$pieces[0]] = $this->stringToArray($config[$pieces[0]], $pieces[1], $value);
        } else {
            $config[$key] = $value;
        }
        return $config;
    }

    protected function _initOptions()
    {
        $this->bootstrap('doctrine');
        $options = $this->getOptions();
        if (defined('APPLICATION_ENV') && APPLICATION_ENV != 'testing') {
            $_options = Index_Model_OptionsTable::getInstance()->findAll();
            foreach($_options as $_option) {
                if (strpos($_option->alias, '.') !== false) {
                    $pieces = explode('.', $_option->alias, 2);
                    if (strlen($pieces[0]) && strlen($pieces[1])) {
                        if (!isset($options[$pieces[0]])) $options[$pieces[0]] = array();
                    }
                    $options[$pieces[0]] = $this->stringToArray($options[$pieces[0]], $pieces[1], $_option->value);
                } else {
                    $options[$_option->alias] = $_option->value;
                }
            }
        }
        Zend_Registry::set('options', $options);
        return $options;
    }
    
    protected function _initView()
    {
        $options = Zend_Registry::get('options');
        
        $view = new Zend_View();
        $view->addHelperPath('usEngine/View/Helper', 'usEngine_View_Helper');
        $view->addHelperPath('ZFDoctrine/View/Helper', 'ZFDoctrine_View_Helper');
        $view->doctype('HTML5');
        
        if (defined('APPLICATION_ENV') && APPLICATION_ENV != 'testing') {
            $view->headMeta()->appendName('keywords', $options['setting']['index']['meta_keys']);
            $view->headMeta()->appendName('description', $options['setting']['index']['meta_desc']);
            $view->headTitle($options['setting']['index']['meta_title']);
            $view->headTitle()->setSeparator(' / ');
            $view->headLink(array('rel' => 'shortcut icon', 'href' => $view->baseUrl('/media/images/icon.ico'), 'APPEND'));
        }     
        
        $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=UTF-8');
        
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
        $request = new Zend_Controller_Request_Http();
        $front->setRequest($request);
        
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view);
        $view->setScriptPath(APPLICATION_PATH . '/templates/');
        $view->addScriptPath(APPLICATION_PATH . '/templates/' . $options['resources']['layout']['template']);
        return $view;
    }

    protected function _initRoutes()
    {        
        if (!defined('APPLICATION_ENV') || APPLICATION_ENV == 'console') return;
        $this->bootstrap('Frontcontroller');
        $fc = $this->getResource('Frontcontroller');
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/routes.ini');
        $router = $fc->getRouter();
        $router->addConfig($config, 'routes');
        
        $plugin = new usEngine_Plugin_Router('frontend');
        $plugin->setRoutes($router);
    }

    protected function _initSession()
    {
        if (!defined('APPLICATION_ENV') || APPLICATION_ENV == 'console') return;
        $options = Zend_Registry::get('options');
        Zend_Session::setOptions(array('name' => $options['cookie_name']));
    }
    
    protected function _initAcl()
    {
        if (!defined('APPLICATION_ENV') || APPLICATION_ENV == 'testing' || APPLICATION_ENV == 'console') return;
        $fc = $this->getResource('Frontcontroller');
        $acl = new usEngine_Acl_Acl();
        Zend_Registry::set('Acl', $acl);
        
        $fc->registerPlugin(new usEngine_Plugin_Acl());
        $fc->registerPlugin(new usEngine_Acl_Plugin($acl));
        
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole(usEngine_Acl_Plugin::getRole());
        
        return $acl;
    }

    protected function _initUserMenu()
    {
        if (!defined('APPLICATION_ENV') || APPLICATION_ENV == 'testing' || APPLICATION_ENV == 'console') return;
        $this->bootstrapView();
        $view = $this->getResource('view');
        $pages = array();
        $items = new Zend_Config_Ini(APPLICATION_PATH . '/configs/menu.ini', 'userMenu');
        foreach ($items as $item) $pages[] = new Zend_Navigation_Page_Mvc($item->toArray());
        $container = new Zend_Navigation($pages);
        $view->userMenu = $container;
    }

    protected function _initHeaderMenu()
    {
        if (!defined('APPLICATION_ENV') || APPLICATION_ENV == 'testing' || APPLICATION_ENV == 'console') return;
        $this->bootstrapView();
        $view = $this->getResource('view');
        $pages = array();
        $items = new Zend_Config_Ini(APPLICATION_PATH . '/configs/menu.ini', 'headerMenu');
        foreach ($items as $item) $pages[] = new Zend_Navigation_Page_Mvc($item->toArray());
        $container = new Zend_Navigation($pages);
        $view->headerMenu = $container;
    }
    
    protected function initRobokassa()
    {

//        if(!Zend_Registry::isRegistered('options')) $this->_initOptions();
//        $options = Zend_Registry::get('options');
//        $result = new usEngine_Robokassa_Result($options['robokassa']['login'], $options['robokassa']['login']);
//        if (($status = $result->proccessResult()) === false) {
//            Zend_Debug::dump($result->getErrors());
//        }
//        Zend_Debug::dump($status);
//        die();

//        $options = Zend_Registry::get('options');
//        $client = new usEngine_Robokassa_Client('amurnetTest', 'zxcASDqwe123'); //zxcFGHqwe456
//        $client->setSumm(100.00);
//        $client->setId(1);
//        $client->setLog(new usEngine_Robokassa_Log_Mysql('Index_Model_Log'));
//        if (($url = $client->getPayUrl()) === false) {
//            Zend_Debug::dump($client->getErrors());
//            die(' died in errors');
//        }
//        Zend_Debug::dump($url);
//        die();
//
//
//
//        $client = new usEngine_Robokassa_Client('amurnetTest', 'zxcASDqwe123'); //zxcFGHqwe456
//        $client->setSumm(100.00);
//        $client->setId(1);
//        $client->setLog(new usEngine_Robokassa_Log_Mysql('Index_Model_LogRobokassa'));
//        if (($url = $client->getPayUrl()) === false) {
//            Zend_Debug::dump($client->getErrors());
//        }
//        Zend_Debug::dump($url);
//        die();
    }

    protected function _initAdminMenu()
    {
        if (!defined('APPLICATION_ENV') || APPLICATION_ENV == 'testing' || APPLICATION_ENV == 'console') return;
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity();
            if ($user->group_id > 1) {
                $this->bootstrapView();
                $view = $this->getResource('view');
                $fc = $this->getResource('Frontcontroller');
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/menu.ini', 'backend');
                $container = new Zend_Navigation($config);
                Zend_Registry::set('adminMenu', $container);
                $fc->registerPlugin(new usEngine_Plugin_Menu('adminMenu', 'backend'));        
                $view->adminMenu = Zend_Registry::get('adminMenu');
            }
        }
    }
    
    protected function _initMail()
    {
        if (!defined('APPLICATION_ENV')) return;
        $options = Zend_Registry::get('options');
        if (isset($options['smtp']) && !empty($options['smtp'])) {
            $mailTransport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $options['smtp']);
            Zend_Mail::setDefaultTransport($mailTransport);
        }
    }
//    
//    protected function _initZFDebug() 
//    {
//        if (!defined('APPLICATION_ENV') || APPLICATION_ENV == 'testing' || APPLICATION_ENV == 'console') return;
//        $fc = Zend_Controller_Front::getInstance();
//        $get = $fc->getRequest()->getQuery();
//        $get = array_keys($get);
//        $options = $this->getOptions();
//        if (isset($options['zfdebug']['enabled']) && $options['zfdebug']['enabled'] && in_array("zfdebug",$get)) {
//            $autoloader = Zend_Loader_Autoloader::getInstance();
//            $autoloader->registerNamespace('ZFDebug');
//            $autoloader->registerNamespace('Danceric');
//            
//            $debug = new ZFDebug_Controller_Plugin_Debug($options['zfdebug']);
//            $frontController = $this->getResource('frontController');
//            $frontController->registerPlugin($debug);
//        }
//    }
//    
//    protected function _initCanonical()
//    {
//        if (!defined('APPLICATION_ENV') || APPLICATION_ENV == 'testing' || APPLICATION_ENV == 'console') return;
//        $this->getResource('Frontcontroller')->registerPlugin(new usEngine_Plugin_Canonical());
//    }
}
