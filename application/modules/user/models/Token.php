<?php
class User_Model_Token extends User_Model_Base_Token
{
    public function setToken($token)
    {
        return $this->_set('token', uniqid());
    }
    
    public function preSave($event)
    {
        $this->_set('token', uniqid());
    }
    
}