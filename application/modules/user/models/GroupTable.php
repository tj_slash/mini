<?php
class User_Model_GroupTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('User_Model_Group');
    }

    public function getGroups()
    {
         return Doctrine_Query::create()->from('User_Model_Group')->orderBy('parent_id ASC')->execute();
    }
}