<?php
class User_Model_TokenTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('User_Model_Token');
    }
    
    public function generateToken(User_Model_User $user)
    {
        $token = new User_Model_Token;
        $token->user_id = $user->id;
        $token->save();
        return $token->token;
    }
}