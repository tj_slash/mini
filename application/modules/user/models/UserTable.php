<?php
class User_Model_UserTable extends usEngine_Doctrine_Table
{
    const USER_BANNED = 1;

    const WRONG_PW_AND_LOGIN = 2;
    
    const CHARS = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890";
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('User_Model_User');
    }
    
    public function registration($data)
    {
        if (empty($data['nick'])) $data['nick'] = $data['email'];
        
        $send['password'] = $data['password'];
        
        $data['password'] = self::cryptPassword($data['password']);
        
        $user = self::getInstance()->add($data);
        
        $send['token'] = User_Model_TokenTable::getInstance()->generateToken($user);
        $send['email'] = $data['email'];
        $this->sendVerification($send);
        
        return $user;
    }
    
    public function verification(User_Model_Token $token)
    {
        $userGroup = User_Model_GroupTable::getInstance()->findOneByAlias('user');
        
        $user = $token->User;
        $user->group_id = $userGroup->id;
        $user->save();
        
        $token->delete();
        
        $this->sendSignup($user);
        
        return $user;
    }
    
    public function verificationForgot(User_Model_Token $token)
    {
        $userGroup = User_Model_GroupTable::getInstance()->findOneByAlias('user');
        
        $user = $token->User;
        $password = $this->generatePassword();
        $user->password = self::cryptPassword($password);
        $user->save();
        
        $token->delete();
        
        $this->sendForgotSuccess($user, $password);
        
        return $user;
    }
    
    public function forgot(User_Model_User $user)
    {        
        $send['token'] = User_Model_TokenTable::getInstance()->generateToken($user);
        $send['email'] = $user->email;
        
        $this->sendForgot($send);
        
        return $user;
    }

    public static function cryptPassword($password)
    {
        $salt  = substr(md5(time()), 0, 8);
        return self::getCryptedPassword($password, $salt) . ':' . $salt;
    }
    
    public static function getCryptedPassword($password, $salt)
    {
        return md5($password . $salt);
    }
    
    private static function sendVerification($data)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        
        $front = Zend_Controller_Front::getInstance();
        $server_name = $front->getRequest()->getServer('SERVER_NAME');
        $view->url = "http://{$server_name}/verification/{$data['token']}";
        
        $body = $view->render('email/verification.phtml');
        
        
        $mail = new Zend_Mail('utf-8');
        $mail->addTo($data['email']);
        $mail->setSubject('Подтверждение регистрации!');
        $mail->setFrom('support@amur.net', 'Amurnet');
        $mail->setBodyHtml($body);
        $mail->send();
        return true;
    }
    
    private static function sendForgot($data)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        
        $front = Zend_Controller_Front::getInstance();
        $server_name = $front->getRequest()->getServer('SERVER_NAME');
        $view->url = "http://{$server_name}/forgot-verification/{$data['token']}";
        
        $body = $view->render('email/forgot.phtml');
        
        $mail = new Zend_Mail('utf-8');
        $mail->addTo($data['email']);
        $mail->setSubject('Восстановление пароля!');
        $mail->setFrom('support@amur.net', 'Amurnet');
        $mail->setBodyHtml($body);
        $mail->send();
        return true;
    }
    
    private static function sendSignup(User_Model_User $user)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        
        $body = $view->render('email/signup.phtml');
        
        $mail = new Zend_Mail('utf-8');
        $mail->addTo($user->email);
        $mail->setSubject('Регистрация на сайте!');
        $mail->setFrom('support@amur.net', 'Amurnet');
        $mail->setBodyHtml($body);
        $mail->send();
        return true;
    }
    
    private static function sendForgotSuccess(User_Model_User $user, $password)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        $view->password = $password;
        $body = $view->render('email/forgot-success.phtml');
        
        $mail = new Zend_Mail('utf-8');
        $mail->addTo($user->email);
        $mail->setSubject('Новый пароль!');
        $mail->setFrom('support@amur.net', 'Amurnet');
        $mail->setBodyHtml($body);
        $mail->send();
        return true;
    }
    
    public function authentication($data)
    {
        $adapter = new usEngine_Auth_UserAdapter($data);
        return Zend_Auth::getInstance()->authenticate($adapter);
    }
    
    public function getUserForAutentificate($email, $password)
    {
        
        $user = $this->findOneByEmail($email);
        if ($user) {
            if ($user->UserGroup->alias != 'guest') {
                $parts = explode(':', $user->password);
                $crypt = $parts[0];
                $salt = $parts[1];
                $testcrypt = self::getCryptedPassword($password, $salt);
                if ($crypt == $testcrypt) {
                    return $user;
                }
                throw new Exception(self::WRONG_PW_AND_LOGIN);
            }
            throw new Exception(self::USER_BANNED);
        }
        throw new Exception(self::WRONG_PW_AND_LOGIN);
    }
    
    private function generatePassword($length = 6)
    {
        $size = strlen(self::CHARS) - 1;
        $chars = self::CHARS;
        $password = null;
        while($length--) $password .= $chars[rand(0, $size)];
        return $password;
    }
}