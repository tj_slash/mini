<?php

/**
 * User_Model_Base_User
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $nick
 * @property integer $group_id
 * @property User_Model_Group $UserGroup
 * @property Doctrine_Collection $News
 * @property Doctrine_Collection $Comment
 * @property Doctrine_Collection $KuponCode
 * @property Doctrine_Collection $Token
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class User_Model_Base_User extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('user__model__user');
        $this->hasColumn('id', 'integer', 11, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'length' => '11',
             ));
        $this->hasColumn('email', 'string', 64, array(
             'type' => 'string',
             'notnull' => true,
             'length' => '64',
             ));
        $this->hasColumn('password', 'string', 64, array(
             'type' => 'string',
             'notnull' => true,
             'length' => '64',
             ));
        $this->hasColumn('nick', 'string', 64, array(
             'type' => 'string',
             'notnull' => true,
             'length' => '64',
             ));
        $this->hasColumn('group_id', 'integer', 1, array(
             'type' => 'integer',
             'default' => 1,
             'length' => '1',
             ));

        $this->option('collate', 'utf8_general_ci');
        $this->option('charset', 'utf8');
        $this->option('type', 'InnoDB');
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('User_Model_Group as UserGroup', array(
             'local' => 'group_id',
             'foreign' => 'id',
             'owningSide' => true));

        $this->hasMany('News_Model_News as News', array(
             'local' => 'id',
             'foreign' => 'author_id'));

        $this->hasMany('Comment_Model_Comment as Comment', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $this->hasMany('Catalog_Model_KuponCode as KuponCode', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $this->hasMany('User_Model_Token as Token', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'created' => 
             array(
              'name' => 'created',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
             ),
             'updated' => 
             array(
              'name' => 'updated',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
             ),
             ));
        $this->actAs($timestampable0);
    }
}