<?php
class User_AdminGroupController extends usEngine_Controller_Action
{    
    public function init()
    {
        $this->_model = User_Model_GroupTable::getInstance();
        $this->_helper->layout->setLayout('admin_content');
    }
    
    public function listAction()
    {
        $grid = new usTable_Table($this->_model->adminListing());
        $grid->setCaption('Группы доступа')->setToolbars(false)->setActions(false);
        $grid->setToolbar(usTable_Plugin_Toolbar::DELETE, array());
        $grid->setToolbar(usTable_Plugin_Toolbar::ADD, array());
        $grid->setAction(usTable_Plugin_Action::EDIT, array());
        $grid->setAction(usTable_Plugin_Action::DELETE, array());
        $grid->setColumns(array('id'=>'ID','title'=>'Наименование','parent_id'=>'Родительская группа','alias'=>'Алиас'));
        $grid->setFormat('parent_id', array($this, 'formatParentId'));
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new User_Form_Group_Add());
    }
    
    public function formatParentId($row, $key)
    {
        if (!empty($row->$key)) return $row->GroupParent->title;
        return '';
    }
}