<?php
class User_UserController extends usEngine_Controller_Action
{
    public function signupAction()
    {
        $form = new User_Form_User_Signup();
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();
            if ($form->isValid($formdata)) {
                $data = $form->getValues();
                User_Model_UserTable::getInstance()->registration($data);
                $this->_redirect('/send-verification');
            }
        }
        $this->view->form = $form;
    }
    
    public function signinAction()
    {
        $form = new User_Form_User_Signin();
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();
            if ($form->isValid($formdata)) {
                $data = $form->getValues();
                $result = User_Model_UserTable::getInstance()->authentication($data);
                if ($result->isValid()) {
                    $this->_redirect('/');
                }
                $messages = $result->getMessages();
                $form->getElement('email')->addError($messages[0]);
            }
        }
        $this->view->form = $form;
    }
    
    public function signoutAction()
    {
        if (Zend_Auth::getInstance()->hasIdentity()) {
            Zend_Auth::getInstance()->clearIdentity();
            Zend_Session::forgetMe();
            $this->_redirect('/');
        } else $this->_redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function sendVerificationAction()
    {
        
    }
    
    public function sendForgotAction()
    {
        
    }
    
    public function forgotVerificationAction()
    {        
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $token_code = $this->getRequest()->getParam('token', null);
        if (!$token_code) throw new Zend_Controller_Action_Exception('Не передан токен', 404);
        
        $token = User_Model_TokenTable::getInstance()->findOneByToken($token_code);
        if (!$token) throw new Zend_Controller_Action_Exception('Токен не идентифицирован', 404);
        
        User_Model_UserTable::getInstance()->verificationForgot($token);
        
        $this->_redirect('/signin');
    }
    
    public function verificationAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $token_code = $this->getRequest()->getParam('token', null);
        if (!$token_code) throw new Zend_Controller_Action_Exception('Не передан токен', 404);
        
        $token = User_Model_TokenTable::getInstance()->findOneByToken($token_code);
        if (!$token) throw new Zend_Controller_Action_Exception('Токен не идентифицирован', 404);
        
        User_Model_UserTable::getInstance()->verification($token);
        
        $this->_redirect('/signin');
    }
    
    public function forgotAction()
    {
        $form = new User_Form_User_Forgot();
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getPost();
            if ($form->isValid($formdata)) {
                $data = $form->getValues();
                
                $user = User_Model_UserTable::getInstance()->findOneByEmail($data['email']);
                
                if ($user) {
                    User_Model_UserTable::getInstance()->forgot($user);
                    $this->_redirect('/send-forgot');
                }
                
                $form->getElement('email')->addError('Пользователь не найден');
            }
            $form->populate($formdata);
        }
        $this->view->form = $form;
    }
    
    public function deniedAction()
    {
        $this->view->headMeta()->setName('robots', 'noindex, nofollow');
    }
}
