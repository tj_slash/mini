<?php
class User_AdminUserController extends usEngine_Controller_Action
{    
    public function init()
    {
        $this->_model = User_Model_UserTable::getInstance();
        $this->_helper->layout->setLayout('admin_content');
    }
    
    public function listAction()
    {
        $grid = new usTable_Table($this->_model->adminListing());
        $grid->setCaption('Пользователи')->setToolbars(false)->setActions(false);
        $grid->setToolbar(usTable_Plugin_Toolbar::DELETE, array());
        $grid->setToolbar(usTable_Plugin_Toolbar::ADD, array());
        $grid->setAction(usTable_Plugin_Action::EDIT, array());
        $grid->setAction(usTable_Plugin_Action::DELETE, array());
        $grid->setColumns(array('id'=>'ID','nick'=>'Никнейм','email'=>'E-mail','group_id'=>'Группа доступа'));
        $grid->setFormat('group_id', array($this, 'formatGroupId'));
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new User_Form_User_Add());
    }
    
    public function formatGroupId($row)
    {
        return $row->UserGroup->title;
    }
}