<?php
class User_Form_Signin extends usEngine_Form_Form
{
    protected static $_elDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false)),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'tip')),
    );
    
    protected static $_btnDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false)),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'li')),
    );
    
    protected static $_dgDecorator = array(
        'FormElements',
        'Fieldset' => array('HtmlTag', array('tag' => 'ul', 'class' => 'uibutton-group', 'style' => 'float:right;margin-right:28px'))
    );
    
    protected function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setRequired(true);
        $item->setAttrib('id', 'username_id');
        $item->setAttrib('placeholder', 'Ваш e-mail');
        $item->setAttrib('original-title', 'Введите ваш e-mail');
        $item->addValidator(new Zend_Validate_EmailAddress());
        $item->addFilter(new Zend_Filter_StripTags());
        $item->addValidator(new Zend_Validate_StringLength(2, 128, 'UTF-8'));
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elDecorator);
        return $item;
    }

    protected function createPassword()
    {
        $item = new Zend_Form_Element_Password('password');
        $item->setRequired(true);
        $item->setAttrib('id', 'password');
        $item->setAttrib('placeholder', 'Ваш пароль');
        $item->setAttrib('original-title', 'Введите ваш пароль');
        $item->addFilter(new Zend_Filter_StripTags());
        $item->addValidator(new Zend_Validate_StringLength(6, 128, 'UTF-8'));
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elDecorator);
        return $item;
    }

    protected function createLogin()
    {
        $item = new Zend_Form_Element_Submit('login');
        $item->setLabel('Войти');
        $item->setAttrib('id', 'but_login');
        $item->setAttrib('class', 'uibutton normal');
        $item->removeDecorator('Label');
        $item->setDecorators(self::$_btnDecorator);
        return $item;
    }

    protected function createForgot()
    {
        $item = new Zend_Form_Element_Submit('forgot');
        $item->setLabel('Забыли пароль?');
        $item->setAttrib('id', 'forgetpass');
        $item->setAttrib('class', 'uibutton normal');
        $item->removeDecorator('Label');
        $item->setDecorators(self::$_btnDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement($this->createEmail());
        $this->addElement($this->createPassword());
        $this->addElement($this->createLogin());
//        $this->addElement($this->createForgot());
//        $this->addDisplayGroup(array('login', 'forgot'), 'uibutton-group');   
        $this->addDisplayGroup(array('login'), 'uibutton-group');   
        $displayGroup = $this->getDisplayGroup('uibutton-group');
        $displayGroup->setDecorators(self::$_dgDecorator);
        $displayGroup->removeDecorator('DtDdWrapper');
        $displayGroup->setAttrib('class', 'uibutton-group');
        $displayGroup->setAttrib('style', 'border:none');
        $this->setAttrib('id', 'formLogin');
    }
}