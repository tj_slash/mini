<?php
class User_Form_User_Forgot extends usEngine_Form_Form
{
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('HtmlTag', array('tag' => 'div', 'class' => 'col-xs-12 col-sm-12 col-md-9')),
        array('Label', array('separator' => ' ', 'escape' => false, 'class' => 'col-xs-12 col-sm-3', 'placement' => 'append')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    
    protected static $_textDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('HtmlTag', array('tag' => 'div', 'class' => 'col-xs-12 col-sm-12')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    
    protected function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setRequired(true);
        $item->setLabel('E-mail');
        $item->setAttrib('class', 'form-control');
        $item->addValidator(new Zend_Validate_EmailAddress());
        $item->addValidator(new Zend_Validate_StringLength(2, 128, 'UTF-8'));
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->addFilter(new Zend_Filter_StripTags());
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    protected function createLogin()
    {
        $item = new Zend_Form_Element_Submit('forgot');
        $item->setLabel('Восстановить пароль');
        $item->setAttrib('class', 'btn btn-primary');
        $item->removeDecorator('Label');
        $item->setDecorators(self::$_textDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement($this->createEmail());
        $this->addElement($this->createLogin());
        
        $this->setMethod('POST');
        $this->setAttrib('class', 'form-horizontal contact');
    }
}