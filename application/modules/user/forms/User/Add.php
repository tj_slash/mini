<?php
class User_Form_User_Add extends usEngine_Form_Form
{
    protected static function createGroupSelect()
    {
        $groups = User_Model_GroupTable::getInstance()->findAll();
        $item = new Zend_Form_Element_Select('group_id');
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setLabel('Группа');
        $item->setDescription('Группа доступа пользователя');
        $item->addFilter(new Zend_Filter_StripTags);
        if($groups){
            foreach($groups as $group) {
                $item->addMultiOption($group->id, $group->title);
            }
        }
        return $item;
    }
    
    private static function createNickname()
    {
        $item = new Zend_Form_Element_Text('nick');
        $item->setLabel('Никнейм');
        $item->setDescription('Никнейм пользователя');
        $item->addValidator(new Zend_Validate_StringLength(2, 128, 'UTF-8'));
        $item->addValidator(new usEngine_Validate_Unique('User_Model_User', 'nick'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    protected function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setLabel('E-mail');
        $item->setDescription('E-mail пользователя');
        $item->addValidator(new usEngine_Validate_Unique('User_Model_User', 'email'));
        $item->addValidator(new Zend_Validate_EmailAddress());
        $item->addFilter(new Zend_Filter_StripTags());
        $item->addValidator(new Zend_Validate_StringLength(2, 128, 'UTF-8'));
        $item->addValidator(new Zend_Validate_NotEmpty());
        return $item;
    }
    
    protected function createPassword()
    {
        $item = new Zend_Form_Element_Password('password');
        $item->setLabel('Пароль');
        $item->setDescription('Пароль пользователя (от 5 символов)');
        $item->setRequired(true);
        $item->addFilter(new Zend_Filter_StripTags());
        $item->addValidator(new Zend_Validate_StringLength(5, 128, 'UTF-8'));
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    protected function createCheckPassword()
    {
        $item = new Zend_Form_Element_Password('check_password');
        $item->setLabel('Повторите пароль');
        $item->setDescription('Повторите введенный пароль пользователя');
        $item->setRequired(true);
        $item->addFilter(new Zend_Filter_StripTags());
        $item->addValidator(new Zend_Validate_StringLength(5, 128, 'UTF-8'));
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->addValidator(new Zend_Validate_Identical('password'));
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createGroupSelect());
        $this->addElement(self::createNickname());
        $this->addElement(self::createEmail());
        $this->addElement(self::createPassword());
        $this->addElement(self::createCheckPassword());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->addElement(self::createSubmitAddInput());
        $this->addElement(self::createCopyAddInput());
        $this->setDecorators(self::$_formDecorator);
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['id'])) {
            $this->getElement('password')->setRequired(false);
            $this->getElement('check_password')->setRequired(false);
            $this->getElement('nick')->addValidator(new usEngine_Validate_Unique('User_Model_User', 'nick', $formdata['id']));
            $this->getElement('email')->addValidator(new usEngine_Validate_Unique('User_Model_User', 'email', $formdata['id']));
        }   
        return $formdata;
    }
    
    public function unsetSave($formdata)
    {
        if (!empty($formdata['id'])) {
            if (empty($formdata['password']) || $formdata['password'] === '') {
                unset($formdata['password']);
            }
        }   
        return $formdata;
    }
    
    public function preSave($formdata)
    {
        if (!empty($formdata['password']) && $formdata['password'] !== '') {
            $formdata['password'] = User_Model_UserTable::cryptPassword($formdata['password']);
        }
        return $formdata;
    }
}