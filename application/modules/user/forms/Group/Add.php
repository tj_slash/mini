<?php
class User_Form_Group_Add extends usEngine_Form_Form
{
    protected static function createGroupSelect()
    {
        $groups = User_Model_GroupTable::getInstance()->findAll();
        $item = new Zend_Form_Element_Select('parent_id');
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setLabel('Группа');
        $item->setDescription('Родительская группа');
        $item->addFilter(new Zend_Filter_StripTags);
        if($groups){
            foreach($groups as $group) {
                $item->addMultiOption($group->id, $group->title);
            }
        }
        return $item;
    }
    
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Название');
        $item->setDescription('Название группы доступа');
        $item->addValidator(new Zend_Validate_StringLength(2, 128, 'UTF-8'));
        $item->addValidator(new usEngine_Validate_Unique('User_Model_User', 'nick'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createAlias()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Алиас');
        $item->setDescription('Алиас группы доступа');
        $regex = new Zend_Validate_Regex('/^[\-a-zA-Z0-9]+$/u', true);
        $regex->setMessage('Алиас может содержать только латинские буквы и цифры');
        $item->addValidator($regex);
        $item->addValidator(new Zend_Validate_StringLength(2, 128, 'UTF-8'));
        $item->addValidator(new usEngine_Validate_Unique('User_Model_Group', 'alias'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createGroupSelect());
        $this->addElement(self::createTitle());
        $this->addElement(self::createAlias());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->addElement(self::createSubmitAddInput());
        $this->addElement(self::createCopyAddInput());
        $this->setDecorators(self::$_formDecorator);
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['id'])) {
            $this->getElement('alias')->addValidator(new usEngine_Validate_Unique('User_Model_Group', 'alias', $formdata['id']));
        }   
        return $formdata;
    }
}