<?php
/**
 * Banner controller.
 *
 * @category    Banner
 * @package     BannerController
 * @author      Vakylenkox A. <vakylenkox@gmail.com>
 */
class Banner_BannerController extends usEngine_Controller_Action
{    
    public function bannerAction ()
    {
        $alias = $this->getRequest()->getParam('position', null);
        if (!empty($alias)) {
            $banner = Doctrine_Query::create()
                ->select('banner.*')
                ->from('Banner_Model_Banner AS banner')
                ->leftJoin('banner.Position AS position')
                ->addWhere('banner.publish = ?', 1)
                ->addWhere('banner.archive = ?', 0)
                ->addWhere('position.publish = ?', 1)
                ->addWhere('position.archive = ?', 0)
                ->addWhere('position.alias = ?', $alias)
                ->orderBy('RAND()')
                ->limit(1)
                ->fetchOne();
            if (!empty($banner)) {
                $this->view->banner = $banner;
            }
        }
    }
}