<?php
/**
 * Admin position controller of Banner module.
 *
 * @category    Banner
 * @package     AdminPositionController
 * @author      Vakylenkox A. <vakylenkox@gmail.com>
 */
class Banner_AdminPositionController extends usEngine_Controller_Action
{
    /**
     * Gallery DoctrineTable class instance
     * @var DoctrineTable
     */
    protected $_model = null;
    
    /**
     * Image DoctrineTable class instance
     * @var DoctrineTable
     */
    protected $_modelImage = null;
    
    /**
     * Count items per page
     * @var int
     */
    protected $_itemsPerPage = 10;
    
    /**
     * Resource string for acl plugin. Ex.: 'gallery:admin'.
     * @var string
     */
    protected $_resoursce = null;
    
    /**
     * Init class properties and others
     */
    
    /**
     * @vars string - names of routes
     */
    protected $_routeList = null;
    protected $_routeAdd = null;
    protected $_routeEdit = null;
    protected $_routePublish = null;
    protected $_routeArchive = null;
    protected $_routeDelete = null;
    protected $_routeListArchive = null;
    
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        
        $options = Zend_Registry::get('options');
        $this->_itemsPerPage = $options['items_per_page'];
        
        $this->_model = Banner_Model_PositionTable::getInstance();
        
        $this->_resoursce = lcfirst($this->getRequest()->getModuleName()) . ':admin-position';
        $this->_redirector = $this->_helper->getHelper('Redirector');
        
        $this->_routeList = 'adminBannerPositionList';
        $this->_routeAdd = 'adminBannerPositionAdd';
        $this->_routeEdit = 'adminBannerPositionEdit';
        $this->_routePublish = 'adminBannerPositionPublish';
        $this->_routeArchive = 'adminBannerPositionArchive';
        $this->_routeDelete = 'adminBannerPositionDelete';
        $this->_routeListArchive = 'adminBannerPositionListArchive';
    }
    
    /**
     * List
     */
    public function listAction()
    {
        $query = $this->_model->adminListing()->where('archive = 0');
        $grid = new usGrid_Grid($query);
        $grid->setTable($this->view->adminMenu->findOneBy('active', true)->getLabel())
            ->setTemplate('adminTable.phtml')
            ->setColumns(array(
                'id' => 'ID', 
                'title' => 'Заголовок', 
                'alias' => 'Алиас', 
                'created' => 'Дата создания', 
                'publish' => 'Публикация'
            ))
            ->setPlugin(usGrid_Grid_Plugin_Filter::factory(
                array('id'=>'ID', 'title'=>'Заголовок', 'alias' => 'Алиас', 'publish'=>'Публикация (1 - да, 0 - нет)')
            ))
            ->setPlugin(usGrid_Grid_Plugin_Sortable::factory(array('id','title', 'alias', 'created', 'publish'), null, 'id', 'ASC'))
            ->setPlugin(usGrid_Grid_Plugin_Actions::factory(
                array(
                    'Редактировать' =>      array('link' => $this->view->url(array(), $this->_routeEdit) . '/?ids=%s', 'data' => array('id'), 'icon' => 'pencil.png','resource' => $this->_resoursce, 'privilege' => 'edit', 'noBaseUrl' => true),
                    'Публикация' =>         array('link' => $this->view->url(array(), $this->_routePublish) . '/?ids=%s', 'data' => array('id'), 'icon' => 'accept.png', 'resource' => $this->_resoursce, 'privilege' => 'publish', 'noBaseUrl' => true),
                    'Удалить' =>            array('link' => $this->view->url(array(), $this->_routeArchive) . '/?ids=%s', 'data' => array('id'), 'icon' => 'delete.png', 'resource' => $this->_resoursce, 'privilege' => 'delete', 'noBaseUrl' => true),
                )
            ))
            ->setPlugin(usGrid_Grid_Plugin_Toolbar::factory(
                array(
                    'Архив' =>      array('link' => $this->view->url(array(), $this->_routeListArchive), 'class' => 'confirm', 'isNull' => true, 'resource' => $this->_resoursce, 'privilege' => 'list-archive', 'noBaseUrl' => true),
                    'Добавить' =>   array('link' => $this->view->url(array(), $this->_routeAdd),'class' => 'icon add', 'isNull' => true, 'resource' => $this->_resoursce, 'privilege' => 'add', 'noBaseUrl' => true),
                    'Публикация' => array('link' => $this->view->url(array(), $this->_routePublish),'class' => 'icon secure', 'resource' => $this->_resoursce, 'privilege' => 'publish', 'noBaseUrl' => true),
                    'Удалить' =>    array('link' => $this->view->url(array(), $this->_routeArchive),'class' => 'special', 'resource' => $this->_resoursce, 'privilege' => 'archive', 'noBaseUrl' => true),
                )
            ))
            ->setPlugin(usGrid_Grid_Plugin_Checkbox::factory())
            ->setPlugin(usGrid_Grid_Plugin_Align::factory(array('id'=>'center', 'publish'=>'center')))
            ->setPlugin(usGrid_Grid_Plugin_Navigator::factory($this->_itemsPerPage))
            ->setPlugin(usGrid_Grid_Plugin_Format::factory(array(
                'publish' => 'formatPublish', 
                'created' => 'formatDate',
            ), $this))
            ->setView($this->view);

        $this->view->grid = $grid;
        $this->view->headTitle($this->view->adminMenu->findOneBy('active', true)->getLabel());
        $this->_helper->viewRenderer->setRender('list');
    }
    
    /**
     * Add category 
     */
    public function addAction()
    {
        $form = new Banner_Form_Position_Add();
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                $category = $this->_model->add($form->getValues());
                usEngine_Message::getInstance()->setMessage('success_add');
                if (isset($formdata['submit']))
                    return $this->_redirector->setGotoRoute(array(), $this->_routeList);
                else if (isset($formdata['submit_continue']))
                    return $this->_redirect($this->view->url(array(), $this->_routeEdit) . '/?ids=' . $category->id, array('prependBase' => false));
                else if (isset($formdata['submit_add']))
                    return $this->_redirector->setGotoRoute(array(), $this->_routeAdd);
            } else {
                usEngine_Message::getInstance()->setMessage('error', 'При добавлении категории произошла ошибка!');
            }
            $form->populate($formdata);
        }
        $this->view->form = $form;
        $this->view->headTitle($this->view->adminMenu->findOneBy('active', true)->getLabel());
        $this->_helper->viewRenderer->setRender('add');
    }
    
    /**
     * Edit category
     */
    public function editAction()
    {
        $id = $this->getRequest()->getParam('ids');
        if (!$id)
            throw new Exception("Не указан идентификатор категории");
        if(!($gallery = $this->_model->findOneById($id))) {
            usEngine_Message::getInstance()->setMessage('error', 'Категория не найдена');
            return $this->_redirector->setGotoRoute(array(), $this->_routeList);
        }
        $mock = $this->getRequest()->getParam('submit_options');
        
        $formdata = $gallery->toArray();
        $form = new Banner_Form_Position_Add();
        if ($this->getRequest()->isPost() && !isset($mock)) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                $data = $form->getValues();
                $data['id'] = $id;
                $this->_model->edit($data);
                
                usEngine_Message::getInstance()->setMessage('success_edit');
                if (isset($formdata['submit']))
                    return $this->_redirector->setGotoRoute(array(), $this->_routeList);
                else if (isset($formdata['submit_continue']))
                    return $this->_redirect($this->view->url(array(), $this->_routeEdit) . '/?ids=' . $id, array('prependBase' => false));
                else if (isset($formdata['submit_add']))
                    return $this->_redirector->setGotoRoute(array(), $this->_routeAdd);
            } else {
                usEngine_Message::getInstance()->setMessage('error', 'При добавлении категории произошла ошибка!');
            }
        }
        $form->populate($formdata);
        $this->view->id = $id;
        $this->view->form = $form;
        $this->view->headTitle($this->view->adminMenu->findOneBy('active', true)->getLabel());
        $this->_helper->viewRenderer->setRender('add');
    }

    /**
     * Listing archived categories 
     */
    public function listArchiveAction()
    {
        $grid = new usGrid_Grid($this->_model->adminListing()->addWhere('archive = 1'));
        $grid->setTable($this->view->adminMenu->findOneBy('active', true)->getLabel())
            ->setTemplate('adminTable.phtml')
            ->setColumns(array(
                'id' => 'ID', 
                'title' => 'Заголовок', 
                'alias' => 'Алиас', 
                'created' => 'Дата создания', 
                'publish' => 'Публикация'
            ))
            ->setPlugin(usGrid_Grid_Plugin_Filter::factory(array('id' => 'ID', 'title' => 'Заголовок', 'alias' => 'Алиас', 'created' => 'Дата создания', 'publish' => 'Публикация (1 - да, 0 - нет)')))
            ->setPlugin(usGrid_Grid_Plugin_Sortable::factory(array('id', 'title', 'alias', 'created', 'publish')))
            ->setPlugin(usGrid_Grid_Plugin_Actions::factory(
                array(
                    'Восстановить' => array('link' => $this->view->url(array(), $this->_routeArchive) . '/?ids=%s', 'data' => array('id'), 'icon' => 'snapback.png', 'resource' => $this->_resoursce, 'privilege' => 'archive', 'noBaseUrl' => true),
                    'Удалить навсегда' => array('link' => $this->view->url(array(), $this->_routeDelete) . '/?ids=%s', 'data' => array('id'), 'icon' => 'delete.png', 'resource' => $this->_resoursce, 'privilege' => 'delete', 'noBaseUrl' => true),
                )
            ))
            ->setPlugin(usGrid_Grid_Plugin_Toolbar::factory(
                array(
                    'Восстановить'=>array('link' => $this->view->url(array(), $this->_routeArchive), 'class' => 'icon answer', 'resource' => $this->_resoursce, 'privilege' => 'archive', 'noBaseUrl' => true),
                    'Удалить навсегда'=>array('link' => $this->view->url(array(), $this->_routeDelete), 'class' => 'special', 'resource' => $this->_resoursce, 'privilege' => 'delete', 'noBaseUrl' => true),
                )
            ))
            ->setPlugin(usGrid_Grid_Plugin_Checkbox::factory())
            ->setPlugin(usGrid_Grid_Plugin_Align::factory(array('id' => 'center')))
            ->setPlugin(usGrid_Grid_Plugin_Navigator::factory())
            ->setPlugin(usGrid_Grid_Plugin_Format::factory(array(
                    'publish' => 'formatPublish',
                    'created' => 'formatDate',
                ), $this
            ))
            ->setView($this->view);
        $this->view->grid = $grid;
        $this->view->headTitle($this->view->adminMenu->findOneBy('active', true)->getLabel());
        $this->_helper->viewRenderer->setRender('list');
    }
    
    public function deleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        $this->_model->delete($ids);
        usEngine_Message::getInstance()->setMessage('success_delete');
        
        $query = $this->_model->adminListing()->addWhere('archive = 1');
        
        if (!$query->execute()->count())
            return $this->_redirector->setGotoRoute(array(), $this->_routeList);
        else if (isset($_SERVER['HTTP_REFERER']))
            $this->_redirect($_SERVER['HTTP_REFERER']);
        else 
            $this->_redirect($this->view->baseUrl() . '/admin');
    }
    
    public function archiveAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        $this->_model->archive($ids);
        usEngine_Message::getInstance()->setMessage('success_delete');
        
        $query = $this->_model->adminListing()->addWhere('archive = 1');
        
        if (!$query->execute()->count())
            return $this->_redirector->setGotoRoute(array(), $this->_routeList);
        else if (isset($_SERVER['HTTP_REFERER']))
            $this->_redirect($_SERVER['HTTP_REFERER']);
        else 
            $this->_redirect($this->view->baseUrl() . '/admin');
    }
}
