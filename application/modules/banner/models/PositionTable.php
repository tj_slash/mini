<?php
class Banner_Model_PositionTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Banner_Model_Position');
    }
}