<?php
class Banner_Model_BannerTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Banner_Model_Banner');
    }
}