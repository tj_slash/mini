<?php
class Banner_Form_Banner_Add extends usEngine_Form_Form
{
    private static function createPosition()
    {
        $item = new Zend_Form_Element_Select('position_id');
        $item->setLabel('Позиция');
        $item->setDescription('Позиция для отображения баннеров');
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        $item->addMultiOption(NULL, 'Выберите позицию');
                
        $positions = Doctrine_Query::create()
                ->from('Banner_Model_Position')
                ->where('publish = ?', 1)
                ->andWhere('archive = ?', 0)
                ->execute();
        if (!empty($positions)) {
            foreach($positions as $position) {
                $item->addMultiOption($position->id, $position->title);
            }
        }
        return $item;
    }
    
    private static function createTitleInput()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок баннера');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createUrl()
    {
        $item = new Zend_Form_Element_Text('url');
        $item->setLabel('URL');
        $item->setDescription('URL баннера');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createBanner()
    {
        $item = new Zend_Form_Element_File('banner');
        $item->setLabel('Баннер');
        $item->setDescription('Баннер (JPG, JPEG, SWF)');
        $item->setRequired(true);
        $item->setDecorators(self::$_fileElementDecorator);
        return $item;
    }

    public function init()
    {
        $this->setMethod('post')->setAttrib('enctype', 'multipart/form-data');
        $this->addElement(self::createPosition());
        $this->addElement(self::createTitleInput());
        $this->addElement(self::createUrl());
        $this->addElement(self::createBanner());
        $this->addElement(self::createPublishChekbox());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->addElement(self::createSubmitAddInput());
        $this->setDecorators(self::$_formDecorator);
    }
}
