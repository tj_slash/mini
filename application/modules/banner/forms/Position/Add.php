<?php
class Banner_Form_Position_Add extends usEngine_Form_Form
{
    private static function createTitleInput()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок позиции баннеров');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAliasInput()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Alias');
        $item->setDescription('Алиас позиции баннеров');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $regex = new Zend_Validate_Regex('/^[\-a-zA-Z0-9_]+$/u');
        $item->addValidator($regex);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(false);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createWidth()
    {
        $item = new Zend_Form_Element_Text('width');
        $item->setLabel('Ширина');
        $item->setDescription('Ширина баннеров в пикселях');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addValidator(new Zend_Validate_Digits());
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createHeight()
    {
        $item = new Zend_Form_Element_Text('height');
        $item->setLabel('Высота');
        $item->setDescription('Высота баннеров в пикселях');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addValidator(new Zend_Validate_Digits());
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createTitleInput());
        $this->addElement(self::createAliasInput());
        $this->addElement(self::createWidth());
        $this->addElement(self::createHeight());
        $this->addElement(self::createPublishChekbox());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->addElement(self::createSubmitAddInput());
        $this->setDecorators(self::$_formDecorator);
    }
}
