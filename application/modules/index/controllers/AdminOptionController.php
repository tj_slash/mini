<?php

/**
 * AdminOption controller of Index module.
 *
 * @category    Index
 * @package     Index_Controller
 * @author Anisimov A. <anisalex@list.ru>
 */
class Index_AdminOptionController extends usEngine_Controller_Action
{
    protected $_model = null;

    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Index_Model_OptionsTable::getInstance();
        $this->_redirector = $this->_helper->getHelper('Redirector');
    }

    public function addAction()
    {
        $form = new Index_Form_AdminOption_Add();
        $form->getElement('name')->addValidator(new usEngine_Validate_Unique('Index_Model_Options', 'name'));
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                $data = $form->getValues();
                $option = $this->_model->add($data);
                Inc_Message::getInstance()->setMessage('success_add');
                if (isset($formdata['submit']))
                    return $this->_redirector->setGotoRoute(array(), 'adminOptionList');
                else if (isset($formdata['submit_continue']))
                    return $this->_redirector->setGotoRoute(array('ids' => $option->id), 'adminOptionEdit');
                else if (isset($formdata['submit_add']))
                    return $this->_redirector->setGotoRoute(array(), 'adminOptionAdd');
            } else {
                Inc_Message::getInstance()->setMessage('error', 'При добавлении страницы произошла ошибка!');
            }
            $form->populate($formdata);
        }
        $this->view->form = $form;
        $this->view->headTitle($this->view->adminMenu->findOneBy('active', true)->getLabel());
        $this->_helper->viewRenderer->setRender('edit');
    }
    
    public function editAction()
    {
        $this->view->headTitle('Редактировать опцию');
        $id = $this->getRequest()->getParam('ids');
        if (empty($id)) {
            Inc_Message::getInstance()->setMessage('error', 'Опция не найдена');
            $this->_redirect('/admin/options');
            return;
        }            
        $mock = $this->getRequest()->getParam('submit_options');
        $form = new Index_Form_AdminOption_Add();
        $form->removeElement('name');
        $option = $this->_model->findOneById($id);
        if (empty($option)) {
            Inc_Message::getInstance()->setMessage('error', 'Опция не найдена');
            $this->_redirect('/admin/options');
            return;
        }
        $option = $option->toArray();
        if ($this->getRequest()->isPost() && !isset($mock)) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                $data = $form->getValues();
                $data['id'] = $id;
                $edited_option = $this->_model->edit($data);
                Inc_Message::getInstance()->setMessage('success_edit');
                if (isset($formdata['submit']))
                    return $this->_redirector->setGotoRoute(array(), 'adminOptionList');
                else if (isset($formdata['submit_continue']))
                    return $this->_redirector->setGotoRoute(array('ids' => $edited_option->id), 'adminOptionEdit');
                else if (isset($formdata['submit_add']))
                    return $this->_redirector->setGotoRoute(array(), 'adminOptionAdd');
            } else {
                Inc_Message::getInstance()->setMessage('error', 'При редактировании опции произошла ошибка!');
            }
        }
//        $this->view->form_options = usEngine_Acl_Options::getInstance()->logicForm(APPLICATION_PATH . '/modules/admin/configs.ini', isset($id) ? 'admin:' . $id : null);
        $form->populate($option);
        $this->view->form = $form;
        $this->view->headTitle($this->view->adminMenu->findOneBy('active', true)->getLabel());
    }
    
    public function listAction()
    {
        $grid = new usGrid_Grid($this->_model->adminListing());
        $grid->setTable('Опции')
            ->setTemplate('adminTable.phtml')
            ->setColumns(array('id'=>'ID','title'=>'Заголовок','name'=>'Название','value'=>'Значение'))
            ->setPlugin(usGrid_Grid_Plugin_Filter::factory(array('id'=>'ID', 'title' => 'Заголовок', 'name'=>'Название', 'value' => 'Значение')))
            ->setPlugin(usGrid_Grid_Plugin_Sortable::factory(array('id','title','name','value')))
            ->setPlugin(usGrid_Grid_Plugin_Actions::factory(
                array(
                    'Редактировать'=>array('link'=>'/admin/options/edit/%s','data'=>array('id'),'icon'=>'pencil.png','resource'=>'index:admin-option','privilege'=>'edit'),
                )
            ))
            ->setPlugin(usGrid_Grid_Plugin_Toolbar::factory(
                array(
                    'Добавить'=>array('link'=>'/admin/options/add','class'=>'icon add','isNull'=>true,'resource'=>'index:admin-option','privilege'=>'add'),
                )
            ))
            ->setPlugin(usGrid_Grid_Plugin_Checkbox::factory())
            ->setPlugin(usGrid_Grid_Plugin_Align::factory(array('id'=>'center','value'=>'left')))
            ->setPlugin(usGrid_Grid_Plugin_Navigator::factory())
            ->setView($this->view);
        $this->view->grid = $grid;
        $this->view->headTitle($this->view->adminMenu->findOneBy('active', true)->getLabel());
    }
}