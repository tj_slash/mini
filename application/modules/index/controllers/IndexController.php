<?php
class Index_IndexController extends usEngine_Controller_Action
{
    public function init()
    {

    }

    public function indexAction()
    {
        $this->_helper->layout->setLayout('layout');
    }

    public function menuAction()
    {
        
    }
    
    public function messagesAction()
    {
        
    }
    
    public function headerAction()
    {
        $options = Zend_Registry::get('options');
        $this->view->logotype = $this->view->baseUrl($options['setting']['index']['logotype']);
        $this->view->isMain = $this->getRequest()->getParam('isMain', false);
    }

    public function footerAction()
    {
        
    }
    
    public function redirectAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $url = $this->getRequest()->getParam('url', false);
        header("X-Robots-Tag: noindex, nofollow, noarchive", true);
        $this->_redirect(base64_decode($url), array('exit' => true, 'code' => 302));
    }
}