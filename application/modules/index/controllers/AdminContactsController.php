<?php
/**
 * Created by JetBrains PhpStorm.
 * User: v25
 * Date: 21.10.13
 * Time: 11:02
 * To change this template use File | Settings | File Templates.
 */
class Index_AdminContactsController extends usEngine_Controller_Action
{
    public function contactsAction()
    {
        $form = new Index_Form_Settings();
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {

                $options = $form->getValues();
                $labels = $form->getLabels();
                foreach ($options[usEngine_Form_Setting::ELS_NAME] as $alias => $value) {
                    $optionAlias = implode('.', array('setting', $form::MODULE_NAME, str_replace('_','.',$alias)));
                    $option = Index_Model_OptionsTable::getInstance()->findOneByAlias($optionAlias);
                    if (!$option) {
                        $option = new Index_Model_Options();
                        if (!empty($labels[$alias])) {
                            $option->title = $labels[$alias];
                        } else {
                            $option->title = $alias;
                        }
                        $option->alias = $optionAlias;
                    }
                    $option->value = $value;
                    $option->save();
                }
                usEngine_Message::getInstance()->setMessage('success_add');
                $this->_redirect($_SERVER['HTTP_REFERER']);
            }
            usEngine_Message::getInstance()->setMessage('error', 'При сохранении опций произошла ошибка!');
            $form->populate($formdata);
        }
        $this->view->form = $form;
        $this->_helper->layout->setLayout('admin_content');
        $this->renderScript('admin/settings.phtml');
    }
}