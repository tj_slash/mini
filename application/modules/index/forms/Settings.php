<?php
/**
 * Created by JetBrains PhpStorm.
 * User: v25
 * Date: 21.10.13
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 */
class Index_Form_Settings extends usEngine_Form_Setting
{
    /**
     * Название модуля для опций
     */
    const MODULE_NAME = 'index';

    protected static $fields = array(
        'contacts.address',
        'contacts.phone',
        'contacts.email',
        'contacts.social.vk',
        'contacts.social.ok',
        'contacts.social.tw',
        'contacts.social.fb',
        'contacts.social.yt',
        'contacts.social.in',
    );

    private static function createAddress()
    {
        $item = new Zend_Form_Element_Text('contacts_address');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Адресс');
        $item->setDescription('адресс организации');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }


    private static function createPhone()
    {
        $item = new Zend_Form_Element_Text('contacts_phone');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Телефон');
        $item->setDescription('Телефон организации');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createEmail()
    {
        $item = new Zend_Form_Element_Text('contacts_email');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Электронная почта');
        $item->setDescription('Электронная почта для связи');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createVk()
    {
        $item = new Zend_Form_Element_Text('contacts_social_vk');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Ссылкла для Вконтакте');
        $item->setDescription('сслыка на группу/профиль vkontakte');
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createOk()
    {
        $item = new Zend_Form_Element_Text('contacts_social_ok');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Ссылкла для Одноклассников');
        $item->setDescription('сслыка на группу/профиль одноклассников');
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }


    private static function createTwitter()
    {
        $item = new Zend_Form_Element_Text('contacts_social_tw');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Ссылкла для Твиттера');
        $item->setDescription('сслыка на профиль твиттера');
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createFaceBook()
    {
        $item = new Zend_Form_Element_Text('contacts_social_fb');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Ссылкла для Фэйсбука');
        $item->setDescription('сслыка на группу/профиль Фэйсбука');
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }


    private static function createYouTube()
    {
        $item = new Zend_Form_Element_Text('contacts_social_yt');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Ссылкла для Ютюба');
        $item->setDescription('сслыка на канал/профиль Ютюба');
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createInstagram()
    {
        $item = new Zend_Form_Element_Text('contacts_social_instagram');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Ссылкла для Instagram');
        $item->setDescription('сслыка на профиль Instagram');
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createAddress());
        $this->addElement(self::createPhone());
        $this->addElement(self::createEmail());
        $this->addElement(self::createVk());
        $this->addElement(self::createOk());
        $this->addElement(self::createFaceBook());
        $this->addElement(self::createTwitter());
        $this->addElement(self::createYouTube());
        $this->addElement(self::createInstagram());
        $this->addElement(self::createSubmitInput());
        $this->setDecorators(self::$_formDecorator);
        $class = get_class($this);
        $values = Index_Model_OptionsTable::getInstance()->getOptionsInAlias($class::$fields, $class::MODULE_NAME);
        $search = implode('_', array(Index_Model_OptionsTable::GLOBAL_ALIAS, $class::MODULE_NAME));
//        var_dump($search);die("dump \$search");
        $options = array();
        foreach ($values as $key => $option) {
            $options[str_replace("{$search}_", '', str_replace('.','_',$option->alias))] = $option->value;
        }
//        var_dump($options);die("dump \$options");
        $this->populate($options);
    }
}
