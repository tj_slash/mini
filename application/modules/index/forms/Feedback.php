<?php
/**
 * Форма для обратной связи
 *
 * @category Obv
 * @package Obv
 * @subpackage Obv
 * 
 * @author Vakylenko A. <vakylenkox@gmail.com>
 **/
class Index_Form_Feedback extends usEngine_Form_Form
{
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Label', array('separator' => ' ')),
        array('Errors', array('escape' => false)),
    );

    protected static $_textareaDecorator = array(
        'ViewHelper',
        array('Label', array('separator' => '')),
        array('Errors', array('escape' => false)),
    );
    
    protected static $_captchaDecorator = array(
        'Captcha',
        array('Description', array('escape' => false, 'tag' => false)),
        array('Label', array('separator' => ' ')),
        array('Errors', array('escape' => false)),
    );

    protected static $_submitDecorator = array(
        'ViewHelper',
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-row clearfix submit-wrapper'))
    );

    private function createName()
    {
        $item = new Zend_Form_Element_Text('name');
        $item->setLabel('Имя');
        $item->setRequired(true);
        $item->setAttrib('class', 'span3');
        $item->addValidator(new Zend_Validate_StringLength(2, 255));
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setLabel('Эл. адрес');
        $item->setRequired(true);
        $item->setAttrib('class', 'span3');
        $item->addValidator(new Zend_Validate_StringLength(2, 255));
        $item->addValidator(new Zend_Validate_EmailAddress());
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private function createSubject()
    {
        $item = new Zend_Form_Element_Text('subject');
        $item->setLabel('Тема');
        $item->setRequired(true);
        $item->setAttrib('class', 'span6');
        $item->addValidator(new Zend_Validate_StringLength(2, 255));
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private function createCaptcha()
    {
        $item = new Zend_Form_Element_Captcha('captcha', array(
            'label' => 'Проверочный код',
            'captcha' => 'image',
            'captchaOptions' => array(
                'captcha' => 'image',
                'font' => APPLICATION_PATH . '/../public/fonts/extaz/robotocondensed-regular.ttf',
                'imgDir' => APPLICATION_PATH . '/../public/images/captcha/',
                'imgUrl' => '/images/captcha/',
                'wordLen' => 4,
                'fsize' => 10,
                'height' => 30,
                'width' => 80,
                'gcFreq' => 50,
                'expiration' => 300,
                'dotNoiseLevel' => 0,
                'lineNoiseLevel' => 0
            )
        ));
        $item->setDecorators(self::$_captchaDecorator);
        return $item;
    }
    
    public function generateCaptcha()
    {
        $captcha = $this->getElement('captcha')->getCaptcha();
        return $captcha->generate();
    }

    private function createText()
    {
        $item = new Zend_Form_Element_Textarea('text');
        $item->setLabel('Текст');
        $item->setRequired(true);
        $item->setAttrib('class', 'span6');
        $item->setAttrib('rows', '10');
        $item->setDecorators(self::$_textareaDecorator);
        $item->addValidator(new Zend_Validate_StringLength(10, 1000));
        return $item;
    }
    
    private function createSubmit()
    {
        $item = new Zend_Form_Element_Submit('feedback');
        $item->setLabel('Отправить');
        $item->setAttrib('class', 'btn btn-danger btn-large');
        $item->setDecorators(self::$_submitDecorator);
        return $item;
    }

    public function init()
    {        
        $this->setMethod("POST")
             ->setAttrib('id', 'form-feedback');
        $this->setDecorators(self::$_formDecorator);
        $this->removeDecorator('Label');
        $this->addElement(self::createName());
        $this->addElement(self::createSubject());
        $this->addElement(self::createEmail());
        $this->addElement(self::createText());
        $this->addElement(self::createCaptcha());
        $this->addElement(self::createSubmit());
    }
}
