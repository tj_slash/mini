<?php
class Index_Form_AdminOption_Add extends usEngine_Form_Form
{
    private static function createName()
    {
        $item = new Zend_Form_Element_Text('name');
        $item->setLabel('Алиас');
        $item->setDescription('Алиас опции на английском');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private static function createTitleInput()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Title');
        $item->setDescription('Заголовок опции');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private static function createValueInput()
    {
        $item = new Zend_Form_Element_Text('value');
        $item->setLabel('Value');
        $item->setDescription('Значние опции');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createName());
        $this->addElement(self::createTitleInput());
        $this->addElement(self::createValueInput());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->addElement(self::createSubmitAddInput());
        $this->setDecorators(self::$_formDecorator);
    }
}