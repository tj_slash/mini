<?php

class Index_View_Helper_FormatDateDigits
{
    public function formatDateDigits($date) {
        $zdate = new Zend_Date($date);
        $str = $zdate->get(Zend_Date::DAY) . '/'
            . $zdate->get(Zend_Date::MONTH) . ' '
	    . $zdate->get(Zend_Date::WEEKDAY_NAME);
	return $str;
    }
}
