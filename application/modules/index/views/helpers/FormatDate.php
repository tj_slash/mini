<?php

class Index_View_Helper_FormatDate
{
    public function formatDate($date) {
        $zdate = new Zend_Date($date);
        $str = $zdate->get(Zend_Date::DAY_SHORT) . ' '
            . $zdate->get(Zend_Date::MONTH_NAME) . ', '
	    . $zdate->get(Zend_Date::WEEKDAY);
	return $str;
    }
}
