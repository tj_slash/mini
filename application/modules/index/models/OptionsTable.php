<?php
class Index_Model_OptionsTable extends usEngine_Doctrine_Table
{
    const GLOBAL_ALIAS = 'setting';
    
    /**
     *
     * @return Index_Model_OptionsTable 
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Index_Model_Options');
    }

    public function get($alias)
    {
        $page = Doctrine_Query::create()
            ->from('Index_Model_Options')
            ->andWhere('alias = ?', $alias)
            ->fetchOne();
        return $page;
    }

    public function getOneOption($name)
    {
        $option = Doctrine_Query::create()
            ->from('Index_Model_Options')
            ->where('alias = ?', $name)
            ->fetchOne();
        if(!empty($option)) return $option->value;
        else return false;
    }

    public function getAllOptions()
    {
        $option = Doctrine_Query::create()->from('Index_Model_Options')->fetchOne();
        return $option;
    }
    
    public function getOptionsForBootstrap()
    {
        $options = self::getInstance()->findAll()->toArray();
        return $options;
    }
    
    public function getOptionsInAlias($params, $module)
    {
        if (is_array($params)) {
            if (count($params) > 0) {
                foreach ($params as $key => $param) {
                    $params[$key] = implode('.', array(self::GLOBAL_ALIAS, $module, $param));
                }
                return Doctrine_Query::create()
                ->from('Index_Model_Options')
                ->whereIn('alias', $params)
                ->execute();
            }
        }
        return array();
    }
    
    /**
     *
     * @param usEngine_Form_Setting $form
     */
    public function updateOptions(usEngine_Form_Setting $form)
    {
        $options = $form->getValues();
        $labels = $form->getLabels();    
        foreach ($options[usEngine_Form_Setting::ELS_NAME] as $alias => $value) {
            $optionAlias = implode('.', array(self::GLOBAL_ALIAS, $form::MODULE_NAME, $alias));
            $option = self::getInstance()->findOneByAlias($optionAlias);
            if (!$option) {
                $option = new Index_Model_Options();
                if (!empty($labels[$alias])) {
                    $option->title = $labels[$alias];
                } else {
                    $option->title = $alias;
                }
                $option->alias = $optionAlias;
            }
            if (is_array($value)) {
                $option->value = serialize($value);
            } else {
                $option->value = $value;
            }
            $option->save();
        }
    }
}