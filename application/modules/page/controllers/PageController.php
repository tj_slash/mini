<?php
class Page_PageController extends usEngine_Controller_Action
{    
    public function viewAction()
    {
        $alias = $this->getRequest()->getParam('alias'. null);
        
        if (!$alias) {
            throw new Zend_Controller_Action_Exception('Не передан идентификатор', 404);
        }
        
        $page = Page_Model_PageTable::getInstance()->findOneByAlias($alias);
        
        if (!$page) {
            throw new Zend_Controller_Action_Exception('Страница не найдена', 404);
        }       
        
        if ($page->publish == false || $page->archive == true) {
            throw new Zend_Controller_Action_Exception('Страница удалена', 404);
        }
        
        $page->useMeta();
        
        $this->view->page = $page;
    }
    
    public function contactsAction()
    {
        $form = new Page_Form_Contact_Feedback();
        $formdata = array();
        
        if($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                
                $this->view->data = $form->getValues();
                
                $body = $this->view->render('email/feedback.phtml');
                
                $mail = new Zend_Mail('utf-8');
                $mail->addTo('vakylenkox@gmail.com');
                $mail->setSubject('Форма обратной связи!');
                $mail->setFrom('support@amur.net', 'Amurnet');
                $mail->setBodyHtml($body);
                $mail->send();
                
                usEngine_Message::getInstance()->setMessage('success', 'Ваш вопрос успешно отправлен!');
                $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));
            }
        }
        
        $this->view->form = $form->populate($formdata);
    }
}
