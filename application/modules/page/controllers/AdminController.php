<?php
class Page_AdminController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_model = Page_Model_PageTable::getInstance();
        $this->_helper->layout->setLayout('admin_content');
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Статичные страницы');
            $grid->removeToolbars(array('list', 'delete'));
        } else {
            $grid->setCaption('Архив статичных страниц');
            $grid->removeToolbars(array('list-archive', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','title'=>'Наименование','alias'=>'Алиас','created'=>'Дата создания','publish'=>'Публикация'));
        $grid->setFormat('alias', array($this, 'formatAlias'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public static function formatAlias($row, $key)
    {
        return "<a href='/page/{$row->$key}' target='_blank'>{$row->$key}</a>";
    }
    
    public function addAction()
    {
        $this->formAction(new Page_Form_Page_Add());
    }
}
