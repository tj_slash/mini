<?php
class Page_Model_Page extends Page_Model_Base_Page
{
    public function useMeta()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        
        if (empty($this->m_t)) {
            $view->headTitle()->prepend($this->title);
        } else {
            $view->headTitle()->prepend($this->m_t);
        }
        
        if (empty($this->m_k)) {
            $view->headMeta()->setName('keywords', $this->title);
        } else {
            $view->headMeta()->setName('keywords', $this->m_k);
        }
        
        if (empty($this->m_d)) {
            $view->headMeta()->setName('description', $this->title);
        } else {
            $view->headMeta()->setName('description', $this->m_d);
        }
    }
    
    public function useBreadcrumbs()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        
        $index = $view->headerMenu->findOneByRoute('index');
        
        if (!empty($index)) {
            $page = array(
                'module' => 'page',
                'controller' => 'page',
                'action' => 'view',
                'resource' => 'page:page',
                'privilege' => 'view',
                'label' => $this->title,
                'route' => 'pagePageView',
                'params' => array(
                    'alias' => $this->alias
                )
            );
            $index->addPage($page);
        }
    }
}