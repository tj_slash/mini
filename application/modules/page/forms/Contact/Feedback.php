<?php
class Page_Form_Contact_Feedback extends usEngine_Form_Form
{
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('Label', array('separator' => ' ', 'escape' => false, 'class' => 'sr-only')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    
    protected static $_buttonDecorator = array(
        'ViewHelper'
    );
    
    private static function createName()
    {
        $item = new Zend_Form_Element_Text('name');
        $item->setLabel('Имя');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Ваше имя');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setLabel('Электронная почта');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Адрес электронной почты');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createText()
    {
        $item = new Zend_Form_Element_Textarea('text');
        $item->addValidator(new Zend_Validate_StringLength(0, 4096, 'UTF-8'));
        $item->setAttrib('style', 'width:550px;height:216px');
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Ваш вопрос');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createSubmit()
    {
        $item = new Zend_Form_Element_Submit('feedback');
        $item->setLabel('Задать вопрос');
        $item->setAttrib('class', 'btn btn-primary');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    public function init()
    {
        $this->setMethod('post');
        $this->addElement(self::createName());
        $this->addElement(self::createEmail());
        $this->addElement(self::createText());
        $this->addElement(self::createSubmit());
    }
}