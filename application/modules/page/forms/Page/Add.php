<?php
class Page_Form_Page_Add extends usEngine_Form_Form
{
    private static function createTitleInput()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Title');
        $item->setDescription('Заголовок статичной страницы');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAliasInput()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Алиас');
        $item->setDescription('Алиас страницы (генерируется автоматически)');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private static function createTextTextarea()
    {
        $item = new Zend_Form_Element_Textarea('text');
        $item->setLabel('Text');
        $item->setDescription('Текст статичной страницы');
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createMetaTitle()
    {
        $item = new Zend_Form_Element_Text('m_t');
        $item->setLabel('Метаданные');
        $item->setDescription('Заголовок');
        $item->addValidator(new Zend_Validate_StringLength(0, 512, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createMetaKeywords()
    {
        $item = new Zend_Form_Element_Text('m_k');
        $item->setDescription('Ключевые слова');
        $item->addValidator(new Zend_Validate_StringLength(0, 512, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createMetaDescription()
    {
        $item = new Zend_Form_Element_Text('m_d');
        $item->setDescription('Описание');
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    public function init()
    {
        $this->setMethod('post')->setAttrib('enctype', 'multipart/form-data');
        $this->addElement(self::createTitleInput());
        $this->addElement(self::createAliasInput());
        $this->addElement(self::createTextTextarea());
        $this->addElement(self::createMetaTitle());
        $this->addElement(self::createMetaKeywords());
        $this->addElement(self::createMetaDescription());
        
        $this->addDisplayGroup(array('m_t','m_k','m_d'), 'meta_group');
        
        parent::initForm();
    }
}