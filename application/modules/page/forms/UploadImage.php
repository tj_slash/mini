<?php
class Page_Form_UploadImage extends usEngine_Form_Form
{
    private static function createFileInput()
    {
        $item = new Zend_Form_Element_File('img_file');
        $item->setRequired(true);
        $item->addValidator('Extension', false, 'jpg,png,gif,jpeg');
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createFileInput());
    }
}
