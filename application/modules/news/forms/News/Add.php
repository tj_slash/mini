<?php
class News_Form_News_Add extends usEngine_Form_Form
{
    private static function createCategory()
    {
        $item = new Zend_Form_Element_Select('category_id');
        $item->setLabel('Категория');
        $item->setDescription('Категория новости');
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->addMultiOption(NULL, 'Выберите категорию');
        $categories = Doctrine_Core::getTable('News_Model_Category')->getTree()->fetchTree();
        if(count($categories) > 0) {
            foreach($categories as $category) {
                $item->addMultiOption($category->id, str_repeat('——', $category->level) . ' ' . $category->title);
            }
        }
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок новости');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAlias()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Alias');
        $item->setDescription('Алиас категории новостей');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $regex = new Zend_Validate_Regex('/^[\-a-zA-Z0-9]+$/u');
        $item->addValidator($regex);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createText()
    {
        $item = new Zend_Form_Element_Textarea('text');
        $item->setLabel('Текст новости');
        $item->setDescription('Содержимое новости');
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    protected static function createImage()
    {
        $item = new Zend_Form_Element_File('image');
        $item->setRequired(true);
        $item->addValidator('Size', false, 1024*1024);
        $item->addValidator('Extension', false, 'jpeg,jpg,png,gif');
        $item->setDecorators(self::$_fileElementDecorator);
        $item->setDescription('Изображение (JPG, PNG, GIF, не более 1MB)');
        $item->setLabel('Изображение');
        return $item;
    }
    
    private static function createAuthor()
    {
        $item = new Zend_Form_Element_Text('author');
        $item->setLabel('Автор');
        $item->setDescription('Автор новости');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createSourceName()
    {
        $item = new Zend_Form_Element_Text('source_name');
        $item->setLabel('Источник');
        $item->setDescription('Источник новости');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createSorceUrl()
    {
        $item = new Zend_Form_Element_Text('source_url');
        $item->setLabel('Ссылка на источник');
        $item->setDescription('Ссылка на источник новости');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createTags()
    {
        $item = new Zend_Form_Element_Text('tags');
        $item->setLabel('Теги');
        $item->setDescription('Теги для новости');
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->isArray();
        $item->setIsArray(true);
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createCategory());
        $this->addElement(self::createTitle());
        $this->addElement(self::createAlias());
        $this->addElement(self::createText());
        $this->addElement(self::createImage());
        $this->addElement(self::createTags());
        $this->addElement(self::createAuthor());
        $this->addElement(self::createSourceName());
        $this->addElement(self::createSorceUrl());
        parent::initMeta();
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['image'])) {
            $image = $formdata['image'];
            $imageElement = $this->getElement('image');
            $imageElement->setDescription("<img src='/media/news/list_{$image}' style='max-width:400px'/>");
            $imageElement->setRequired(false);
        }
        
        if (!empty($formdata['id'])) {
            $newsTags = Doctrine_Query::create()->from('News_Model_NewsTag')->where('news_id = ?', $formdata['id'])->execute();
            $valueTags = array();
            if (count($newsTags) > 0) {
                foreach($newsTags as $newsTag) {
                    $tag = News_Model_TagTable::getInstance()->findOneById($newsTag->tag_id);
                    if ($tag) {
                        $valueTags[] = $tag->title;
                    }
                }
            }

            $view = $this->getView();
            $view->keywords = $valueTags;  
        }   
        return $formdata;
    }
    
    public function postSave($news)
    {
        $data = $this->getValues();
        
        Doctrine_Query::create()->delete('News_Model_NewsTag')->where('news_id = ?', $news->id)->execute();
        
        if (!empty($data['tags']) && count($data['tags']) > 0) {
            foreach($data['tags'] as $tagname) {
                if (!empty($tagname)) {
                    $tag = News_Model_TagTable::getInstance()->findOneByTitle($tagname);
                    if (!$tag) {
                        $tag = new News_Model_Tag();
                        $tag->title = $tagname;
                        $tag->save();
                    }
                    $newsTag = new News_Model_NewsTag();
                    $newsTag->news_id = $news->id;
                    $newsTag->tag_id = $tag->id;
                    $newsTag->save();
                }
            }
        }
    }
    
    public function preSave()
    {
        $data = array();
        $file = $this->image->getFileInfo();
        if (!empty($file['image']['name']) > 0) {
            $ext = explode(".", $file['image']['name']);
            $newName = uniqid() . '.' . $ext[count($ext)-1];
            
            $dest = APPLICATION_PATH . '/../public/media/news/';
            
            $this->image->addFilter('Rename', $dest . $newName);
            $this->image->receive();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(250)
                ->setHeight(250)
                ->setPath($dest)
                ->setPrefix('list_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(80)
                ->setHeight(80)
                ->setPath($dest)
                ->setPrefix('thumb_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            $data['image'] = $newName;
        }
        
        $data['author_id'] = Zend_Auth::getInstance()->getIdentity()->id;
        
        return $data;
    }
}
