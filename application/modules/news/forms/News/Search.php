<?php
class News_Form_News_Search extends Zend_Form
{    
    private static function createSearch()
    {
        $item = new Zend_Form_Element_Text('query', array('disableLoadDefaultDecorators' => true));
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim);
        $item->setAttrib('placeholder', 'Поиск по новостям');
        $item->setAttrib('class', 'query');
        $item->setAttrib('style', 'width:100%');
        $item->setDecorators(array('ViewHelper'));
        return $item;
    }
    
    public function init()
    {
        $this->setAction('/news/search');
        $this->setMethod('GET');
        $this->setAttrib('id', 'search');
        $this->addElement(self::createSearch());
    }
}