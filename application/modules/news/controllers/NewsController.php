<?php
class News_NewsController extends usEngine_Controller_Action
{
    public function init()
    {
        
    }
    
    public function listAction()
    {
        $paginator = News_Model_NewsTable::getInstance()->getPaginator();
        $this->view->paginator = $paginator;
    }
    
    public function viewAction ()
    {
        $alias = $this->getRequest()->getParam('alias', false);
        
        if (!$alias) {
            throw new Zend_Controller_Action_Exception('Не передан идентификатор', 404);
        }
        
        $news = News_Model_NewsTable::getInstance()->getByAlias($alias);
        
        if (!$news) {
            throw new Zend_Controller_Action_Exception('Новость не найдена', 404);
        }       
        
        if ($news->publish == false || $news->archive == true) {
            throw new Zend_Controller_Action_Exception('Новость удалена', 404);
        }
        
        $news->updateViews();
        $news->useMeta();
        
        $this->view->news = $news;
    }
    
    public function widgetCategoriesAction()
    {
        $categories = News_Model_CategoryTable::getInstance()->getCategories();
        $this->view->categories = $categories;
    }
    
    public function widgetCommentedAction()
    {
        $news = $this->getRequest()->getParam('news');
        $commented = News_Model_NewsTable::getInstance()->getCommented($news, 3);
        $this->view->news = $commented;
    }
    
    public function widgetRelatedAction()
    {
        $news = $this->getRequest()->getParam('news');
        $related = News_Model_NewsTable::getInstance()->getRelated($news, 3);
        $this->view->news = $related;
    }
    
    public function widgetLastAction()
    {
        $news = $this->getRequest()->getParam('news');
        $lasted = News_Model_NewsTable::getInstance()->getLast($news, 3);
        $this->view->news = $lasted;
    }
    
    public function widgetPopularAction()
    {
        $news = $this->getRequest()->getParam('news');
        $popular = News_Model_NewsTable::getInstance()->getPopular($news, 3);
        $this->view->news = $popular;
    }
    
    public function widgetViewedAction()
    {
        $news = $this->getRequest()->getParam('news');
        $viewed = News_Model_NewsTable::getInstance()->getViewed($news, 3);
        $this->view->news = $viewed;
    }
    
    public function widgetArchiveAction()
    {
        
    }
    
    public function widgetSeacrhAction()
    {
        $form = new News_Form_News_Search();
        $form->populate($this->getRequest()->getParams());
        $this->view->form = $form;
    }
    
    public function widgetTagCloudAction()
    {
        $cloud = News_Model_NewsTable::getInstance()->getCloud();
        $this->view->cloud = $cloud;
    }
}