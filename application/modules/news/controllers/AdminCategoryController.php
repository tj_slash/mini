<?php
class News_AdminCategoryController extends usEngine_Controller_Action
{    
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');        
        $this->_model = News_Model_CategoryTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Категории новостей');
            $grid->removeToolbars(array('list-archive', 'delete'));
        } else {
            $grid->setCaption('Архив категорий новостей');
            $grid->removeToolbars(array('list', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','title'=>'Наименование','alias'=>'Алиас','created'=>'Дата создания','publish'=>'Публикация'));
        $grid->setFormat('alias', array($this, 'formatAlias'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public static function formatAlias($row, $key)
    {
        return "<a href='/news/category/{$row->$key}' target='_blank'>{$row->$key}</a>";
    }
    
    public function addAction()
    {
        $this->formAction(new News_Form_Category_Add());
    }
}
