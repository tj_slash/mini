<?php
class News_AdminNewsController extends usEngine_Controller_Action
{    
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = News_Model_NewsTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Список новостей');
            $grid->removeToolbars(array('list', 'delete'));
        } else {
            $grid->setCaption('Архив новостей');
            $grid->removeToolbars(array('list-archive', 'archive'));
        }
        $grid->setColumns(array(
            'id'=>'ID',
            'image'=>'Изображение',
            'title'=>'Наименование',    
            'created'=>'Дата создания',
            'publish'=>'Публикация'
        ));
        $grid->setFormat('title', array($this, 'formatTitle'));
        $grid->setFormat('image', array($this, 'formatImage'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new News_Form_News_Add());
    }
    
    public static function formatTitle($row, $key)
    {
        return "<a href='/news/{$row->alias}' target='_blank'>{$row->$key}</a>";
    }
    
    public static function formatImage($row)
    {
        return "<img src='/media/news/{$row->image}' width='40' height='40' >";
    }
}
