<?php
class News_Model_CategoryTable extends usEngine_Doctrine_TreeTable
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('News_Model_Category');
    }
    
    public function getCategories()
    {
        $query = Doctrine_Query::create()
            ->from('News_Model_Category')
            ->addWhere('archive = ?', false)
            ->addWhere('publish = ?', true)
            ->addWhere('level = ?', 1)
            ->execute();
        return $query;
    }
}