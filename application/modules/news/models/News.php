<?php
class News_Model_News extends News_Model_Base_News
{
    protected $next = null;
    
    protected $previous = null;
    
    protected $authorNews = null;
    
    protected $tags = null;
    
    protected $publishDate = null;
    
    const COOKIE_VIEWED = 'newsViewed';
    
    public function getNext()
    {
        if (!$this->next) {
            $this->next = News_Model_NewsTable::getInstance()->getNext($this->get('id'));
        }
        return $this->next;
    }
    
    public function getPrevious()
    {
        if (!$this->previous) {
            $this->previous = News_Model_NewsTable::getInstance()->getPrevious($this->get('id'));
        }
        return $this->previous;
    }
    
    public function useMeta()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        
        if (!empty($this->m_t)) {
            $view->headTitle()->prepend($this->m_t);
        } else {
            $view->headTitle()->prepend($this->title);
        }
        
        if (!empty($this->m_k)) {
            $view->headMeta()->setName('keywords', $this->m_k);
        } else {
            $tags = $this->getTags();
            if ($tags) {
                $keywords = array();
                foreach ($tags as $tag) {
                    $keywords[] = $tag->title;
                }
                $view->headMeta()->setName('keywords', implode(', ', $keywords));
            } else {
                $view->headMeta()->setName('keywords', $this->title);
            }
        }
        
        if (!empty($this->m_d)) {
            $view->headMeta()->setName('description', $this->m_d);
        } else {
            $view->headMeta()->setName('description', $this->title);
        }
    }
    
    public function getTags()
    {
        if (!$this->tags) {
            $newsTags = Doctrine_Query::create()->from('News_Model_NewsTag')->addWhere('news_id = ?', $this->id)->execute();
            if (count($newsTags) > 0) {
                $tags_id = array();
                foreach($newsTags as $newsTag) {
                    $tags_id[] = $newsTag->tag_id;
                }

                $this->tags = Doctrine_Query::create()->from('News_Model_Tag')->whereIn('id', $tags_id)->execute();
            }
        }
        return $this->tags;
    }
    
    public function getAuthorNews()
    {
        if (!$this->authorNews) {
            if (!empty($this->author)) {
                $this->authorNews = $this->author;
            } else {
                $user = User_Model_UserTable::getInstance()->findOneById($this->author_id);
                if (!empty($user)) {
                    $this->authorNews = $user->nick;
                } else {
                    $this->authorNews = 'Администратор';
                }
            }
        }        
        return $this->authorNews;
    }
    
    public function getPublishDate()
    {
        if (!$this->publishDate) {
            $class = new usEngine_View_Helper_DateFormat;
            $this->publishDate = $class->dateFormat($this->created);
        }
        return $this->publishDate;
    }
    
    public function updateViews()
    {
        $this->views += 1;
        $this->save();
        
        $vieweds = array();
        if (!empty($_COOKIE[self::COOKIE_VIEWED])) $vieweds = explode(',',$_COOKIE[self::COOKIE_VIEWED]);
        if (!in_array($this->id, $vieweds)) $vieweds[] = $this->id;
        if (count($vieweds) > 20) $vieweds = array_slice($vieweds, -20, 20);
        setcookie(self::COOKIE_VIEWED, implode(',',$vieweds), 7*24*60*60 + time(), '/');
    }
}