<?php
class News_Model_NewsTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('News_Model_News');
    }

    public function getPaginator()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        
        $page = $request->getParam('page', 1);
        $category_alias = $request->getParam('category_alias', false);
        $tag_title = $request->getParam('tag_title', false);
        $year = $request->getParam('year', false);
        $month = $request->getParam('month', false);
        $day = $request->getParam('day', false);
        $search = $request->getParam('query', false);
        
        $limit = 10;
        $head_title = 'Новости';
        
        $query = Doctrine_Query::create()
            ->from('News_Model_News AS news')
            ->addWhere('news.publish = ?', true)
            ->addWhere('news.archive = ?', false)
            ->orderBy('news.created DESC');
        
        if ($category_alias) {
            $category = News_Model_CategoryTable::getInstance()->findOneByAlias($category_alias);
            if (!$category) {
                throw new Zend_Controller_Action_Exception('Категория не найдена', 404);
            }
            if ($category->publish == false || $category->archive == true) {
                throw new Zend_Controller_Action_Exception('Категория удалена', 404);
            }
            $query = $query->addWhere('news.category_id = ?', $category->id);
            $head_title = "{$category->title}";
        }
        
        if ($tag_title) {
            $tag = News_Model_TagTable::getInstance()->findOneByTitle($tag_title);
            if (!$tag) {
                throw new Zend_Controller_Action_Exception('Тег не найден', 404);
            }
            $query = $query->leftJoin('news.NewsTag AS newsTag')
                    ->addWhere('newsTag.tag_id = ?', $tag->id);
            $head_title = "Новости с тегом {$tag_title}";
        }
        
        if (!empty($year)) {
            $date = $year;
            $format = '%Y';
            $head_title = "Новости за {$year} год";
            if (!empty($month)) {
                $dateFormat = new usEngine_View_Helper_DateFormat;
                $months = $dateFormat->getMonths();
                $monthr = $dateFormat->getMonthr();
                $date = "{$date}-{$month}";
                $format = "{$format}-%m";
                $head_title = "Новости за {$months[(int)$month]} {$year} года";                
                if (!empty($day)) {
                    $date = "{$date}-{$day}";
                    $format = "{$format}-%d";
                    $head_title = "Новости за {$day} {$monthr[(int)$month]} {$year} года";
                }
            }
            $query = $query->addWhere('DATE_FORMAT(news.created, "' . $format . '") = ?', $date);
        }
        
        if (!empty($search)) {
            $query = Doctrine_Core::getTable('News_Model_News')->search($search, $query);
        }
        
        $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($query);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setDefaultItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);
        
        $view->headTitle()->prepend($head_title);
        
        return $paginator;
    }
    
    public function getByAlias($alias)
    {
        $news = Doctrine_Query::create()
            ->from('News_Model_News')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->addWhere('alias = ?', $alias)
            ->fetchOne();
        return $news;
    }
    
    public function getCommented(News_Model_News $news, $limit = 4)
    {
        $this->hasOne('Comment_Model_Comment as Comment', array(
             'local' => 'id',
             'foreign' => 'content_id',
             'owningSide' => true));
        
        $news = Doctrine_Query::create()
            ->select('news.*, COUNT(comment.id) AS count')
            ->from('News_Model_News AS news')
            ->leftJoin('news.Comment AS comment')
            ->leftJoin('comment.Type AS type')
            ->addWhere('news.publish = ?', true)
            ->addWhere('news.archive = ?', false)
            ->addWhere('comment.publish = ?', true)
            ->addWhere('comment.archive = ?', false)
            ->addWhere('type.publish = ?', true)
            ->addWhere('type.archive = ?', false)
            ->addWhere('type.alias = ?', 'news')
            ->addWhere('news.id <> ?', $news->id)
            ->orderBy('count DESC')
            ->groupBy('news.id')
            ->limit($limit)
            ->execute();
        return $news;
    }
    
    public function getRelated(News_Model_News $news, $limit = 4)
    {
        $news = Doctrine_Query::create()
            ->from('News_Model_News')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->addWhere('id <> ?', $news->id)
            ->addWhere('category_id = ?', $news->category_id)
            ->orderBy('created DESC')
            ->limit($limit)
            ->execute();
        return $news;
    }
    
    public function getLast(News_Model_News $news, $limit = 4)
    {
        $news = Doctrine_Query::create()
            ->from('News_Model_News')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->addWhere('id <> ?', $news->id)
            ->orderBy('created DESC')
            ->limit($limit)
            ->execute();
        return $news;
    }
    
    public function getPopular(News_Model_News $news, $limit = 4)
    {
        $news = Doctrine_Query::create()
            ->from('News_Model_News')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->addWhere('id <> ?', $news->id)
            ->orderBy('views DESC')
            ->limit($limit)
            ->execute();
        return $news;
    }
    
    public function getViewed(News_Model_News $news, $limit = 4)
    {
        if (!empty($_COOKIE[News_Model_News::COOKIE_VIEWED])) {
            $vieweds = explode(',',$_COOKIE[News_Model_News::COOKIE_VIEWED]);
            $news = Doctrine_Query::create()
                ->from('News_Model_News')
                ->addWhere('publish = ?', true)
                ->addWhere('archive = ?', false)
                ->addWhere('id <> ?', $news->id)
                ->whereIn('id', $vieweds)
                ->orderBy('FIELD(id, ' . implode(',', array_reverse($vieweds)) . ')')
                ->limit($limit)
                ->execute();
            return $news;
        }
        return null;
    }
    
    public function indexSearch()
    {        
        $table = Doctrine_Core::getTable('News_Model_News');
        $table->batchUpdateIndex();
    }
    
    public function getCloud()
    {
        $tags = Doctrine_Query::create()
            ->select('COUNT(*) AS count, tags.tag_id, tag.title')
            ->from('News_Model_NewsTag AS tags')
            ->leftJoin('tags.Tag AS tag')
            ->groupBy('tags.tag_id')
            ->execute();
        if (!empty($tags) && count($tags) > 0) {
            $resultTags = array();

            foreach ($tags as $tag) {
                $resultTags['tags'][] = array(
                    'title' => $tag->Tag->title, 
                    'weight' => $tag->count, 
                    'params' => array('url' => "/news/tag/{$tag->Tag->title}")
                );
            }

            $cloud = new Zend_Tag_Cloud($resultTags);
            $cloud->getTagDecorator()->setOptions(array('minFontSize' => 12, 'maxFontSize' => 16));
            return $cloud;
        }
        return false;
    }
}