<?php
class Catalog_Model_KuponDealTable extends usEngine_Doctrine_Table
{
    const COOKIE = 'coupon_code';
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Catalog_Model_KuponDeal');
    }
    
    public function isValidKuponCode($kupon_code)
    {
        $code = Catalog_Model_KuponCodeTable::getInstance()->findOneByCode($kupon_code);
        if ($code->status == 0 && $code->user_id === null && $code->order_id === null) {
            $deal = Catalog_Model_KuponDealTable::getInstance()->findOneById($code->deal_id);
            if ($deal->publish == true && $deal->archive == false) {
                $now = new Zend_Date();
                $start = new Zend_Date($deal->start_date);
                $end = new Zend_Date($deal->end_date);
                if ($now->isEarlier($end) && $now->isLater($start)) {
                    return $code;
                }
            }
        }
        return false;
    }
}