<?php
class Catalog_Model_FavoriteTable
{
    const COOKIE = 'favs';
    
    protected static $instance;

    /**
     * 
     * @return Catalog_Model_FavoriteTable
     */
    public static function getInstance() 
    {
        if (is_null(self::$instance)) self::$instance = new self;
        return self::$instance;
    }
    
    public function delete($product_id)
    {        
        if (self::getInstance()->exist($product_id)) {
            $cookies = $this->toArray();
            unset($cookies[$product_id]);
            $this->save($cookies);
        }
        return true;
    }
    
    public function add($product_id)
    {        
        if (!self::getInstance()->exist($product_id)) {
            $cookies = $this->toArray();
            $cookies[$product_id] = $product_id;
            $this->save($cookies);
        }
        return true;
    }
    
    public function exist($product_id)
    {
        if (in_array($product_id, $this->toArray())) return true;
        return false;
    }
    
    private function toArray()
    {
        if (!empty($_COOKIE[self::COOKIE])) return Zend_Json::decode($_COOKIE[self::COOKIE]);
        return array();
    }
    
    private function save($cookies)
    {
        setcookie(self::COOKIE, Zend_Json::encode($cookies), 30*24*60*60 + time(), '/');
    }
    
    public function getList($sort = 'id:asc', $page = 1, $limit = 16)
    {
        $products = $this->toArray();
        if (!empty($products)) {
            $sort = Catalog_Model_ProductTable::getInstance()->getSortByKey($sort);
            $limit = Catalog_Model_ProductTable::getInstance()->getLimitByKey($limit);
            $query = Doctrine_Query::create()
                    ->select('product.*, image.*, image.image AS image')
                    ->from('Catalog_Model_Product product')
                    ->leftJoin('product.Image AS image')
                    ->addWhere('product.archive = ?', false)
                    ->addWhere('product.publish = ?', true)
                    ->andWhereIn('product.id', $products)
                    ->orderBy($sort);
            $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($query);
            $paginator = new Zend_Paginator($adapter);
            $paginator->setDefaultItemCountPerPage($limit);
            $paginator->setCurrentPageNumber($page);
            return $paginator;
        }
        return null;
    }
    
    public function count()
    {
        $products = $this->getList();
        return count($products) ? count($products) : 0;
    }
}