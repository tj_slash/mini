<?php
class Catalog_Model_Category extends Catalog_Model_Base_Category
{
    public function useMeta()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $ancestors = $this->getNode()->getAncestors();
        if (!empty($ancestors)) {
            foreach ($ancestors as $ancestor) {
                $view->headTitle()->prepend(!empty($ancestor->m_t) ? $ancestor->m_t : $ancestor->title);
            } 
        }
        
        $view->headTitle()->prepend(!empty($this->m_t) ? $this->m_t : $this->title);
        $view->headMeta()->setName('keywords', $this->m_k);
        $view->headMeta()->setName('description', $this->m_d);
    }
    
    public function useBreadcrumbs()
    {
        if ($this->level > 0) {
            $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            $catalog = $view->headerMenu->findOneBy('id', 'catalog');
            if (!empty($catalog)) {
                $page = array(
                    'module' => 'catalog',
                    'controller' => 'category',
                    'action' => 'list',
                    'resource' => 'catalog:category',
                    'privilege' => 'list',
                    'label' => $this->title,
                    'route' => 'catalog',
                    'params' => array('alias' => $this->alias),
                    'pages' => Catalog_Model_CategoryTable::getInstance()->getCategoriesMenu($this) 
                );
                
                $ancestors = $this->getNode()->getAncestors();
                if (!empty($ancestors)) {
                    $ancestors = array_reverse($ancestors->toArray());
                    foreach ($ancestors as $ancestor) {
                        if ($ancestor['level'] > 0) {
                            $page = array(
                                'module' => 'catalog',
                                'controller' => 'category',
                                'action' => 'list',
                                'resource' => 'catalog:category',
                                'privilege' => 'list',
                                'label' => $ancestor['title'],
                                'route' => 'catalog',
                                'params' => array('alias' => $ancestor['alias']),
                                'pages' => array($page)
                            );
                        }
                    } 
                }
                
                $catalog->addPage($page);
            }
        }
    }
}