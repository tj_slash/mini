<?php
class Catalog_Model_CategoryTable extends usEngine_Doctrine_TreeTable
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Catalog_Model_Category');
    }
    
    public function getCategories()
    {
        $query = Doctrine_Query::create()
            ->from('Catalog_Model_Category')
            ->addWhere('archive = ?', false)
            ->addWhere('publish = ?', true)
            ->addWhere('level = ?', 1)
            ->execute();
        return $query;
    }
    
    public function getInternalCategories(Catalog_Model_Category $category)
    {
        $query = Doctrine_Query::create()
            ->from('Catalog_Model_Category')
            ->addWhere('archive = ?', false)
            ->addWhere('publish = ?', true)
            ->addWhere('level >= ?', $category->level)
            ->addWhere('lft >= ?', $category->lft)
            ->addWhere('rgt <= ?', $category->rgt)
            ->execute();
        return $query;
    }
    
    public function getParentsCategories(Catalog_Model_Category $category)
    {
        $query = Doctrine_Query::create()
            ->from('Catalog_Model_Category')
            ->addWhere('archive = ?', false)
            ->addWhere('publish = ?', true)
            ->addWhere('level <= ?', $category->level)
            ->addWhere('lft <= ?', $category->lft)
            ->addWhere('rgt >= ?', $category->rgt)
            ->execute();
        return $query;
    }
    
    public static function import($xml, $parent_id = 1)
    {
        if (!empty($xml->Группы->Группа)) {
            foreach ($xml->Группы->Группа as $xml_group) {
                $category = self::getInstance()->findOneByCId($xml_group->Ид);
                if (empty($category)) {
                    $category = new Catalog_Model_Category();
                    $category->c_id = $xml_group->Ид;
                    $category->publish = true;
                    
                    $root = self::getInstance()->findOneById($parent_id);
                    $category->getNode()->insertAsLastChildOf($root);
                }
                $category->title = $xml_group->Наименование;
                $category->save();
                $_SESSION[usEngine_1c_Catalog::S_CATEGORY_MAP][strval($xml_group->Ид)] = $category->id;
                self::import($xml_group, $category->id);
            }
        }
    }
}