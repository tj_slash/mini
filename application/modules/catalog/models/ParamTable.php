<?php
class Catalog_Model_ParamTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Catalog_Model_Param');
    }
    
    public function _getParams($category_id)
    {
        $category = Catalog_Model_CategoryTable::getInstance()->findOneById($category_id);
        if (!empty($category)) {
            $categories_id[] = $category->id;
            $categories = Catalog_Model_CategoryTable::getInstance()->getParentsCategories($category);
            if (!empty($categories) && count($categories) > 0) {
                foreach ($categories as $category) {
                    $categories_id[] = $category->id;
                }
            }
            $query = Doctrine_Query::create()
                ->from('Catalog_Model_Param')
                ->addWhere('archive = ?', false)
                ->addWhere('publish = ?', true)
                ->whereIn('category_id', $categories_id)
                ->execute();
            return $query;
        }
        return null;
    }
}