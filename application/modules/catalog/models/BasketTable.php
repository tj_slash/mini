<?php
class Catalog_Model_BasketTable
{
    const COOKIE = 'basket';
    
    protected static $instance;

    /**
     * 
     * @return Catalog_Model_BasketTable
     */
    public static function getInstance() 
    {
        if (is_null(self::$instance)) self::$instance = new self;
        return self::$instance;
    }
    
    public function delete($product_id)
    {
        if (self::getInstance()->exist($product_id)) {
            $cookies = $this->toArray();
            unset($cookies[$product_id]);
            self::getInstance()->save($cookies);
        }
        return true;
    }
    
    public function remove()
    {
        setcookie(self::COOKIE, null, 30*24*60*60 + time(), '/');
        return true;
    }
    
    public function add($product_id)
    {
        $cookies = self::getInstance()->toArray();
        if (self::getInstance()->exist($product_id)) $cookies[$product_id]['count']++;
        else $cookies[$product_id]['count'] = 1;
        self::getInstance()->save($cookies);
        return true;
    }
    
    public function exist($product_id)
    {
        $cookies = self::getInstance()->toArray();
        if (isset($cookies[$product_id]) && !empty($cookies[$product_id])) return true;
        return false;
    }
    
    public function toArray()
    {
        if (!empty($_COOKIE[self::COOKIE])) return Zend_Json::decode($_COOKIE[self::COOKIE]);
        return array();
    }
    
    private function save($cookies)
    {
        setcookie(self::COOKIE, Zend_Json::encode($cookies), 30*24*60*60 + time(), '/');
    }
    
    public function getProducts()
    {
        $products = self::getInstance()->toArray();
        if (!empty($products)) {
            $product_ids = array();
            foreach ($products as $id => $product) {
                $product_ids[] = $id;
            }
            return Doctrine_Query::create()
                ->from('Catalog_Model_Product AS product')
                ->whereIn('product.id', $product_ids)
                ->addWhere('product.publish = ?', true)
                ->addWhere('product.archive = ?', false)
                ->execute();
        }
        return false;
    }
    
    public function setCount($product_id, $count)
    {
        $cookies = self::getInstance()->toArray();
        $cookies[$product_id]['count'] = $count;
        $this->save($cookies);
        return true;
    }
    
    public function getDeliveryPrice($zip)
    {
        $request_params = array(
            'version' => '1.0',
            'senderCityId' => 286,
            'receiverCityPostCode' => $zip,
            'tariffId' => 137,
            'goods' => array(array('volume' => 0.015, 'weight' => 3))
        );
        $options = array(
            'http' => array(
                'method'  => 'POST',
                'content' => json_encode($request_params),
                'header'=>  "Content-Type: application/json\r\nAccept: application/json\r\n"
            )
        );
        $context  = stream_context_create( $options );
        $json = file_get_contents('http://api.edostavka.ru/calculator/calculate_price_by_json.php', false, $context);
        
        if (!empty($json)) {
            $result = Zend_Json::decode($json);
            if (!empty($result['result']['price'])) {
                return $result['result']['price'];
            }
        }
        
        return 0;
    }
    
    

    































    public function getList()
    {
        $products = $this->toArray();
        if (!empty($products)) {
            $ids = array();
            foreach ($products as $id => $product) $ids[] = $id;
            return Doctrine_Query::create()
                ->select('product.*, image.*, image.image AS image')
                ->from('Catalog_Model_Product AS product')
                ->leftJoin('product.Image AS image')
                ->whereIn('product.id', $ids)
                ->addWhere('product.publish = ?', true)
                ->addWhere('product.archive = ?', false)
                ->execute();
        }
    }
    
    public function count()
    {
        $products = $this->getList();
        return count($products) ? count($products) : 0;
    }
    
    public function notice($data)
    {
        $view = Zend_Layout::getMvcInstance()->getView();
        
        $view->user = $data;
        $view->products = $this->getList();
        $view->in_basket = $this->toArray();
        
        $fav = $view->mainMenu->findBy('route','favorites');
        $view->mainMenu->removePage($fav);
        
        $body = $view->render('emails/notice.phtml');
        
        $options = Zend_Registry::get('options');
        $email = !empty($options['support_email']) ? $options['support_email'] : 'myagkoe-zoloto@mail.ru';
        
        $subject = 'Новый заказ с сайта!';
        $mail = new Zend_Mail('utf-8');
        $mail->addTo('vakylenkox@gmail.com');
        $mail->setSubject($subject);
        $mail->setFrom($email, 'Мягкое Золото');
        $mail->setBodyHtml($body);
        $mail->send();
    }
}