<?php
class Catalog_Model_OrderTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Catalog_Model_Order');
    }
    
    public function addOrder($data)
    {
        $methods = Catalog_Form_Order_Delivery::getMethods();
        $product_ids = array();
        $total = 0;
        
        $order = new Catalog_Model_Order();
        $order->status = 0;
        $order->name = $data['name'];
        $order->comment = $data['comment'];
        $order->zip = $data['zip'];
        $order->region = $data['region'];
        $order->city = $data['city'];
        $order->address = $data['address'];
        $order->phone = $data['phone'];
        $order->email = $data['email'];
        $order->kupon_code = $data['kupon_code'];
        $order->delivery_method = $methods[$data['delivery']];
        $order->save();
        
        foreach($data['products'] as $product_id => $count) {
            $product = new Catalog_Model_Purchase();
            $product->order_id = $order->id;
            $product->product_id = $product_id;
            $product->amount = $count;
            $product->save();
            
            $product_ids[] = $product_id;
        }
        
        $products = Doctrine_Query::create()
            ->from('Catalog_Model_Product AS product')
            ->addWhere('product.publish = ?', true)
            ->addWhere('product.archive = ?', false)
            ->whereIn('id', $product_ids)
            ->execute();
        
        if (!empty($products) && count($products)) {
            foreach ($products as $product) {
                $total = $total + (double)str_replace(' ', '', $product->currentPrice) * $data['products'][$product->id]['count'];
            }
        }
        
        if (!empty($data['zip']) && !empty($data['delivery']) && $data['delivery'] == 2) {
            $delivery = Catalog_Model_BasketTable::getInstance()->getDeliveryPrice($data['zip']);
        }
        
        if (!empty($data['kupon_code'])) {
            $discount = Catalog_Model_KuponDealTable::getInstance()->isValidKuponCode($data['kupon_code']);
            $discount->order_id = $order->id;
            $discount->status = 1;
            $discount->save();
        }
        
        $order->delivery = (float)$delivery;
        $order->discount = (float)($total/100)*$discount->KuponDeal->discount;
        $order->price = (float)$total;
        $order->save();
        
        Catalog_Model_BasketTable::getInstance()->remove();
    }
    
    
    public function newOrder()
    {
        $order = new Catalog_Model_Order();
        $order->c_id = uniqid('order_');
        $order->status = 0;
        return $order;
    }
    public function getOrderForEdit($id)
    {
        if(!isset($id)) return false;
        $order = Doctrine_Query::create()
            ->from('Catalog_Model_Order as order')
            ->leftJoin('order.Purchase as purchase')
            ->leftJoin('purchase.Product as product')
            ->where('order.id = ?', $id)
            ->fetchArray();
//        var_dump($order);die("dump \$order");
        if(!$order) return false;
        $items = array();
        $summ = 0;
        foreach($order[0]['Purchase'] as $item)
        {
            if(!isset($item['Product']) || !is_array($item['Product']))
                continue;
            $i = array();
//            var_dump($item);die("dump \$item");
            $i['title'] = $item['Product']['title'];
            $i['price'] = $item['Product']['price'];
            $i['amount'] = $item['amount'];
            $summ += $item['Product']['price']*$item['amount'];
            $items[] = $i;
        }
        $order[0]['items'] = $items;
        $order[0]['summ'] = $summ;
        unset($order[0]['Purchase']);
        return $order[0];
    }
    public function getOrderForExport($id)
    {
        if(!isset($id)) return false;
        $order = Doctrine_Query::create()
            ->from('Catalog_Model_Order as order')
            ->leftJoin('order.Purchase as purchase')
            ->leftJoin('purchase.Product as product')
            ->leftJoin('product.Category as category')
            ->where('order.id = ?', $id)
            ->fetchArray();
//        var_dump($order);die("dump \$order");
        if(!$order) return false;
        $items = array();
        $summ = 0;
        foreach($order[0]['Purchase'] as $item)
        {
            if(!isset($item['Product']) || !is_array($item['Product']))
                continue;
            $i = array();
//            var_dump($item);die("dump \$item");
            $i['title'] = $item['Product']['title'];
            $i['price'] = $item['Product']['price'];
            $i['c_id'] = $item['Product']['c_id'];
            $i['category_c_id'] = $item['Product']['Category']['c_id'];
            $i['amount'] = $item['amount'];
            $summ += $item['Product']['price']*$item['amount'];
            $items[] = $i;
        }
        $order[0]['items'] = $items;
        $order[0]['summ'] = $summ;
        unset($order[0]['Purchase']);
        return $order;
    }
    public function setStatus($id, $status)
    {
        $order = self::getInstance()->findOneById($id);
        if(!$order) return false;
        else {
            try {
                $order->status = $status;
                $order->save();
            }
            catch(Exception $e) { return false;}
        }
        return true;
    }

    public static function exportOrders()
    {
//        $log = new Index_Model_Log();
//        $log->message = (' in Catalog_Model_OrderTable export' );
//        $log->save();

        $options = Zend_Registry::get('options');
        $last_sync = isset($options['last_1c']) ? $options['last_1c'] :'0000-00-00 00:00:00';
        $query = Doctrine_Query::create()
            ->select('id')
            ->from('Catalog_Model_Order')
            ->where('updated >= ?', $last_sync)
            ->fetchArray();
        $new_orders = array();
//        Zend_Debug::dump($query);die("dump \$query");
        foreach($query as $record) {
            $order = Catalog_Model_OrderTable::getInstance()->getOrderForExport($record['id']);
            if($order) $new_orders[] = $order;
        }
        return $new_orders;
    }

    public static function importOrders($xml)
    {
        foreach($xml->Документ as $xml_order)
        {
            $order_comment_array = explode(' ',$xml_order->Комментарий);
            $order_id = $order_comment_array[1];
            if(isset($order_id) && (int) $order_id > 0) {
                $order = Catalog_Model_OrderTable::getInstance()->findOneById((int) $order_id);
                if($order) {

                    if(isset($xml_order->ЗначенияРеквизитов->ЗначениеРеквизита))
                    foreach($xml_order->ЗначенияРеквизитов->ЗначениеРеквизита as $r)
                    {
                        switch ($r->Наименование) {
                        case 'Проведен':
                            $proveden = ($r->Значение == 'true');
                            break;
                        case 'ПометкаУдаления':
                            $udalen = ($r->Значение == 'true');
                            break;
                        }
                    }

                    if($udalen)
                        $order->status = 3;
                    elseif($proveden)
                        $order->status = 2;

                    $order->save();
                }
            }
        }
    }

}