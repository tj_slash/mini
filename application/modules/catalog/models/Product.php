<?php
class Catalog_Model_Product extends Catalog_Model_Base_Product
{
    protected $_currentPrice = null;
    
    protected $_originalPrice = null;
    
    protected $_discountPrice = null;
    
    protected $_currentImage = null;
    
    protected $_images = null;
    
    const COOKIE_VIEWED = 'productViewed';
    
    public function getCurrentPrice()
    {
        if (!$this->_currentPrice) {
            $price = $this->price;
            if (!empty($this->discount) && $this->discount > 0) {
                $price -= $this->discount;
            } elseif(!empty($this->discount_percent) && $this->discount_percent > 0) {
                $price = $price - ($price / 100 * $this->discount_percent);
            }
            $this->_currentPrice = number_format((float)$price, 2, '.', ' ');
        }
        return $this->_currentPrice;
    }
    
    public function getOriginalPrice()
    {
        if (!$this->_originalPrice) {
            if ($this->discount > 0 || $this->discount_percent > 0) {
                $this->_originalPrice = number_format((double)$this->price, 2, '.', ' ');
            }
        }
        return $this->_originalPrice;
    }
    
    public function getDiscountPrice()
    {
        if (!$this->_discountPrice) {
            if ($this->discount > 0) {
                $this->_discountPrice = number_format((double)$this->discount, 2, '.', ' ') . ' руб.';
            } elseif($this->discount_percent > 0) {
                $this->_discountPrice = "$this->discount_percent %";
            }
        }
        return $this->_discountPrice;
    }
    
    public function getImage()
    {
        if (!$this->_currentImage) {
            $image = Doctrine_Query::create()
                ->from('Catalog_Model_Image')
                ->addWhere('product_id = ?', $this->id)
                ->orderBy('order DESC')
                ->fetchOne();
            if (!empty($image)) $image = $image->image;
            else $image = 'default.png';
            $this->_currentImage = $image;
        }
        return $this->_currentImage;
    }
    
    public function getImages()
    {
        if (!$this->_images) {
            $images = Doctrine_Query::create()
                ->from('Catalog_Model_Image')
                ->addWhere('product_id = ?', $this->id)
                ->orderBy('order DESC')
                ->execute();
            if (count($images) > 0) {
                $this->_images = $images;
            } else {
                $this->_images = array();
            }
        }
        return $this->_images;
    }
    
    public function updateViews()
    {
        $this->views += 1;
        $this->save();
        
        $vieweds = array();
        if (!empty($_COOKIE[self::COOKIE_VIEWED])) $vieweds = explode(',',$_COOKIE[self::COOKIE_VIEWED]);
        if (!in_array($this->id, $vieweds)) $vieweds[] = $this->id;
        if (count($vieweds) > 20) $vieweds = array_slice($vieweds, -20, 20);
        setcookie(self::COOKIE_VIEWED, implode(',',$vieweds), 7*24*60*60 + time(), '/');
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public function useMeta()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        
        $category = Catalog_Model_CategoryTable::getInstance()->findOneById($this->category_id);
        
        $ancestors = $category->getNode()->getAncestors();
        if (!empty($ancestors)) {
            foreach ($ancestors as $ancestor) {
                $view->headTitle()->prepend(!empty($ancestor->m_t) ? $ancestor->m_t : $ancestor->title);
            } 
        }
        $options = Zend_Registry::get('options');
        $view->headTitle()->prepend(!empty($category->m_t) ? $category->m_t : $category->title);
        $view->headTitle()->prepend(!empty($this->m_t) ? $this->m_t : $this->title);
        $view->headMeta()->setName('keywords', isset($this->m_k) && !empty($this->m_k) ? $this->m_k : $this->title.', купить '. $this->title);
        $view->headMeta()->setName('description', isset($this->m_d) && !empty($this->m_d) ? $this->m_d : 'Купить '. $this->title .' в интернет-магазине '. $options["setting"]["index"]["site_name"] );
    }
    
    public function useBreadcrumbs()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        
        $index = $view->headerMenu->findOneByRoute('catalog');
        
        if (!empty($index)) {
            
            $category = Catalog_Model_CategoryTable::getInstance()->findOneById($this->category_id);
            
            $page = array(
                'module' => 'catalog',
                'controller' => 'category',
                'action' => 'list',
                'resource' => 'catalog:category',
                'privilege' => 'list',
                'label' => $category->title,
                'route' => 'catalog',
                'params' => array('alias' => $category->alias),
                'pages' => array(array(
                    'module' => 'catalog',
                    'controller' => 'product',
                    'action' => 'view',
                    'resource' => 'catalog:product',
                    'privilege' => 'view',
                    'label' => $this->title,
                    'route' => 'product',
                    'params' => array('alias' => $this->alias)
                ))
            );
            
            $ancestors = $category->getNode()->getAncestors();
            if (!empty($ancestors)) {
                $ancestors = array_reverse($ancestors->toArray());
                foreach ($ancestors as $ancestor) {
                    if ($ancestor['level'] > 0) {
                        $page = array(
                            'module' => 'catalog',
                            'controller' => 'category',
                            'action' => 'list',
                            'resource' => 'catalog:category',
                            'privilege' => 'list',
                            'label' => $ancestor['title'],
                            'route' => 'catalog',
                            'params' => array('alias' => $ancestor['alias']),
                            'pages' => array($page)
                        );
                    }
                } 
            }
            
            $index->addPage($page);
        }
        
        $index = $view->categoriesMenu->findOneBy('id',"category_{$this->category_id}");
        if (!empty($index)) {
            $index->active = true;
        }
    }
}