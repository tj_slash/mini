<?php

/**
 * Catalog_Model_Base_KuponDeal
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $title
 * @property integer $discount
 * @property datetime $start_date
 * @property datetime $end_date
 * @property bool $publish
 * @property bool $archive
 * @property Doctrine_Collection $KuponCode
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class Catalog_Model_Base_KuponDeal extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('catalog__model__kupon_deal');
        $this->hasColumn('id', 'integer', 11, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             'length' => '11',
             ));
        $this->hasColumn('title', 'string', 255, array(
             'type' => 'string',
             'notnull' => true,
             'length' => '255',
             ));
        $this->hasColumn('discount', 'integer', 3, array(
             'type' => 'integer',
             'notnull' => true,
             'length' => '3',
             ));
        $this->hasColumn('start_date', 'datetime', null, array(
             'type' => 'datetime',
             'notnull' => true,
             ));
        $this->hasColumn('end_date', 'datetime', null, array(
             'type' => 'datetime',
             'notnull' => true,
             ));
        $this->hasColumn('publish', 'bool', null, array(
             'type' => 'bool',
             'notnull' => true,
             'default' => 0,
             ));
        $this->hasColumn('archive', 'bool', null, array(
             'type' => 'bool',
             'notnull' => true,
             'default' => 0,
             ));

        $this->option('collate', 'utf8_general_ci');
        $this->option('charset', 'utf8');
        $this->option('type', 'InnoDB');
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Catalog_Model_KuponCode as KuponCode', array(
             'local' => 'id',
             'foreign' => 'deal_id'));
    }
}