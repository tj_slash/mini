<?php
class Catalog_Model_ProductTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Catalog_Model_Product');
    }
    
    public function getPaginator($params)
    {
        $page = $params['page'];
        $brand = $params['brand'];
        $category = $params['category'];
        $search = $params['query'];
        $limit = $params['limit'];
        $order = $params['order'];
        $dir = $params['dir'];
        
        
        
        $query = Doctrine_Query::create()
            ->from('Catalog_Model_Product AS product')
            ->addWhere('product.publish = ?', true)
            ->addWhere('product.archive = ?', false)
            ->orderBy("product.{$order} {$dir}");
            
        if (!empty($brand)) {
            $query = $query->addWhere('brand_id = ?', $brand->id);
        } else {
            $categories = Catalog_Model_CategoryTable::getInstance()->getInternalCategories($category);
            $internalCategoriesId[] = $category->id;
            if (count($categories) > 0) {
                foreach ($categories as $category) {
                    $internalCategoriesId[] = $category->id;
                }
                $query = $query->whereIn('category_id', $internalCategoriesId);
            }
        }
        
        if (!empty($search)) {
            $query = Doctrine_Core::getTable('Catalog_Model_Product')->search($search, $query);
        }
        
        $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($query);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setDefaultItemCountPerPage($limit);
        $paginator->setCurrentPageNumber($page);        
        return $paginator;
    }
    
    public function getRelated(Catalog_Model_Product $product = null, $limit = 4)
    {
        $products = Doctrine_Query::create()
            ->from('Catalog_Model_Product')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->orderBy('created DESC')
            ->limit($limit);
        if ($product) {
            $products->addWhere('id <> ?', $product->id);
            $products->addWhere('category_id = ?', $product->category_id);
        }
        return $products->execute();
    }
    
    public function getCommented(Catalog_Model_Product $product = null, $limit = 4)
    {
        $this->hasOne('Comment_Model_Comment as Comment', array(
             'local' => 'id',
             'foreign' => 'content_id',
             'owningSide' => true));
        
        $products = Doctrine_Query::create()
            ->select('product.*, COUNT(comment.id) AS count')
            ->from('Catalog_Model_Product AS product')
            ->leftJoin('product.Comment AS comment')
            ->leftJoin('comment.Type AS type')
            ->addWhere('product.publish = ?', true)
            ->addWhere('product.archive = ?', false)
            ->addWhere('comment.publish = ?', true)
            ->addWhere('comment.archive = ?', false)
            ->addWhere('type.publish = ?', true)
            ->addWhere('type.archive = ?', false)
            ->addWhere('type.alias = ?', 'product')
            ->addWhere('product.id <> ?', $product->id)
            ->orderBy('count DESC')
            ->groupBy('product.id')
            ->limit($limit)
            ->execute();
        return $products;
    }
    
    public function getLast(Catalog_Model_Product $product = null, $limit = 4)
    {
        $products = Doctrine_Query::create()
            ->from('Catalog_Model_Product')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->orderBy('created DESC')
            ->limit($limit);
        if ($product) {
            $products->addWhere('id <> ?', $product->id);
        }
        return $products->execute();
    }
    
    public function getPopular(Catalog_Model_Product $product = null, $limit = 4)
    {
        $products = Doctrine_Query::create()
            ->from('Catalog_Model_Product')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->orderBy('views DESC')
            ->limit($limit);
        if ($product) {
            $products->addWhere('id <> ?', $product->id);
        }
        return $products->execute();
    }
    
    public function getViewed(Catalog_Model_Product $product = null, $limit = 4)
    {
        if (!empty($_COOKIE[Catalog_Model_Product::COOKIE_VIEWED])) {
            $vieweds = explode(',',$_COOKIE[Catalog_Model_Product::COOKIE_VIEWED]);
            $products = Doctrine_Query::create()
                ->from('Catalog_Model_Product')
                ->addWhere('publish = ?', true)
                ->addWhere('archive = ?', false)
                ->addWhere('id <> ?', $product->id)
                ->whereIn('id', $vieweds)
                ->orderBy('FIELD(id, ' . implode(',', array_reverse($vieweds)) . ')')
                ->limit($limit)
                ->execute();
            return $products;
        }
        return null;
    }
    
    public function getDicounted(Catalog_Model_Product $product = null, $limit = 4)
    {
        $products = Doctrine_Query::create()
            ->from('Catalog_Model_Product')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->addWhere('discount > ?', 0.0)
            ->orWhere('discount_percent > ?', 0.0)
            ->orderBy('created DESC')
            ->limit($limit);
        if ($product) {
            $products->addWhere('id <> ?', $product->id);
        }
        return $products->execute();
    }
    
    public function indexSearch()
    {        
        $table = Doctrine_Core::getTable('Catalog_Model_Product');
        $table->batchUpdateIndex();
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static function import($xml)
    {
        @list($c_id, $v_id) = explode('#', $xml->Ид);
        if (empty($v_id)) $v_id = '';
        if(isset($xml->Группы->Ид)) {
            $category = Catalog_Model_CategoryTable::getInstance()->findOneByCId($xml->Группы->Ид);
            if (!empty($category)) {
                $product = self::getInstance()->findOneByCId($c_id);
                if (empty($product)) {
                    $product = new Catalog_Model_Product();
                    $product->c_id = $c_id;
                    $product->publish = true;         
                }
                if(isset($xml->Изготовитель->Ид)) {
                    $brand = Catalog_Model_BrandTable::getInstance()->findOneByCId($xml->Изготовитель->Ид);
                    if(empty($brand)){
                        $brand = new Catalog_Model_Brand();
                        $brand->c_id = $xml->Изготовитель->Ид;;
                        $brand->publish = true;
                    }
                    $brand->title = $xml->Изготовитель->Наименование;
                    $brand->save();
                    $product->brand_id = $brand->id;
                }

                $product->title = $xml->Наименование;
                $product->description = $xml->Описание;
                $product->category_id = $category->id;
                $product->sku = $xml->Артикул;
                $product->price = 0;
                if(isset($xml->Статус) && $xml->Статус == 'Удален')
                    $product->publish = false;
                else
                    $product->publish = true;
                $product->save();
                
                Catalog_Model_ImageTable::import($xml, $product->id);
            }
        }
    }
}