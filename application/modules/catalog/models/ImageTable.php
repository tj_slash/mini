<?php
class Catalog_Model_ImageTable extends usEngine_Doctrine_Table
{
    const IMG_PATH = '/media/products/';
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Catalog_Model_Image');
    }
    
    public static function import($xml, $product_id)
    {
        if (isset($xml->Картинка)) {
            Doctrine_Query::create()->delete('Catalog_Model_Image')->where('product_id = ?', $product_id)->execute();
            $dir = realpath(usEngine_1c_Catalog::getInstance()->getDir()) . '/';
            for ($i = 0; $i < count($xml->Картинка); $i++) {
                $image_name = basename($xml->Картинка[$i]);
                if(!empty($image_name) && is_file($dir . $image_name)) {
                    rename(
                        $dir . $image_name, 
                        APPLICATION_PATH . '/../public' . Catalog_Model_ImageTable::IMG_PATH . $image_name
                    );
                    $image = new Catalog_Model_Image();
                    $image->product_id = $product_id;
                    $image->image = $image_name;
                    $image->save();
                }
            }
        }
    }
}