<?php
class Catalog_Model_ValueTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Catalog_Model_Value');
    }
}