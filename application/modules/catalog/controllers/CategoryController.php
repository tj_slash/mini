<?php
class Catalog_CategoryController extends usEngine_Controller_Action
{
    const COOKIE_MODE = 'mode';
    
    public function init()
    {
        
    }
    
    public function listAction()
    {
        $params['page'] = $this->getRequest()->getParam('page', 1);
        $params['query'] = $this->getRequest()->getParam('query', null);
        $params['order'] = $this->getRequest()->getParam('order', 'created');
        $params['dir'] = $this->getRequest()->getParam('dir', 'DESC');
        $params['limit'] = $this->getRequest()->getParam('limit', 15);
        $category = null;
        
        $category_alias = $this->getRequest()->getParam('category_alias', null);
        $brand_alias = $this->getRequest()->getParam('brand_alias', null);
        
        $mode = $this->getRequest()->getParam('mode', false);
        
        if (!empty($brand_alias)) {
            $brand = Catalog_Model_BrandTable::getInstance()->findOneByAlias($brand_alias);
            if (empty($brand)) throw new Zend_Controller_Action_Exception('Проиводитель не найден');        
            if ($brand->archive === true) throw new Zend_Controller_Action_Exception('Проиводитель удален');
            if ($brand->publish === false) throw new Zend_Controller_Action_Exception('Проиводитель не опубликованн');
        } else {
            if (!$category_alias) $category = Catalog_Model_CategoryTable::getInstance()->findOneByLevel(0);
            else $category = Catalog_Model_CategoryTable::getInstance()->findOneByAlias($category_alias);
            
            if (empty($category)) throw new Zend_Controller_Action_Exception('Категория не найдена');
            if ($category->archive === true) throw new Zend_Controller_Action_Exception('Категория удалена');
            if ($category->publish === false) throw new Zend_Controller_Action_Exception('Категория не опубликованна');
        }
        
        if ($mode) setcookie('mode', $mode, 7*24*60*60 + time(), '/');
        else {
            $mode = 'grid';
            if (!empty($_COOKIE['mode']) && $_COOKIE['mode'] === 'list') $mode = 'list';
        }
        
        $params['category'] = $category;
        $params['brand'] = $brand;
        
        $paginator = Catalog_Model_ProductTable::getInstance()->getPaginator($params);
        
        $this->view->paginator = $paginator;
        $this->view->mode = $mode;
        $this->view->order = $params['order'];
        $this->view->dir = $params['dir'];
        $this->view->limit = $params['limit'];
    }
    
    public function widgetCategoriesAction()
    {
        $categories = Catalog_Model_CategoryTable::getInstance()->getCategories();
        $this->view->categories = $categories;
    }
}
