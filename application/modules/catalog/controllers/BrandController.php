<?php
class Catalog_BrandController extends usEngine_Controller_Action
{
    public function init()
    {
        
    }
    public function widgetBrandsAction()
    {
        $alias =  $this->getRequest()->getParam('alias', false);
        $this->view->alias = $alias ? $alias : '';
        $this->view->brands = Catalog_Model_BrandTable::getInstance()->getForWidget(60);
//        Zend_Debug::dump($this->view->brands->toArray());die("dump \$this->view->brands->toArray()");
    }
    public function listAction()
    {
//        $a = Catalog_Model_BrandTable::getInstance()->countProducts();
        $alias = $this->getRequest()->getParam('alias',false);
        if(!$alias) return $this->_redirect('/');

        $page = $this->getRequest()->getParam('page', 1);
        $limit = $this->getRequest()->getParam('limit', 16);
        $sort = $this->getRequest()->getParam('sort', 'id:asc');

        if (empty($alias)) throw new Zend_Controller_Action_Exception('Не передан идентификатор');

        $brand = Catalog_Model_BrandTable::getInstance()->findOneByAlias($alias);

        if (empty($brand)) throw new Zend_Controller_Action_Exception('Не найден Бренд ');

        if ($brand->archive === true) throw new Zend_Controller_Action_Exception('Бренд удалена');

        if ($brand->publish === false) throw new Zend_Controller_Action_Exception('Бренд не опубликован');

        $products = Catalog_Model_ProductTable::getInstance()->getProductsByBrand($brand, $sort, $page, $limit);
        $this->view->products = $products;

        $this->view->sort = $sort;
        $this->view->sorts = Catalog_Model_ProductTable::getInstance()->getSorts();

        $this->view->limit = $limit;
        $this->view->limits = Catalog_Model_ProductTable::getInstance()->getLimits();

        $brand->useMeta();

        $brand->useBreadcrumbs();

        $view = $this->getRequest()->getParam('view', false);
        if ($view) {
            setcookie('view', $view, 7*24*60*60 + time(), '/');
        } else {
            if (empty($_COOKIE['view']) || $_COOKIE['view'] === 'grid') $view = 'grid';
            else $view = 'list';
        }
        $this->view->view = $view;

    }
}