<?php
class Catalog_AdminKuponController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Catalog_Model_KuponDealTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Список акций');
            $grid->removeToolbars(array('list-archive', 'delete'));
        } else {
            $grid->setCaption('Архив акций');
            $grid->removeToolbars(array('list', 'archive'));
        }
        $grid->setColumns(array(
            'id'=>'ID',
            'title'=>'Наименование',
            'discount'=>'Скидка',
            'start_date'=>'Начало',
            'end_date'=>'Конец',
            'publish'=>'Публикация'
        ));
        $grid->setFormat('discount', array($this, 'formatDiscount'));
        $grid->setFormat('start_date', 'usTable_Format_Date');
        $grid->setFormat('end_date', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public static function formatDiscount($row, $key)
    {
        return "{$row->$key}%";
    }
    
    public function addAction()
    {
        $this->formAction(new Catalog_Form_Kupon_Add());
    }
}