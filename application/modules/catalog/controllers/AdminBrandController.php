<?php
class Catalog_AdminBrandController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Catalog_Model_BrandTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Производители');
            $grid->removeToolbars(array('list-archive', 'delete'));
        } else {
            $grid->setCaption('Архив производителей');
            $grid->removeToolbars(array('list', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','title'=>'Наименование','created'=>'Дата создания','publish'=>'Публикация'));
        $grid->setFormat('title', array($this, 'formatTitle'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new Catalog_Form_Brand_Add());
    }
    
    public static function formatTitle($row)
    {
        return "<a href='/catalog/brand/{$row->alias}' target='_blank'>{$row->title}</a>";
    }
}