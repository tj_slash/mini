<?php
class Catalog_BasketController extends usEngine_Controller_Action
{
    public function addAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $product_id = $this->getRequest()->getParam('product_id', null);
        if ($product_id) Catalog_Model_BasketTable::getInstance()->add($product_id);
        $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));
    }
    
    public function deleteAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $product_id = $this->getRequest()->getParam('product_id', null);
        if ($product_id) Catalog_Model_BasketTable::getInstance()->delete($product_id);
        $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));
    }
    
    public function widgetHeaderAction()
    {
        $products = Catalog_Model_BasketTable::getInstance()->getProducts();
        $cookies = Catalog_Model_BasketTable::getInstance()->toArray();
        
        $count = $total = 0;
        
        if (!empty($products)) {
            foreach ($products as $product) {
                $total = $total + (double)str_replace(' ', '', $product->currentPrice) * $cookies[$product->id]['count'];
                $count = $count + $cookies[$product->id]['count'];
            } 
        }

        $this->view->products = $products;
        $this->view->total = $total;
        $this->view->count = $count;
        $this->view->cookies = $cookies;
    }
    
    public function viewAction()
    {
        $discount = false;
        $delivery = 0;
        $formdata = array();
        
        $formContacts = new Catalog_Form_Order_Contact();
        $formDelivery = new Catalog_Form_Order_Delivery();
        $formKupon = new Catalog_Form_Order_Kupon();
        $formPayment = new Catalog_Form_Order_Payment();
        
        if($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if ($this->getRequest()->getParam('checkout', false)) {
                if ($formContacts->isValid($formdata)) {
                    if ($formDelivery->isValid($formdata)) {
                        if ($formKupon->isValid($formdata)) {
                            if ($formPayment->isValid($formdata)) {
                                $data['products'] = $formdata['products'];
                                $data = array_merge($formContacts->getValues(), $data);
                                $data = array_merge($formDelivery->getValues(), $data);
                                $data = array_merge($formKupon->getValues(), $data);
                                $data = array_merge($formPayment->getValues(), $data);
                                Catalog_Model_OrderTable::getInstance()->addOrder($formdata);
                                $this->_redirect('/basket/success-order');
                            }
                        }
                    }
                }
            } elseif ($this->getRequest()->getParam('recount', false)) {
                if (!empty($formdata['products']) && count($formdata['products']) > 0) {
                    foreach($formdata['products'] as $product_id => $count) {
                        Catalog_Model_BasketTable::getInstance()->setCount($product_id, $count);
                    }
                    $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));
                }
            } else {
                if (!empty($formdata['kupon_code'])) {
                    $code = Catalog_Model_KuponDealTable::getInstance()->isValidKuponCode($formdata['kupon_code']);
                    $discount = $code->KuponDeal->discount;
                    $this->view->kupon_code = $formdata['kupon_code'];
                }
                if (!empty($formdata['zip']) && !empty($formdata['delivery']) && $formdata['delivery']==2) {
                    $delivery = Catalog_Model_BasketTable::getInstance()->getDeliveryPrice($formdata['zip']);
                }
            }
        }
        $products = Catalog_Model_BasketTable::getInstance()->getProducts();
        $this->view->products = $products;
        
        $cookies = Catalog_Model_BasketTable::getInstance()->toArray();
        $this->view->cookies = $cookies;
        
        $this->view->discount = $discount;
        
        $this->view->delivery = $delivery;
        
        $this->view->formContacts = $formContacts->populate($formdata);
        $this->view->formDelivery = $formDelivery->populate($formdata);
        $this->view->formKupon = $formKupon->populate($formdata);
        $this->view->formPayment = $formPayment->populate($formdata);
    }
    
    public function successOrderAction()
    {
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
//    
//    
//    
//    public function step2Action()
//    {
//        $form = new Catalog_Form_Order_User();
//        if ($this->getRequest()->isPost()) {
//            $formdata = $this->getRequest()->getParams();
//            if ($form->isValid($formdata)) {
//                $products = Catalog_Model_BasketTable::getInstance()->getList();
////                Zend_Debug::dump($products);die("dump \$products");
//                $basket = Catalog_Model_BasketTable::getInstance()->toArray();
////                Zend_Debug::dump($basket);die("dump \$basket");
//                $data = $form->getValues();
//                $status = 0;
//                $date = date('Y-m-d H:i:s', time());
//                $order = Catalog_Model_OrderTable::getInstance()->add(array_merge($data,array('status'=>$status,'date'=>$date)));
//
//                $total_prize = 0;
//                foreach($products as $product) {
//                    $purchase = new Catalog_Model_Purchase();
//                    $purchase->order_id = $order->id;
//                    $purchase->product_id = $product->id;
//                    $purchase->price = $product->price;
//                    $purchase->amount = isset($basket[$product->id]['amount']) ? $basket[$product->id]['amount'] : 1 ;
//                    $purchase->save();
//                    $total_prize += $purchase->price * $purchase->amount;
//                }
//                $order->total_price = $total_prize;
//                $order->save();
//
//                $options = Zend_Registry::get('options');
//                $client = new usEngine_Robokassa_Client($options['robokassa']['login'], $options['robokassa']['pass1']); //zxcFGHqwe456
//                $client->setSumm($total_prize);
//                $client->setId($order->id);
//                $client->setLog(new usEngine_Robokassa_Log_Mysql('Index_Model_Log'));
//                if (($url = $client->getPayUrl()) === false) {
//                    usEngine_Message::getInstance()->setMessage('error','Неудалось осуществить платеж');
//                    return $this->_redirect('/');
//                }
//                Catalog_Model_BasketTable::getInstance()->remove();
//                return $this->_redirect($url);
//            }
//            usEngine_Message::getInstance()->setMessage('error', 'При приеме заказа произошла ошибка!');
//            $form->populate($formdata);
//        }
//        $this->view->form = $form;
//    }
//
//    public function roboSuccessAction()
//    {
//        //суда прихходит пользователь после расплаты
//        $id = $this->getRequest()->getParam('InvId');
//        $price = $this->getRequest()->getParam('OutSum');
//
//        $options = Zend_Registry::get('options');
//        if(strtoupper(md5($price.':'.$id.':'.$options['robokassa']['pass1']))!=strtoupper($this->getRequest()->getParam('SignatureValue'))){
//            exit('r Not SignatureValue');
//        }
//        $order = Catalog_Model_OrderTable::getInstance()->findOneById($id);
//        $order->status = 1;
//        $order->save();
//        self::sendMail($order->id);
//
//        usEngine_Message::getInstance()->setMessage('success','Заказ принят.');
//        return $this->_redirect('/');
//    }
//    public function roboResultAction()
//    {
//        $this->_helper->layout()->disableLayout();
//        $this->_helper->viewRenderer->setNoRender(true);
////var_dump($this->getRequest()->getParams()); die(' dump params');
//        $id = $this->getRequest()->getParam('InvId');
//        $price = $this->getRequest()->getParam('OutSum');
//
//        $options = Zend_Registry::get('options');
////var_dump($options['robokassa']['pass2']); die(' dump pass2');
//        if(strtoupper(md5($price.':'.$id.':'.$options['robokassa']['pass2']))!=strtoupper($this->getRequest()->getParam('SignatureValue'))){
//            exit('r Not SignatureValue');
//        }
//        $order = Catalog_Model_OrderTable::getInstance()->findOneById($id);
//        if($order) {
//            $order->status = 1;
//            $order->save();
//	    echo 'OK'.$order->id;
//        }    
//    }
//    public function roboFailAction()
//    {
//        $id = $this->getRequest()->getParam('InvId');
//
//        $order = Catalog_Model_OrderTable::getInstance()->findOneById($id);
//        $order->status = 3;
//        $order->save();
//        //отмена оплаты - менять ордеру статус
//        usEngine_Message::getInstance()->setMessage('error','Заказ не принят. Отменено пользователем.');
//        return $this->_redirect('/');
//    }
//    
//    public function step3Action()
//    {
//        /**
//         * @todo
//         * 
//         * Туточки надо нам работать с робокассой.
//         * См. usEngine_Robokassa_*
//         * 
//         * К сожалению, я утратил способность вспомнить как работать с этими классами, но...
//         * Я верю, у тебя, мой милый программист, все получится и ты быстро разберешься.
//         * 
//         * Если что знай, великий и непоколебимый Гугл тебе в помощь!!!
//         * 
//         * P.S.: К сожалению, я не успел дождаться 1 апреля, но у меня все равно
//         * не пропадает чувство уверенности, что ты, мой милый друг, прочитаешь эти
//         * строки именно в этот замечательный и веселый день.
//         * 
//         * P.P.S.: Всегда ваш покорный слуга, ТТТ.
//         * 
//         */
//    }
//
//    public function sendMail($order_id)
//    {
//        $order = Catalog_Model_OrderTable::getInstance()->getOrderForEdit($order_id);
//        $client_view = new Zend_View();
//        $client_view->setScriptPath(APPLICATION_PATH . '/templates/extaz/basket/');
//        $view = new Zend_View();
//        $view->setScriptPath(APPLICATION_PATH . '/templates/extaz/basket/');
////                            var_dump($options['setting']['index']['site_name']);die("dump \$options['setting']['index']['site_name']");
//        $options = Zend_Registry::get('options');
//        $data['site_name'] = $options['setting']['index']['site_name'];
//        $data['link'] = $_SERVER['SERVER_NAME'];
//        $data['url'] = $_SERVER['HTTP_REFERER'];
//        $client_view->data = $view->data = $data;
//        $client_view->order = $view->order = $order;
////                            $view->order_items = $order_items;
//        $client_view->summ = $view->summ = $order['summ'];
//
//        $mail_to_user = new Zend_Mail('utf-8');
//
//        $body = $client_view->render('client-mail.phtml');
//        $mail_to_user->setBodyHtml($body);
////                        var_dump(explode(',', $options['setting']['index']['contacts']['email']));die("dump ");
//        $mail_to_user->setFrom($options['setting']['index']['contacts']['email'], 'Amur.net');
////                            var_dump($data);die("dump \$data");
//        $mail_to_user->addTo($order['email']);
//        $mail_to_user->setSubject('Ваш заказ');
////        if(APPLICATION_ENV == 'production')
////            $mail_to_user->send();
//
//        $mail = new Zend_Mail('utf-8');
////                            echo $view->render('order/client-mail.phtml');die;
//        $body = $view->render('seller-mail.phtml');
////        echo $body;die;
//        $mail->setBodyHtml($body);
//        $mail->setFrom($order['email'], $order['name']);
//        $mail->addTo(explode(',', $options['setting']['index']['contacts']['email']));
//        $mail->setSubject('Поступил заказ');
//        if(APPLICATION_ENV == 'production')
//            $mail->send();
//
//    }

}
