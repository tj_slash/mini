<?php
class Catalog_AdminOrderController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Catalog_Model_OrderTable::getInstance();
    }
    
    public function listAction()
    {
        $grid = new usTable_Table($this->_model->adminListing());
        $grid->setCaption('Заказы');
        $grid->removeToolbars(array('list-archive', 'delete', 'list', 'archive', 'publish', 'add'));
        $grid->removeActions(array('publish', 'archive', 'edit'));
        $grid->setAction('Просмотр', array('action' => 'view', 'class' => 'green view'));
        $grid->setColumns(array(
            'id'=>'ID',
            'status'=>'Статус', 
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Почта',
            'created'=>'Дата создания'
        ));
        $grid->setFormat('status', array($this, 'formatStatus'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public function viewAction()
    {
        $id = $this->getRequest()->getParam('id', null);
            
        if (!$id) {
            usEngine_Message::getInstance()->setMessage('error', 'Неверно указан идентификатор');
            if (!empty($this->getRequest()->getServer('HTTP_REFERER'))) 
                $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));
            else 
                $this->_redirect($this->view->baseUrl() . '/admin');
        }

        $order = $this->_model->findOneById($id);
        if (!$order) {
            usEngine_Message::getInstance()->setMessage('error', 'Заказ не найден');
            if (!empty($this->getRequest()->getServer('HTTP_REFERER'))) 
                $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));
            else 
                $this->_redirect($this->view->baseUrl() . '/admin');
        }
        
        $form = new Catalog_Form_Order_Status();
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if ($form->isValid($formdata)) {
                $data = $form->getValues();
                $order->status = $data['status'];
                $order->save();
                $this->_redirector($item);
            }
        }
        
        $form->populate($order->getData());
        $this->view->form = $form;
        $this->view->order = $order;
        
        $query = Doctrine_Query::create()->from('Catalog_Model_Purchase')->addWhere('order_id = ?', $order->id);
        $grid = new usTable_Table($query);
        $grid->setCaption('Товары')->setCheckbox(false)->setToolbars(false)->setActions(false);
        $grid->setColumns(array('image'=>'Изображение', 'title'=>'Товар', 'amount' => 'Количество', 'price' => 'Цена'));
        $grid->setFormat(array(
            array('title', array($this, 'formatTitle')),
            array('price', array($this, 'formatPrice')),
            array('image', array($this, 'formatImage'))
        ));
        $this->view->grid = $grid;
        
    }
    
    public static function formatStatus($row, $key)
    {
        switch($row->$key) {
            case 0 :
                return '<div style="width:100px" class="uibutton confirm">В обработке</div>';
                break;
            case 1:
                return '<div style="width:100px" class="uibutton normal">Принят</div>';
                break;
            case 2:
                return '<div style="width:100px" class="uibutton">Выполнен</div>';
                break;
            default :
                return '<div style="width:100px" class="uibutton special">Отменен</div>';
        }
        return $row->$key;
    }
    
    public static function formatImage($row)
    {
        return "<img src='/media/products/list_{$row->Product->image}' width='40' height='40'>";
    }
    
    public static function formatTitle($row)
    {
        return "<a href='/catalog/product/{$row->Product->alias}' target='_blank'>{$row->Product->title}</a>";
    }
    
    public static function formatPrice($row)
    {
        return $row->Product->currentPrice . ' руб.';
    }
}
