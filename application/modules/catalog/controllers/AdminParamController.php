<?php
class Catalog_AdminParamController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Catalog_Model_ParamTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Параметры товаров');
            $grid->removeToolbars(array('list-archive', 'delete'));
        } else {
            $grid->setCaption('Архив параметров товаров');
            $grid->removeToolbars(array('list', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','title'=>'Наименование','category_id'=>'Категория','type'=>'Тип','publish'=>'Публикация'));
        $grid->setFormat('type', array($this, 'formatType'));
        $grid->setFormat('category_id', array($this, 'formatCategory'));
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new Catalog_Form_Param_Add());
    }
    
    public function ajaxGetParamsAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $category_id = $this->getRequest()->getParam('category_id', null);
        $params = Catalog_Model_ParamTable::getInstance()->getParams($category_id);
        if (!empty($params) && count($params) > 0) {
            $form = new Catalog_Form_Product_Params(array('params' => array('params' => $params)));
            $this->view->form = $form;
            $this->view->params = $params;
        }
        echo $this->view->render('admin-param/subform.phtml');
    }
    
    public static function formatType($row)
    {
        switch ($row->type) {
            case 'select': $type = 'Список'; break;
            case 'radio': $type = 'Радио кнопка'; break;
            case 'checkbox': $type = 'Галочка'; break;
            case 'multiselect': $type = 'Мультисписок'; break;
            case 'text': default: $type = 'Текст'; break;
        }
        return $type;
    }
    
    public static function formatCategory($row)
    {
        return "<a href='/catalog/category/{$row->Category->alias}' target='_blank'>{$row->Category->title}</a>";
    }
}