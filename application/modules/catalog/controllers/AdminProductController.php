<?php
class Catalog_AdminProductController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Catalog_Model_ProductTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Каталог товаров');
            $grid->removeToolbars(array('list-archive', 'delete'));
        } else {
            $grid->setCaption('Архив товаров');
            $grid->removeToolbars(array('list', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','image'=>'Изображение','title'=>'Наименование','created'=>'Дата создания','publish'=>'Публикация'));
        $grid->setFormat('title', array($this, 'formatTitle'));
        $grid->setFormat('image', array($this, 'formatImage'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new Catalog_Form_Product_Add());
    }
    
    public static function formatTitle($row)
    {
        return "<a href='/catalog/product/{$row->alias}' target='_blank'>{$row->title}</a>";
    }
    
    public static function formatImage($row)
    {
        return "<img src='/media/products/list_{$row->image}' width='40' height='40'>";
    }
}