<?php
class Catalog_ProductController extends usEngine_Controller_Action
{
    public function init()
    {
        
    }
    
    public function viewAction()
    {
        $alias = $this->getRequest()->getParam('product_alias', null);
        
        if (empty($alias)) throw new Zend_Controller_Action_Exception('Не передан идентификатор');
        
        $product = Catalog_Model_ProductTable::getInstance()->findOneByAlias($alias);
        
        if (empty($product)) throw new Zend_Controller_Action_Exception('Не найден товар');

        if ($product->archive === true) throw new Zend_Controller_Action_Exception('Товар удален');

        if ($product->publish === false) throw new Zend_Controller_Action_Exception('Товар не опубликован');
        
        $product->updateViews();
//        $product->useMeta();
//        $product->useBreadcrumbs();
        
        $this->view->product = $product;
    }
    
    public function widgetSeacrhAction()
    {
        $form = new Catalog_Form_Product_Search();
        $form->populate($this->getRequest()->getParams());
        $this->view->form = $form;
    }
    
    public function widgetRelatedAction()
    {
        $product = $this->getRequest()->getParam('product', null);
        $products = Catalog_Model_ProductTable::getInstance()->getRelated($product, 3);
        $this->view->products = $products;
    }
    
    public function widgetCommentedAction()
    {
        $product = $this->getRequest()->getParam('product', null);
        $products = Catalog_Model_ProductTable::getInstance()->getCommented($product, 3);
        $this->view->products = $products;
    }
    
    public function widgetLastAction()
    {
        $product = $this->getRequest()->getParam('product', null);
        $products = Catalog_Model_ProductTable::getInstance()->getLast($product, 3);
        $this->view->products = $products;
    }
    
    public function widgetPopularAction()
    {
        $product = $this->getRequest()->getParam('product', null);
        $products = Catalog_Model_ProductTable::getInstance()->getPopular($product, 3);
        $this->view->products = $products;
    }
    
    public function widgetViewedAction()
    {
        $product = $this->getRequest()->getParam('product', null);
        $products = Catalog_Model_ProductTable::getInstance()->getViewed($product, 3);
        $this->view->products = $products;
    }
    
    public function widgetDiscountedAction()
    {
        $product = $this->getRequest()->getParam('product', null);
        $products = Catalog_Model_ProductTable::getInstance()->getDicounted($product, 3);
        $this->view->products = $products;
    }
    
    public function widgetDiscountedSidebarAction()
    {
        $product = $this->getRequest()->getParam('product', null);
        $products = Catalog_Model_ProductTable::getInstance()->getDicounted($product, 3);
        $this->view->products = $products;
    }
}