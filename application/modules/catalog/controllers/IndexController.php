<?php
class Catalog_IndexController extends Zend_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    public function exchangeAction()
    {
        $exchange = new usEngine_1c_Exchange(APPLICATION_PATH . '/../1c/');
        $exchange->setHandlerCategory(array('Catalog_Model_CategoryTable', 'import'));
        $exchange->setHandlerProduct(array('Catalog_Model_ProductTable', 'import'));
        $exchange->setHandlerOrder(array('Catalog_Model_OrderTable', 'importOrders'));
        $exchange->setHandlerQuery(array('Catalog_Model_OrderTable', 'exportOrders'));
        $exchange->exchange();
        if($this->getRequest()->getParam('mode') == 'query') {
            $this->getResponse()->clearHeaders();
            $this->getResponse()->setHeader('Content-Type', 'text/xml');
        }
    }
}