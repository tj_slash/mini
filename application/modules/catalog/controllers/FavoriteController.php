<?php
class Catalog_FavoriteController extends usEngine_Controller_Action
{
    public function listAction()
    {
        $page = $this->getRequest()->getParam('page', 1);
        $limit = $this->getRequest()->getParam('limit', 16);
        $sort = $this->getRequest()->getParam('sort', 'id:asc');
        
        $products = Catalog_Model_FavoriteTable::getInstance()->getList($sort, $page, $limit);
        $this->view->products = $products;
        
        $this->view->sort = $sort;
        $this->view->sorts = Catalog_Model_ProductTable::getInstance()->getSorts();
        
        $this->view->limit = $limit;
        $this->view->limits = Catalog_Model_ProductTable::getInstance()->getLimits();
        
        $view = $this->getRequest()->getParam('view', false);
        if ($view) {
            setcookie('view', $view, 7*24*60*60 + time(), '/');
        } else {
            if (empty($_COOKIE['view']) || $_COOKIE['view'] === 'grid') $view = 'grid';
            else $view = 'list';
        }
        $this->view->view = $view;
        
        $this->view->headTitle()->prepend("Избранное");
        $this->view->headMeta()->setName('keywords', "Избранное");
        $this->view->headMeta()->setName('description', "Избранное");
    }
    
    public function addAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $product_id = $this->getRequest()->getParam('product_id', null);
        if ($product_id) Catalog_Model_FavoriteTable::getInstance()->add($product_id);
        if ($this->getRequest()->isXmlHttpRequest()) {
            echo Zend_Json::encode(array('status' => 200));
        } else {
            $this->_redirect($_SERVER['HTTP_REFERER']);
        }
    }
    
    public function deleteAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $product_id = $this->getRequest()->getParam('product_id', null);
        if ($product_id) Catalog_Model_FavoriteTable::getInstance()->delete($product_id);
        if ($this->getRequest()->isXmlHttpRequest()) {
            echo Zend_Json::encode(array('status' => 200));
        } else {
            $this->_redirect($_SERVER['HTTP_REFERER']);
        }
    }
}