<?php
class Catalog_AdminCategoryController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Catalog_Model_CategoryTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Каталог категорий');
            $grid->removeToolbars(array('list', 'delete'));
        } else {
            $grid->setCaption('Архив категорий');
            $grid->removeToolbars(array('list-archive', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','title'=>'Наименование','alias'=>'Алиас','created'=>'Дата создания','publish'=>'Публикация'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $grid->setFormat('alias', array($this, 'formatAlias'));
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new Catalog_Form_Category_Add());
    }
    
    public static function formatAlias($row, $key)
    {
        return "<a href='/catalog/category/{$row->$key}' target='_blank'>{$row->$key}</a>";
    }
}