<?php
class Catalog_AdminImageController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender();
        $this->_model = Catalog_Model_ProductTable::getInstance();
    }
    
    public function uploadAction()
    {
        if (!empty($_FILES)) {
            $tempFile = $_FILES['Filedata']['tmp_name'];
            $ext = explode(".", $_FILES['Filedata']['name']);
            $newName = uniqid() . '.' . $ext[count($ext)-1];
            $dest = APPLICATION_PATH . '/../public/media/products/';
            
            $targetFile = APPLICATION_PATH . '/../public/media/products/' . $newName;
            move_uploaded_file($tempFile, $targetFile);
            
            $image = new Catalog_Model_Image();
            $image->image = $newName;
            $image->save();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(240)
                ->setHeight(300)
                ->setPath($dest)
                ->setPrefix('list_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            sleep(1);
            echo Zend_Json_Encoder::encode(
                array(
                    'src' => '/media/products/' . $newName,
                    'id' => $image->id,
                )
            );
        }
    }
}