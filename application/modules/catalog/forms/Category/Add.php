<?php
class Catalog_Form_Category_Add extends usEngine_Form_Form
{
    private static function createCategoryId()
    {
        $item = new Zend_Form_Element_Select('category_id');
        $item->setLabel('Категория');
        $item->setDescription('Выберите категорию');
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->addMultiOption(NULL, 'Выберите категорию');
        $categories = Doctrine_Core::getTable('Catalog_Model_Category')->getTree()->fetchTree();
        if(count($categories) > 0) {
            foreach($categories as $category) {
                $item->addMultiOption($category->id, str_repeat('——', $category->level) . ' ' . $category->title);
            }
        }
        return $item;
    }
    
    private static function createTitleInput()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок категории');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAliasInput()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Alias');
        $item->setDescription('Алиас категории');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $regex = new Zend_Validate_Regex('/^[\-a-zA-Z0-9]+$/u');
        $item->addValidator($regex);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(false);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createCIdInput()
    {
        $item = new Zend_Form_Element_Text('c_id');
        $item->setLabel('ID 1C');
        $item->setDescription('ID категории из 1С');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private static function createImageInput()
    {
        $item = self::createFileUploadInput('image');
        $item->setLabel('Изображение');
        $item->setDescription('Bзображение категории (JPG, PNG, GIF)');
        $item->addValidator('Size', false, 1024*1024);
        $item->addValidator('Extension', false, 'jpeg,jpg,png,gif');
        $item->setAttrib('class', 'fileupload');
        $item->setDestination(APPLICATION_PATH . '/../public/tmp');
        $item->setDecorators(self::$_fileElementDecorator);
        return $item;
    }

    protected static function createOnMainChekbox()
    {
        $item = new Zend_Form_Element_Checkbox('on_main');
        $item->setLabel('Выводить на главной');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createCategoryId());
        $this->addElement(self::createTitleInput());
        $this->addElement(self::createAliasInput());
        $this->addElement(self::createCIdInput());
        $this->addElement(self::createImageInput());
        $this->addElement(self::createOnMainChekbox());
        parent::initMeta();
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['image'])) {
            $image = $formdata['image'];
            $imageElement = $this->getElement('image');
            $imageElement->setDescription("<img src='/media/category/{$image}' style='max-width:400px'/>");
            $imageElement->setRequired(false);
        }
        
        if (!empty($formdata['id'])) {
            $category = Catalog_Model_CategoryTable::getInstance()->findOneById($formdata['id']);
            $this->getElement('category_id')->removeMultiOption($category->id);
            
            $parent = $category->getNode()->getParent();
            $formdata['category_id'] = $parent->id;    
            
            $this->getElement('c_id')->setAttrib('disabled', 'disabled');
        }   
        return $formdata;
    }
    
    public function preSave($formdata)
    {
        $data = array();
        $file = $this->image->getFileInfo();
        if (!empty($file['image']['name']) > 0) {
            $ext = explode(".", $file['image']['name']);
            $newName = uniqid() . '.' . $ext[count($ext)-1];
            
            $dest = APPLICATION_PATH . '/../public/media/category/';
            
            $this->image->addFilter('Rename', $dest . $newName);
            $this->image->receive();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(250)
                ->setHeight(250)
                ->setPath($dest)
                ->setPrefix('list_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(80)
                ->setHeight(80)
                ->setPath($dest)
                ->setPrefix('thumb_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            $data['image'] = $newName;
        }
        
        if (isset($formdata['category_id'])) $data['parent_id'] = $formdata['category_id'];        
        return $data;
    }
}
