<?php
class Catalog_Form_Param_Add extends usEngine_Form_Form
{
    private static function createCategoryId()
    {
        $item = new Zend_Form_Element_Select('category_id');
        $item->setLabel('Категория');
        $item->setDescription('Выберите категорию');
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->addMultiOption(NULL, 'Выберите категорию');
        $categories = Doctrine_Core::getTable('Catalog_Model_Category')->getTree()->fetchTree();
        if(count($categories) > 0) {
            foreach($categories as $category) {
                $item->addMultiOption($category->id, str_repeat('——', $category->level) . ' ' . $category->title);
            }
        }
        return $item;
    }
    
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок параметра');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createType()
    {
        $item = new Zend_Form_Element_Select('type');
        $item->setLabel('Тип');
        $item->setDescription('Тип параметра');
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->addMultiOption(NULL, 'Выберите тип параметра');
        $item->addMultiOption('select', 'Список');
        $item->addMultiOption('multiselect', 'Мультисписок');
        $item->addMultiOption('radio', 'Радио кнопка');
        $item->addMultiOption('checkbox', 'Галочка');
        $item->addMultiOption('text', 'Текст');
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createValues()
    {
        $item = new Zend_Form_Element_Text('value');
        $item->setLabel('Значения');
        $item->setDescription('Значения параметра');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setAttrib('style', 'width:90%');
        $item->isArray();
        $item->setIsArray(true);
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createCategoryId());
        $this->addElement(self::createTitle());
        $this->addElement(self::createType());
        $this->addElement(self::createValues());
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['id'])) {
            if (!empty($formdata['values'])) {
                $values = Zend_Json::decode($formdata['values']);
                if (is_array($values) && count($values) > 0) {
                    $valueValue = array();
                    foreach($values as $value) {
                        $valueValue[] = $value;
                    }
                    $view = $this->getView();
                    $view->values = $valueValue;  
                }
            }
        }   
        return $formdata;
    }
    
    public function postSave($param, $form)
    {
        $formdata = $form->getValues();
        if (!empty($formdata['value']) && is_array($formdata['value']) && count($formdata['value']) > 0) {
            $values = array();
            foreach ($formdata['value'] as $value){
                if (!empty($value)) $values[] = $value;
            }
            if (count($values) > 0) {
                $param->values = Zend_Json::encode($values);
                $param->save();
            }
        }
    }
}