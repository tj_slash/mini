<?php
class Catalog_Form_Product_Params extends usEngine_Form_Form
{
    protected static $_paramsDecorator = array(
        'ViewHelper',
        array('Description', array('tag' => 'span', 'class' => 'f_help', 'escape' => false)),
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('HtmlTag', array('tag' => 'div')),
        array('Label', array('separator' => ' ', 'escape' => false)),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'section section-params'))
    );
    
    private static function createSelect(Catalog_Model_Param $param)
    {
        $item = new Zend_Form_Element_Select($param->alias);
        $values = Zend_Json::decode($param->values);
        if(!empty($values) && is_array($values) && count($values) > 0) {
            foreach($values as $value) $item->addMultiOption($value, $value);
        }
        $item->setLabel($param->title);
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_paramsDecorator);
        return $item;
    }
    
    private function createRadio(Catalog_Model_Param $param)
    {
        $item = new Zend_Form_Element_Radio($param->alias);
        $values = Zend_Json::decode($param->values);
        if(!empty($values) && is_array($values) && count($values) > 0) {
            foreach($values as $value) $item->addMultiOption($value, $value);
        }
        $item->setLabel($param->title);
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setDecorators(self::$_paramsDecorator);
        return $item;
    }
    
    private static function createCheckbox(Catalog_Model_Param $param)
    {
        $values = Zend_Json::decode($param->values);
        if(!empty($values) && is_array($values) && count($values) > 0) {
            $item = new Zend_Form_Element_MultiCheckbox($param->alias);
            foreach($values as $value) $item->addMultiOption($value, $value);
        } else {
            $item = new Zend_Form_Element_Checkbox($param->alias);
        }
        $item->setLabel($param->title);
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setDecorators(self::$_paramsDecorator);
        return $item;
    }
    
    private static function createMultiselect(Catalog_Model_Param $param)
    {
        $item = new Zend_Form_Element_Multiselect($param->alias);
        $values = Zend_Json::decode($param->values);
        if(!empty($values) && is_array($values) && count($values) > 0) {
            foreach($values as $value) $item->addMultiOption($value, $value);
        }
        $item->setLabel($param->title);
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_paramsDecorator);
        return $item;
    }
    
    private static function createText(Catalog_Model_Param $param)
    {
        $item = new Zend_Form_Element_Text($param->alias);
        $item->setLabel($param->title);
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_paramsDecorator);
        return $item;
    }

    public function init()
    {
        $params = $this->getParam('params');
        foreach ($params as $param) {
            switch ($param->type) {
                case 'select': $this->addElement(self::createSelect($param)); break;
                case 'radio': $this->addElement($this->createRadio($param)); break;
                case 'checkbox': $this->addElement(self::createCheckbox($param)); break;
                case 'multiselect': $this->addElement(self::createMultiselect($param)); break;
                case 'text': $this->addElement(self::createText($param)); break;
            }
        }
    }
}