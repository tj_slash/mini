<?php
class Catalog_Form_Product_Add extends usEngine_Form_Form
{
    private static function createCategoryId()
    {
        $item = new Zend_Form_Element_Select('category_id');
        $item->setLabel('Категория');
        $item->setDescription('Выберите категорию');
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->addMultiOption(NULL, 'Выберите категорию');
        $categories = Doctrine_Core::getTable('Catalog_Model_Category')->getTree()->fetchTree();
        if(count($categories) > 0) {
            foreach($categories as $category) {
                $item->addMultiOption($category->id, str_repeat('——', $category->level) . ' ' . $category->title);
            }
        }
        return $item;
    }
    
    private static function createBrandId()
    {
        $item = new Zend_Form_Element_Select('brand_id');
        $item->setLabel('Производитель');
        $item->setDescription('Выберите производителя');
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        $item->addMultiOption(NULL, 'Выберите производителя');
        $brands = Doctrine_Core::getTable('Catalog_Model_Brand')->findAll();
        if(count($brands) > 0) {
            foreach($brands as $brand) {
                $item->addMultiOption($brand->id, $brand->title);
            }
        }
        return $item;
    }
    
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок товара');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAlias()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Alias');
        $item->setDescription('Алиас товара');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $regex = new Zend_Validate_Regex('/^[\-a-zA-Z0-9]+$/u');
        $item->addValidator($regex);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createSku()
    {
        $item = new Zend_Form_Element_Text('sku');
        $item->setLabel('Артикул');
        $item->setDescription('Артикул товара');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createCId()
    {
        $item = new Zend_Form_Element_Text('c_id');
        $item->setLabel('ID 1C');
        $item->setDescription('ID товара из 1С');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createPrice()
    {
        $item = new Zend_Form_Element_Text('price');
        $item->setLabel('Цена');
        $item->setDescription('Цена товара');
        $item->addValidator(new Validate_Float());
        $item->addValidator(new Zend_Validate_StringLength(0, 11, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createDiscount()
    {
        $item = new Zend_Form_Element_Text('discount');
        $item->setLabel('Скидка (руб.)');
        $item->setDescription('Значение скидки в рублях');
        $item->addValidator(new Validate_Float());
        $item->addValidator(new Zend_Validate_StringLength(0, 11, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createDiscountPercent()
    {
        $item = new Zend_Form_Element_Text('discount_percent');
        $item->setLabel('Скидка (%)');
        $item->setDescription('Значение скидки в процентах');
        $item->addValidator(new Zend_Validate_Digits());
        $item->addValidator(new Zend_Validate_StringLength(0, 2, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createDescription()
    {
        $item = new Zend_Form_Element_Textarea('description');
        $item->setLabel('Описание');
        $item->setDescription('Краткое описание товара');
        $item->addValidator(new Zend_Validate_StringLength(0, 4096, 'UTF-8'));
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createImages()
    {
        $item = new usEngine_Form_Element_Uploader('image');
        $item->setLabel('Изображения');
        $item->setDescription('Изображения товара');
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createCategoryId());        
        $this->addElement(self::createBrandId());        
        $this->addElement(self::createTitle());
        $this->addElement(self::createAlias());
        $this->addElement(self::createSku());
        $this->addElement(self::createCId());
        $this->addElement(self::createPrice());
        $this->addElement(self::createDiscount());
        $this->addElement(self::createDiscountPercent());
        $this->addElement(self::createDescription());
        $this->addElement(self::createImages());
        parent::initMeta();
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['id'])) {
            $images = Doctrine_Query::create()
                ->from('Catalog_Model_Image')
                ->where('product_id = ?', $formdata['id'])
                ->orderBy('order ASC')
                ->execute();
            if (count($images) > 0) {
                $image = $this->getElement('image');
                $image->setOptions(array('images' => $images));
            }
        }   
        return $formdata;
    }
    
    public function postSave($product)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $images = $request->getParam('image', false);
        if (!empty($images) && is_array($images) && count($images) > 0) {
            foreach($images as $order => $id) {
                Doctrine_Query::create()
                    ->update('Catalog_Model_Image')
                    ->addWhere('id = ?', $id)
                    ->set('order', $order)
                    ->set('product_id', $product->id)
                    ->execute();
            }
        }
    }
}

class Validate_Float extends Zend_Validate_Abstract
{
    /**
     * notSuccess
     */
    const NOT_SUCCESS = 'notSuccess';
    
    /**
     * Template error messages
     * @var array 
     */
    protected $_messageTemplates = array(
        self::NOT_SUCCESS => "'%value%' не является валидной ценой",
    );
    
    /**
     * Validation
     * 
     * @param string $value
     * @return boolean 
     */
    public function isValid($value)
    {        
        if (!is_float((float)$value)) {
            $this->_error(self::NOT_SUCCESS);
            return false;
        }
        return true;
    }
}