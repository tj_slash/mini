<?php
class Catalog_Form_Brand_Add extends usEngine_Form_Form
{    
    private static function createTitleInput()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Наименование');
        $item->setDescription('Наименование производителя');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAliasInput()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Alias');
        $item->setDescription('Алиас категории');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $regex = new Zend_Validate_Regex('/^[\-a-zA-Z0-9]+$/u');
        $item->addValidator($regex);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(false);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createCIdInput()
    {
        $item = new Zend_Form_Element_Text('c_id');
        $item->setLabel('ID 1C');
        $item->setDescription('ID производителя из 1С');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    private static function createImageInput()
    {
        $item = self::createFileUploadInput('image');
        $item->setLabel('Изображение');
        $item->setDescription('Изображение производителя (JPG, PNG, GIF)');
        $item->addValidator('Size', false, 1024*1024);
        $item->addValidator('Extension', false, 'jpeg,jpg,png,gif');
        $item->setAttrib('class', 'fileupload');
        $item->setDestination(APPLICATION_PATH . '/../public/tmp');
        $item->setDecorators(self::$_fileElementDecorator);
        return $item;
    }
    
    private static function createDescription()
    {
        $item = new Zend_Form_Element_Textarea('description');
        $item->setLabel('Описание');
        $item->setDescription('Краткое описание производителя');
        $item->addValidator(new Zend_Validate_StringLength(0, 4096, 'UTF-8'));
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createTitleInput());
        $this->addElement(self::createAliasInput());
        $this->addElement(self::createDescription());
        $this->addElement(self::createCIdInput());
        $this->addElement(self::createImageInput());
        parent::initMeta();
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['image'])) {
            $image = $formdata['image'];
            $imageElement = $this->getElement('image');
            $imageElement->setDescription("<img src='/media/brand/{$image}' style='max-width:400px'/>");
        }
        if (!empty($formdata['id'])) {
            $this->getElement('c_id')->setAttrib('disabled', 'disabled');
        }   
        return $formdata;
    }
    
    public function preSave($formdata)
    {
        $data = array();
        $file = $this->image->getFileInfo();
        if (!empty($file['image']['name']) > 0) {
            $ext = explode(".", $file['image']['name']);
            $newName = uniqid() . '.' . $ext[count($ext)-1];
            
            $dest = APPLICATION_PATH . '/../public/media/brand/';
            
            $this->image->addFilter('Rename', $dest . $newName);
            $this->image->receive();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(250)
                ->setHeight(250)
                ->setPath($dest)
                ->setPrefix('list_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(80)
                ->setHeight(80)
                ->setPath($dest)
                ->setPrefix('thumb_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            $data['image'] = $newName;
        }
        
        return $data;
    }
}
