<?php
class Catalog_Form_Kupon_Add extends usEngine_Form_Form
{    
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок акции');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createDiscount()
    {
        $item = new Zend_Form_Element_Text('discount');
        $item->setLabel('Скидка');
        $item->setDescription('Значение скидки в процентах');
        $item->addValidator(new Zend_Validate_Digits());
        $item->addValidator(new Zend_Validate_Between(array('min' => 1, 'max' => 100)));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createStart()
    {
        $item = new Zend_Form_Element_Text('start_date');
        $item->setLabel('Начало акции');
        $item->setDescription('Дата начала акции');
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full datetimepicker');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createEnd()
    {
        $item = new Zend_Form_Element_Text('end_date');
        $item->setLabel('Конец акции');
        $item->setDescription('Дата конца акции');
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full datetimepicker');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createValues()
    {
        $item = new Zend_Form_Element_Text('value');
        $item->setLabel('Коды купонов');
        $item->setDescription('Список кодов купонов');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setAttrib('style', 'width:90%');
        $item->isArray();
        $item->setIsArray(true);
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createTitle());
        $this->addElement(self::createDiscount());
        $this->addElement(self::createStart());
        $this->addElement(self::createEnd());
        $this->addElement(self::createValues());
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['id'])) {
            $codes = Catalog_Model_KuponCodeTable::getInstance()->findByDealId($formdata['id']);
            if (!empty($codes) && count($codes) > 0) {
                $values = array();
                foreach($codes as $code) {
                    $values[] = $code->code;
                }
                $this->getView()->values = $values;  
            }
        }   
        return $formdata;
    }
    
    public function postSave($item, $form)
    {
        $formdata = $form->getValues();
        if (!empty($formdata['value']) && is_array($formdata['value']) && count($formdata['value']) > 0) {
            Doctrine_Query::create()->delete('Catalog_Model_KuponCode')->where('deal_id = ?', $item->id)->execute();
            foreach ($formdata['value'] as $value){
                if (!empty($value)) {
                    $code = new Catalog_Model_KuponCode();
                    $code->code = $value;
                    $code->deal_id = $item->id;
                    $code->save();
                }
            }
        }
    }
}