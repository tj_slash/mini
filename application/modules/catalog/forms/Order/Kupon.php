<?php
class Catalog_Form_Order_Kupon extends usEngine_Form_Form
{
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('Label', array('separator' => ' ', 'escape' => false, 'class' => 'sr-only')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-xs-12 col-sm-4'))
    );
    
    protected static $_buttonDecorator = array(
        'ViewHelper',
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'col-xs-12 col-sm-4'))
    );
    
    private static function createKuponCode()
    {
        $item = new Zend_Form_Element_Text('kupon_code');
        $item->setLabel('Введите код купона, если он у вас имеется');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('placeholder', 'Код купона');
        $item->setAttrib('class', 'form-control');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createSubmit()
    {
        $item = new Zend_Form_Element_Submit('kupon_submit');
        $item->setLabel('Применить код купона');
        $item->setAttrib('class', 'btn btn-primary btn-small');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createKuponCode());
        $this->addElement(self::createSubmit());
        $this->setDecorators(self::$_formDecorator);
    }
}