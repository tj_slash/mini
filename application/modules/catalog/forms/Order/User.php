<?php
class Catalog_Form_Order_User extends usEngine_Form_Form
{
    private static function createName()
    {
        $item = new Zend_Form_Element_Text('name');
        $item->setLabel('Ваше имя');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('style', 'width:550px');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createPhone()
    {
        $item = new Zend_Form_Element_Text('phone');
        $item->setLabel('Номер телефона');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('style', 'width:550px');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setLabel('Электронная почта');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('style', 'width:550px');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAddress()
    {
        $item = new Zend_Form_Element_Text('address');
        $item->setLabel('Адрес доставки');
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('style', 'width:550px');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createComment()
    {
        $item = new Zend_Form_Element_Textarea('comment');
        $item->setLabel('Комментарий к заказу');
        $item->addValidator(new Zend_Validate_StringLength(0, 4096, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('style', 'width:550px;height:216px');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createSubmit()
    {
        $item = new Zend_Form_Element_Submit('submit');
        $item->setLabel('Перейти к оплате');
        $item->setAttrib('style', 'width:200px');
        $item->setAttrib('class', 'buttonNext');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createName());
        $this->addElement(self::createPhone());
        $this->addElement(self::createEmail());
        $this->addElement(self::createAddress());
        $this->addElement(self::createComment());
        $this->addElement(self::createSubmit());
        $this->setDecorators(self::$_formDecorator);
    }
}