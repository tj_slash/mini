<?php
class Catalog_Form_Order_Contact extends usEngine_Form_Form
{
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('Label', array('separator' => ' ', 'escape' => false, 'class' => 'sr-only')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    
    protected static $_buttonDecorator = array(
        'ViewHelper'
    );
    
    private static function createName()
    {
        $item = new Zend_Form_Element_Text('name');
        $item->setLabel('Имя');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Ваше имя');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createPhone()
    {
        $item = new Zend_Form_Element_Text('phone');
        $item->setLabel('Номер телефона');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Ваш номер телефона');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setLabel('Электронная почта');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Адрес электронной почты');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAddress()
    {
        $item = new Zend_Form_Element_Text('address');
        $item->setLabel('Адрес доставки');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Адрес доставки');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createRegion()
    {
        $item = new Zend_Form_Element_Text('region');
        $item->setLabel('Регион');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Регион');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createCity()
    {
        $item = new Zend_Form_Element_Text('city');
        $item->setLabel('Город');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Город');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createZip()
    {
        $item = new Zend_Form_Element_Text('zip');
        $item->setLabel('Индекс');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Индекс');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createComment()
    {
        $item = new Zend_Form_Element_Textarea('comment');
        $item->setLabel('Комментарий');
        $item->addValidator(new Zend_Validate_StringLength(0, 4096, 'UTF-8'));
        $item->setAttrib('style', 'width:550px;height:216px');
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'form-control max-width');
        $item->setAttrib('placeholder', 'Комментарий к заказу');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createSubmit()
    {
        $item = new Zend_Form_Element_Submit('contact');
        $item->setLabel('Сохранить');
        $item->setAttrib('class', 'btn btn-primary');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createName());
        $this->addElement(self::createPhone());
        $this->addElement(self::createEmail());
        $this->addElement(self::createAddress());
        $this->addElement(self::createRegion());
        $this->addElement(self::createCity());
        $this->addElement(self::createZip());
        $this->addElement(self::createComment());
        $this->addElement(self::createSubmit());
        $this->setDecorators(self::$_formDecorator);
    }
}