<?php
class Catalog_Form_Order_Status extends usEngine_Form_Form
{    
    private static function createStatus()
    {
        $item = new Zend_Form_Element_Select('status');
        $item->setLabel('Статус');
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->addMultiOption(0, 'В обработке');
        $item->addMultiOption(1, 'Принят');
        $item->addMultiOption(2, 'Выполнен');
        $item->addMultiOption(3, 'Отменен');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createStatus());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->setDecorators(self::$_formDecorator);
    }
}