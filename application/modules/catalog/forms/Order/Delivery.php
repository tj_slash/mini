<?php
class Catalog_Form_Order_Delivery extends usEngine_Form_Form
{
    public static function getMethods() 
    {
        return array('Самовывоз', 'Курьером по Благовещенску', 'СДЭК по России');
    }
    
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('Label', array('separator' => ' ', 'escape' => false, 'class' => 'sr-only')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    
    protected static $_buttonDecorator = array(
        'ViewHelper'
    );
    
    private static function createDelivery()
    {
        $item = new Zend_Form_Element_Radio('delivery');
        $methods = self::getMethods();
        foreach($methods as $key => $method) {
            $item->addMultiOption($key, $method);
        }
        $item->setDecorators(self::$_elementDecorator);
        $item->removeDecorator('Label');
        $item->setValue(1);
        return $item;
    }
    
    protected static function createSubmit()
    {
        $item = new Zend_Form_Element_Submit('delivery_submit');
        $item->setLabel('Сохранить');
        $item->setAttrib('class', 'btn btn-primary');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createDelivery());
        $this->addElement(self::createSubmit());
        $this->setDecorators(self::$_formDecorator);
    }
}