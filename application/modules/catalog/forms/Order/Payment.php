<?php
class Catalog_Form_Order_Payment extends usEngine_Form_Form
{
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('Label', array('separator' => ' ', 'escape' => false, 'class' => 'sr-only')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    
    protected static $_buttonDecorator = array(
        'ViewHelper'
    );
    
    private static function createPayment()
    {
        $item = new Zend_Form_Element_Radio('payment');
        $item->addMultiOption(1, 'Через сервис Робокасса');
        $item->addMultiOption(2, 'Курьеру при получении');
        $item->addMultiOption(3, 'Наличными в офисе');
        $item->setDecorators(self::$_elementDecorator);
        $item->removeDecorator('Label');
        $item->setValue(1);
        return $item;
    }
    
    protected static function createSubmit()
    {
        $item = new Zend_Form_Element_Submit('payment_submit');
        $item->setLabel('Сохранить');
        $item->setAttrib('class', 'btn btn-primary');
        $item->setDecorators(self::$_buttonDecorator);
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createPayment());
        $this->addElement(self::createSubmit());
        $this->setDecorators(self::$_formDecorator);
    }
}