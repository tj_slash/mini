<?php
class Catalog_View_Helper_PrintTreeCategories extends Zend_View_Helper_Abstract
{    
    public static function printTreeCategories(Catalog_Model_Category $category, $level = 0)
    {
        echo "<li>";
        echo "<i class='icon-angle-right'></i>";
        echo "<a href='/catalog/{$category->alias}' title='{$category->title}'>{$category->title}</a>";
        if (($children = $category->Child->getIterator()) && $children->count() > 0) {
            echo "<ul class='categories-submenu'>";
            $level++;
            while (($child = $children->current())) {
                Catalog_View_Helper_PrintTreeCategories::printTreeCategories($child, $level);
                $children->next();
            }
            echo "</ul>";
	}
        echo "</li>\n";
    }
}