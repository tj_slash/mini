<?php
class Comment_Form_Comment_Add extends usEngine_Form_Form
{
    private static function createTypeId()
    {
        $item = new Zend_Form_Element_Select('type_id');
        $item->setLabel('Тип');
        $item->setDescription('Тип комментария');
        $types = Doctrine_Core::getTable('Comment_Model_Type')->findAll();
        if(count($types) > 0) {
            foreach($types as $type) {
                $item->addMultiOption($type->id, $type->title);
            }
        }
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('disabled', 'disabled');
        return $item;
    }
    
    private static function createContentId()
    {
        $item = new Zend_Form_Element_Text('content_id');
        $item->setLabel('ID контента');
        $item->setDescription('ID контента комментария');
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        $item->setAttrib('disabled', 'disabled');
        return $item;
    }
    
    private static function createName()
    {
        $item = new Zend_Form_Element_Text('name');
        $item->setLabel('Имя');
        $item->setDescription('Имя комментатора');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setLabel('E-mail');
        $item->setDescription('E-mail комментатора');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createPhone()
    {
        $item = new Zend_Form_Element_Text('phone');
        $item->setLabel('Телефон');
        $item->setDescription('Телефон комментатора');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createComment()
    {
        $item = new Zend_Form_Element_Textarea('comment');
        $item->setLabel('Текст');
        $item->setDescription('Текст комментария');
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        $item->setAttrib('rows', 4);
        return $item;
    }
    
    private static function createUserId()
    {
        $item = new Zend_Form_Element_Text('user_ip');
        $item->setLabel('IP');
        $item->setDescription('IP комментатора');
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        $item->setAttrib('disabled', 'disabled');
        return $item;
    }
    
    private static function createUserAgent()
    {
        $item = new Zend_Form_Element_Textarea('user_agent');
        $item->setLabel('User-agent');
        $item->setDescription('User-agent комментатора');
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('disabled', 'disabled');
        $item->setAttrib('class', 'full');
        $item->setAttrib('rows', 4);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createTypeId());
        $this->addElement(self::createContentId());
        $this->addElement(self::createName());
        $this->addElement(self::createEmail());
        $this->addElement(self::createPhone());
        $this->addElement(self::createComment());
        $this->addElement(self::createUserId());
        $this->addElement(self::createUserAgent());
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        return $formdata;
    }
    
    public function postSave($news)
    {
        
    }
    
    public function preSave()
    {
        $data = array();
        return $data;
    }
}
