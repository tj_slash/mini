<?php
class Comment_Form_Comment_New extends Zend_Form
{
    protected static $_elementDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('HtmlTag', array('tag' => 'div', 'class' => 'col-xs-12 col-sm-12 col-md-9')),
        array('Label', array('separator' => ' ', 'escape' => false, 'class' => 'col-xs-12 col-sm-3', 'placement' => 'append')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );

    protected static $_textDecorator = array(
        'ViewHelper',
        array('Errors', array('escape' => false, 'tag'=>'div', 'style'=>'color:#f00', 'escape' => false)),
        array('HtmlTag', array('tag' => 'div', 'class' => 'col-xs-12 col-sm-12')),
        array(array('elementDiv' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group'))
    );
    
    private static function createName()
    {
        $item = new Zend_Form_Element_Text('name');
        $item->setLabel('Имя');
        $item->setRequired(true);
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim);
        $item->setAttrib('class', 'form-control');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createEmail()
    {
        $item = new Zend_Form_Element_Text('email');
        $item->setLabel('E-mail');
        $item->setRequired(true);
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim);
        $item->setAttrib('class', 'form-control');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createPhone()
    {
        $item = new Zend_Form_Element_Text('phone');
        $item->setLabel('Телефон');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim);
        $item->setAttrib('class', 'form-control');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createComment()
    {
        $item = new Zend_Form_Element_Textarea('comment');
        $item->addValidator(new Zend_Validate_StringLength(0, 4096, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim);
        $item->setAttrib('class', 'form-control');
        $item->setDecorators(self::$_textDecorator);
        return $item;
    }
    
    private static function createSubmit()
    {
        $item = new Zend_Form_Element_Submit('submit');
        $item->setLabel('Комментировать');
        $item->setAttrib('class', 'btn btn-primary');
        $item->setDecorators(self::$_textDecorator);
        return $item;
    }
    
    private static function createType()
    {
        $item = new Zend_Form_Element_Hidden('type', array('disableLoadDefaultDecorators' => true));
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim);
        $item->setDecorators(array('ViewHelper'));
        return $item;
    }
    
    private static function createContentId()
    {
        $item = new Zend_Form_Element_Hidden('content_id', array('disableLoadDefaultDecorators' => true));
        $item->addValidator(new Zend_Validate_StringLength(0, 11, 'UTF-8'));
        $item->addValidator(new Zend_Validate_Digits());
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim);
        $item->setDecorators(array('ViewHelper'));
        return $item;
    }
    
    public function init()
    {
        $this->setAction('/comment/comment/widget-form');
        $this->setMethod('POST');
        $this->addElement(self::createName());
        $this->addElement(self::createEmail());
        $this->addElement(self::createPhone());
        $this->addElement(self::createComment());
        $this->addElement(self::createSubmit());
        $this->addElement(self::createType());
        $this->addElement(self::createContentId());
        $this->setAttrib('class', 'form-horizontal contact');
    }
}