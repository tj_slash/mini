<?php
class Comment_Form_Type_Add extends usEngine_Form_Form
{
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок типа комментариев');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAlias()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Alias');
        $item->setDescription('Алиас типа комментариев');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $regex = new Zend_Validate_Regex('/^[\-a-zA-Z0-9]+$/u');
        $item->addValidator($regex);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(false);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createTitle());
        $this->addElement(self::createAlias());
        parent::initForm();
    }
}
