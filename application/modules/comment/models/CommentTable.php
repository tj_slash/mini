<?php
class Comment_Model_CommentTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Comment_Model_Comment');
    }
    
    public function getPaginator($type_alias, $content_id)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $page = $request->getParam('page', 1);
        $limit = 10;
        
        if (!empty($content_id) && !empty($type_alias)) {
            $query = Doctrine_Query::create()
                ->from('Comment_Model_Comment AS comment')
                ->leftJoin('comment.Type AS type')
                ->addWhere('comment.publish = ?', true)
                ->addWhere('comment.archive = ?', false)
                ->addWhere('comment.parent_id IS NULL')
                ->addWhere('type.publish = ?', true)
                ->addWhere('type.archive = ?', false)
                ->addWhere('comment.content_id = ?', $content_id)
                ->addWhere('type.alias = ?', $type_alias)
                ->orderBy('comment.created DESC');
            $adapter = new ZFDoctrine_Paginator_Adapter_DoctrineQuery($query);
            $paginator = new Zend_Paginator($adapter);
            $paginator->setDefaultItemCountPerPage($limit);
            $paginator->setCurrentPageNumber($page);        
            return $paginator;
        }
        return false;        
    }
}