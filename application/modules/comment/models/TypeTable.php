<?php
class Comment_Model_TypeTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Comment_Model_Type');
    }
}