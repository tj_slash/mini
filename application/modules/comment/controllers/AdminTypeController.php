<?php
class Comment_AdminTypeController extends usEngine_Controller_Action
{    
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');        
        $this->_model = Comment_Model_TypeTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Типы комментариев');
            $grid->removeToolbars(array('list-archive', 'delete'));
        } else {
            $grid->setCaption('Архив типов комментариев');
            $grid->removeToolbars(array('list', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','title'=>'Наименование','alias'=>'Алиас','publish'=>'Публикация'));
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new Comment_Form_Type_Add());
    }
}
