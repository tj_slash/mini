<?php
class Comment_AdminCommentController extends usEngine_Controller_Action
{    
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Comment_Model_CommentTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Список комментариев');
            $grid->removeToolbars(array('list', 'delete'));
        } else {
            $grid->setCaption('Архив комментариев');
            $grid->removeToolbars(array('list-archive', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','comment'=>'Текст','created'=>'Дата создания','publish'=>'Публикация'));
        $grid->setFormat('comment', 'usTable_Format_Substring');
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new Comment_Form_Comment_Add());
    }
}
