<?php
class Comment_CommentController extends usEngine_Controller_Action
{
    public function init()
    {
        
    }
    
    public function widgetListAction()
    {
        $type_alias = $this->getRequest()->getParam('type', false);
        $content_id = $this->getRequest()->getParam('content_id', false);        
        $paginator = Comment_Model_CommentTable::getInstance()->getPaginator($type_alias, $content_id);
        if ($paginator) {
            $this->view->paginator = $paginator;
        }
    }
    
    public function widgetFormAction()
    {
        $form = new Comment_Form_Comment_New();
        $formdata = $this->getRequest()->getParams();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($formdata)) {
                $data = $form->getValues();
                $data['type_id'] = Comment_Model_TypeTable::getInstance()->findOneByAlias($data['type'])->id;
                $data['publish'] = true;
                $data['user_ip'] = $this->getRequest()->getServer('REMOTE_ADDR');
                $data['user_agent'] = $this->getRequest()->getServer('HTTP_USER_AGENT');
                Comment_Model_CommentTable::getInstance()->add($data);
                $this->_redirect($this->getRequest()->getServer('HTTP_REFERER'));
            }
            usEngine_Message::getInstance()->setMessage('error', 'При произошла ошибка!');
        }
        $form->populate($formdata);
        $this->view->form = $form;
    }
}