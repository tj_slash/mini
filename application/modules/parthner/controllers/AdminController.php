<?php
class Parthner_AdminController extends usEngine_Controller_Action
{
    public function init()
    {
        $this->_model = Parthner_Model_ParthnerTable::getInstance();
        $this->_helper->layout->setLayout('admin_content');
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Партнеры');
            $grid->removeToolbars(array('list-archive', 'delete'));
        } else {
            $grid->setCaption('Архив партнеров');
            $grid->removeToolbars(array('list', 'archive'));
        }
        $grid->setColumns(array('id'=>'ID','image'=>'Изображение','title'=>'Наименование','created'=>'Дата создания','publish'=>'Публикация'));
        $grid->setFormat('image', array($this, 'formatImage'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public function formatImage($row, $key)
    {
        return "<img src='/media/parthners/{$row->$key}' height='40' />";
    }
    
    public function addAction()
    {
        $this->formAction(new Parthner_Form_Parthner_Add());
    }
}