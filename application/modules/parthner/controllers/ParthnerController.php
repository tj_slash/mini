<?php
class Parthner_ParthnerController extends usEngine_Controller_Action
{    
    public function widgetAction()
    {
        $parthners = Parthner_Model_ParthnerTable::getInstance()->getForWidget();
        $this->view->parthners = $parthners;
    }
}
