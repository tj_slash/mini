<?php
class Parthner_Model_ParthnerTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Parthner_Model_Parthner');
    }
    
    /**
     * @todo Ordering
     * 
     * @return Doctrine_Collection
     */
    public function getForWidget()
    {
        $query = Doctrine_Query::create()
            ->from('Parthner_Model_Parthner')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->execute();
        return $query;
    }
}