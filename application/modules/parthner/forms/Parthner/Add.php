<?php
class Parthner_Form_Parthner_Add extends usEngine_Form_Form
{
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Title');
        $item->setDescription('Заголовок статичной страницы');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createLink()
    {
        $item = new Zend_Form_Element_Text('link');
        $item->setLabel('Ссылка');
        $item->setDescription('Ссылка на сайт партнера');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createImage()
    {
        $item = new Zend_Form_Element_File('image');
        $item->setRequired(true);
        $item->addValidator('Size', false, 1024*1024);
        $item->addValidator('Extension', false, 'jpeg,jpg,png,gif');
        $item->setDecorators(self::$_fileElementDecorator);
        $item->setDescription('Изображение партнера (JPG, PNG, GIF, не более 1MB)');
        $item->setLabel('Изображение');
        return $item;
    }

    public function init()
    {
        $this->setMethod('post')->setAttrib('enctype', 'multipart/form-data');
        $this->addElement(self::createTitle());        
        $this->addElement(self::createLink());        
        $this->addElement(self::createImage());        
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['image'])) {
            $image = $formdata['image'];
            $imageElement = $this->getElement('image');
            $imageElement->setDescription("<img src='/media/parthners/list_{$image}' style='max-width:400px'/>");
            $imageElement->setRequired(false);
        }
    }
    
    public function preSave()
    {
        $data = array();
        $file = $this->image->getFileInfo();
        if (!empty($file['image']['name']) > 0) {
            $ext = explode(".", $file['image']['name']);
            $newName = uniqid() . '.' . $ext[count($ext)-1];
            
            $dest = APPLICATION_PATH . '/../public/media/parthners/';
            
            $this->image->addFilter('Rename', $dest . $newName);
            $this->image->receive();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(250)
                ->setHeight(75)
                ->setPath($dest)
                ->setPrefix('list_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            $imagick = new usEngine_Image_Imagick($dest . $newName);
            $imagick->setWidth(80)
                ->setHeight(80)
                ->setPath($dest)
                ->setPrefix('thumb_')
                ->setName($newName)
                ->setExtension('')
                ->resizeImage()
                ->saveImage();
            
            $data['image'] = $newName;
        }
        return $data;
    }
}