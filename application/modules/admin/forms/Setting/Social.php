<?php
class Admin_Form_Setting_Social extends usEngine_Form_Setting
{
    const MODULE_NAME = 'index';
    
    protected static $fields = array(
        'code_like'
    );

    private static function createCodeLike()
    {
        $item = new Zend_Form_Element_Textarea('code_like');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Код "Мне нравится"');
        $item->setDescription('Код кнопок "Мне нравится" (AddThis и т.д.)');
        $item->addValidator(new Zend_Validate_StringLength(0, 4096, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        $item->setAttrib('style', 'height:100px');
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createCodeLike());
        $this->addElement(self::createSubmitInput());
        $this->setDecorators(self::$_formDecorator);
        parent::init();
    }
}