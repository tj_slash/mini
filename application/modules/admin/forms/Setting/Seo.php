<?php
class Admin_Form_Setting_Seo extends usEngine_Form_Setting
{
    const MODULE_NAME = 'index';
    
    protected static $fields = array(
        'meta_title',
        'meta_desc',
        'meta_keys',
        'code_counter'
    );
    
    private static function createMetaTitle()
    {
        $item = new Zend_Form_Element_Text('meta_title');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Мета заголовок');
        $item->setDescription('Мета заголовок для сайта');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createMetaDescription()
    {
        $item = new Zend_Form_Element_Textarea('meta_desc');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Мета описание');
        $item->setDescription('Мета описание для сайта');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        $item->setAttrib('style', 'height:100px');
        return $item;
    }
    
    private static function createMetaKeywords()
    {
        $item = new Zend_Form_Element_Text('meta_keys');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Мета ключевые слова');
        $item->setDescription('Мета ключевые слова для сайта');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createCodeCounter()
    {
        $item = new Zend_Form_Element_Textarea('code_counter');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Код счетчика');
        $item->setDescription('Коды счетчиков посещений (Google Analytics, LiveInternet и т.д.)');
        $item->addValidator(new Zend_Validate_StringLength(0, 4096, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addFilter(new Zend_Filter_StripTags(array(
            'allowTags' => 'script'
        )));
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        $item->setAttrib('style', 'height:100px');
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createMetaTitle());
        $this->addElement(self::createMetaDescription());
        $this->addElement(self::createMetaKeywords());
        $this->addElement(self::createCodeCounter());
        $this->addElement(self::createSubmitInput());
        $this->setDecorators(self::$_formDecorator);
        parent::init();
    }
}