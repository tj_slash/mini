<?php
class Admin_Form_Setting_Main extends usEngine_Form_Setting
{
    /**
     * Название модуля для опций 
     */
    const MODULE_NAME = 'index';
    
    /**
     * Алиасы опций (используется для метода populate)
     * 
     * @var array 
     */
    protected static $fields = array(
        'site_name',
        'logotype',
        'favicon'
    );
    
    private static function createSiteName()
    {
        $item = new Zend_Form_Element_Text('site_name');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Название сайта');
        $item->setDescription('Введите название сайта');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createVkApiId()
    {
        $item = new Zend_Form_Element_Text('vk_api_id');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('API ID ВКонтакте');
        $item->setDescription('API ID приложения ВКонтакте');
        $item->addValidator(new Zend_Validate_Int());
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }

    private static function createLogo()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $item = new usEngine_Form_Element_UploadImage('logotype');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Логотип');
        $item->setDescription('Логотип для сайта (170x30px)');
        $item->setRequired(true);
        $item->setAttrib('path', APPLICATION_PATH . '/../public/media/images/');
        $item->setAttrib('url', $view->baseUrl('/media/images/'));
        $item->setAttrib('style', 'width:170px;height:30px');
        $item->setDecorators(self::$_elementDecorator);
        $options = Zend_Registry::get('options');
        $item->setValue($options['setting']['index']['logotype']);
        return $item;
    }

    private static function createFavicon()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $item = new usEngine_Form_Element_UploadImage('favicon');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Favicon');
        $item->setDescription('Favicon для сайта');
        $item->setRequired(true);
        $item->setAttrib('path', APPLICATION_PATH . '/../public/media/images/');
        $item->setAttrib('url', $view->baseUrl('/media/images/'));
        $item->setAttrib('style', 'width:50px;height:50px');
        $item->setDecorators(self::$_elementDecorator);
        $options = Zend_Registry::get('options');
        $item->setValue($options['setting']['index']['favicon']);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createSiteName());
        $this->addElement(self::createVkApiId());
        $this->addElement(self::createLogo());
        $this->addElement(self::createFavicon());
        $this->addElement(self::createSubmitInput());
        $this->setDecorators(self::$_formDecorator);
        parent::init();
    }
}