<?php
class Admin_IndexController extends usEngine_Controller_Action
{
    public function indexAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) $this->_redirect('/admin/login');
        $this->_redirect('/admin');
    }
    
    public function dashboardAction()
    {
        if (!Zend_Auth::getInstance()->hasIdentity()) $this->_redirect('/admin/login');
        $this->_helper->layout->setLayout('admin_content');
    }
    
    public function loginAction()
    {
        $form = new User_Form_Signin();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {
                $data = $form->getValues();
                $result = User_Model_UserTable::getInstance()->authentication($data);
                if ($result->isValid()) {
                    $this->_redirect('/admin');
                } else {
                    $messages = $result->getMessages();
                    usEngine_Message::getInstance()->setMessage('error', implode(';', $messages));
                }
            }
        }
        $this->_helper->layout->setLayout('admin_layout');
        $this->view->form = $form;
        $this->view->headTitle('Войти в административную панель сайта');
        $options = Zend_Registry::get('options');
        $this->view->logotype = $this->view->baseUrl($options['setting']['index']['logotype']);
        $this->view->site_name = $options['setting']['index']['site_name'];
    }
    
    public function menuAction()
    {
        
    }
    
    public function headerAction()
    {
        
    }
    
    public function settingsAction()
    {
        $this->formSetting(new Admin_Form_Setting_Main());
        $this->_helper->layout->setLayout('admin_content');
    }
    
    public function settingsSeoAction()
    {
        $this->formSetting(new Admin_Form_Setting_Seo());
        $this->_helper->layout->setLayout('admin_content');
    }
    
    public function settingsSocialAction()
    {
        $this->formSetting(new Admin_Form_Setting_Social());
        $this->_helper->layout->setLayout('admin_content');
    }
    
    public function mceFileUploadAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $link = false;
        $upload_dir = $this->_getParam('upload_dir', 'default');
        if ($this->getRequest()->isPost()) {
            if (isset($_FILES['img_file']['name'])) {
                $file = $_FILES['img_file'];
                $ext = explode('.', $file['name']);
                $name = md5($file['name'] . time()) . '.' . end($ext);
                $media_path = 'media/mce_upload/' . $upload_dir . '/';
                $upload_dir = APPLICATION_PATH . '/../public/' . $media_path;
                if (move_uploaded_file($file['tmp_name'], $upload_dir .  $name))
                    $link = 'http://' . $_SERVER['SERVER_NAME'] . $this->view->baseUrl() . '/' . $media_path . $name;
            }
        }
        if ($link)
            echo Zend_Json::encode(array('status' => 'UPLOADED', 'src' => $link));
        else 
            echo Zend_Json::encode(array('status' => false));
    }
}