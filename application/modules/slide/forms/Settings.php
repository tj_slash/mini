<?php
/**
 * Created by JetBrains PhpStorm.
 * User: v25
 * Date: 18.10.13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
class Slide_Form_Settings extends usEngine_Form_Setting
{
    /**
     * Название модуля для опций
     */
    const MODULE_NAME = 'slide';

    /**
     * Алиасы опций (используется для метода populate)
     *
     * @var array
     */
    protected static $fields = array(
        'type'
    );

    private static function createTypeSelect()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $item = new Zend_Form_Element_Select('type');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Тип слайдера');
        $item->setDescription('Выберете тип слайдера на главной странице сайта');
        $item->setRequired(true);
        $item->setMultiOptions(array(
            '2' => 'Простой',
            '1' => 'С картинкой и двумя кнопками',
        ));
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        $alias = implode('.', array(Index_Model_OptionsTable::GLOBAL_ALIAS, self::MODULE_NAME, 'type'));
        $item->setValue(Index_Model_OptionsTable::getInstance()->getOneOption($alias));
        return $item;
    }

    public function init()
    {
        $this->addElement(self::createTypeSelect());
        $this->addElement(self::createSubmitInput());
        $this->setDecorators(self::$_formDecorator);
        parent::init();
    }
}