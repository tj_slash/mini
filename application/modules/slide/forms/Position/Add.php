<?php
class Slide_Form_Position_Add extends usEngine_Form_Form
{
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок позиции слайдов');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createAlias()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Алиас');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_Regex('/^[\-a-zA-Z0-9]+$/u', true));
        $item->setAttrib('class', 'full');
        $item->setDescription('Алиас позиции слайдов (только латинские символы)');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createWidth()
    {
        $item = new Zend_Form_Element_Text('width');
        $item->setLabel('Ширина');
        $item->addValidator(new Zend_Validate_StringLength(1, 3, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_Digits());
        $item->addValidator(new Zend_Validate_Between(array('min' => 0, 'max' => 940)));
        $item->setAttrib('class', 'full');
        $item->setDescription('Ширина слайдов в пикселях');
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createHeight()
    {
        $item = new Zend_Form_Element_Text('height');
        $item->setLabel('Высота');
        $item->addValidator(new Zend_Validate_StringLength(1, 3, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_Digits());
        $item->addValidator(new Zend_Validate_Between(array('min' => 0, 'max' => 760)));
        $item->setAttrib('class', 'full');
        $item->setDescription('Высота слайдов в пикселях');
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createTime()
    {
        $item = new Zend_Form_Element_Text('time');
        $item->setLabel('Время переключения');
        $item->addValidator(new Zend_Validate_StringLength(0, 3, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_Between(array('min' => 0, 'max' => 120)));
        $item->setAttrib('class', 'full');
        $item->setValue(0);
        $item->setDescription('Время переключения между слайдами в секундах');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createType()
    {
        $item = new Zend_Form_Element_Select('type');
        $item->setLabel('Тип');
        $item->setDescription('Тип слайдера');
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->addMultiOption('html', 'HTML код');
        $item->addMultiOption('image', 'Изображения');
        $item->setValue('image');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->setMethod('post')->setAttrib('enctype', 'multipart/form-data');
        $this->addElement(self::createTitle());
        $this->addElement(self::createAlias());
        $this->addElement(self::createWidth());
        $this->addElement(self::createHeight());
        $this->addElement(self::createTime());
        $this->addElement(self::createType());
        $this->addElement(self::createPublishChekbox());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->addElement(self::createSubmitAddInput());
        $this->setDecorators(self::$_formDecorator);
    }
}