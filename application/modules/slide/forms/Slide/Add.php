<?php
class Slide_Form_Slide_Add extends usEngine_Form_Form
{
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок слайда');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    protected static function createPositionId()
    {
        $item = new Zend_Form_Element_Select('position_id');
        $item->addValidator(new Zend_Validate_Alnum());
        $item->setRequired(true);
        $item->setLabel('Позиция');
        $item->addFilter(new Zend_Filter_StripTags);
        $positions = Slide_Model_PositionTable::getInstance()->findAll();
        if($positions){
            foreach($positions as $position) {
                $item->addMultiOption($position->id, $position->title);
            }
        }
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createDescription()
    {
        $item = new Zend_Form_Element_Textarea('description');
        $item->setLabel('Краткое описание');
        $item->setDescription('Краткое описание слайда');
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addValidator(new Zend_Validate_StringLength(0, 1024, 'UTF-8'));
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createLink()
    {
        $item = new Zend_Form_Element_Text('link');
        $item->setLabel('Ссылка');
        $item->addValidator(new Zend_Validate_StringLength(0, 255, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDescription('Ссылка для перехода');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    protected static function createImage($position = null)
    {
        $item = new Zend_Form_Element_File('image');
        
        $item->setRequired(true);
        $item->addValidator('Size', false, 1024*1024);
        $item->addValidator('Extension', false, 'jpeg,jpg,png,gif');
        $item->setDecorators(self::$_fileElementDecorator);
        $item->setDescription('Изображение (JPG, PNG, GIF, не более 1MB)');
        if (!empty($position)) {
            $item->setLabel("Изображение ({$position->width}x{$position->height})");
            $item->addValidator('ImageSize', false, array(
                'minwidth' => $position->width,
                'maxwidth' => $position->width,
                'minheight' => $position->height,
                'maxheight' => $position->height
            ));
        } else {
            $item->setLabel('Изображение');
        }
        return $item;
    }

    protected static function createHtml()
    {
        $item = new Zend_Form_Element_Textarea('html');
        $item->setLabel('HTML код');
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        $item->setDescription('HTML код слайда');
        return $item;
    }
    
    private static function createOrder()
    {
        $item = new Zend_Form_Element_Text('order');
        $item->setLabel('Очередность');
        $item->setDescription('очередность слайда');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addValidator(new Zend_Validate_Digits());
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }

    public function init()
    {
        $this->setMethod('post')->setAttrib('enctype', 'multipart/form-data');
        $this->addElement(self::createTitle());
        $this->addElement(self::createPositionId());
        $this->addElement(self::createDescription());
        $this->addElement(self::createLink());
        $this->addElement(self::createImage());
        $this->addElement(self::createHtml());
        $this->addElement(self::createOrder());
        $this->addElement(self::createPublishChekbox());
        $this->addElement(self::createSubmitInput());
        $this->addElement(self::createSubmitContinueInput());
        $this->addElement(self::createSubmitAddInput());
        $this->setDecorators(self::$_formDecorator);
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['image'])) {
            $image = $formdata['image'];
            $imageElement = $this->getElement('image');
            $imageElement->setDescription("<img src='{$image}' width='400'/>");
            $imageElement->setRequired(false);
        }
    }
    
    public function preSave()
    {
        $data = array();
        $file = $this->image->getFileInfo();
        if (!empty($file['image']['name']) > 0) {
            $ext = explode(".", $file['image']['name']);
            $newName = uniqid() . '.' . $ext[count($ext)-1];
            $this->image->addFilter('Rename', APPLICATION_PATH . '/../public/media/slides/' . $newName);
            $this->image->receive();
            $data['image'] = '/media/slides/' . $newName;
        }
        return $data;
    }
}