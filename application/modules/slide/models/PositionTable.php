<?php
class Slide_Model_PositionTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Slide_Model_Position');
    }
    
    public function getForFrontByAlias($alias)
    {
        return Doctrine_Query::create()
            ->from('Slide_Model_Position')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->addWhere('alias = ?', $alias)
            ->fetchOne();
    }
}