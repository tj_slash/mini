<?php
class Slide_Model_SlideTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Slide_Model_Slide');
    }
    
    public function getForFrontByPositionId($position_id)
    {
        $slides = Doctrine_Query::create()
            ->from('Slide_Model_Slide')
            ->addWhere('publish = ?', true)
            ->addWhere('archive = ?', false)
            ->addWhere('position_id = ?', $position_id)
            ->execute();
        return $slides;
    }
}