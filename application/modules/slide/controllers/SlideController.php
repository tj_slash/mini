<?php
class Slide_SlideController extends usEngine_Controller_Action
{    
    public function slideAction()
    {
        $alias = $this->getRequest()->getParam('alias', null);
        if (!empty($alias)) {
            $position = Slide_Model_PositionTable::getInstance()->getForFrontByAlias($alias);
            if (!empty($position)) {
                $slides = Slide_Model_SlideTable::getInstance()->getForFrontByPositionId($position->id);
                if (!empty($slides) && count($slides) > 0) {
                    $this->view->slides = $slides;
                    $this->view->position = $position;
                    switch ($position->type) {
                        case 'html':
                            $this->_helper->viewRenderer->setRender('view-html');
                            break;
                        case 'image':
                            $this->_helper->viewRenderer->setRender('view-image');
                            break;
                    }
                    if (count($slides) > 1) {
                        $time = $position->time > 0 ? $position->time * 1000 : 'false';
                        $javascript = <<<HTML
jQuery(document).ready(function($){
    var _slider = $('#{$position->id}');
    _slider.flexslider({
        animation: 'slide',
        easing: 'easeInBack',
        useCSS: false,
        animationSpeed: {$time},
        slideshow: false,
        smoothHeight: true,
        start: function(slider) {
            var controlNav = _slider.find('.flex-control-nav');
            controlNav.wrap('<div class="flex-pagination-container" />').wrap('<div class="container" />').wrap('<div class="col-xs-12 col-sm-12" />');
            controlNav.fadeIn();
            center_caption(slider);
        },
        before: function(slider) {
            _slider.find('.slides li .animation-done').each(function() {
                $(this).removeClass('animation-done');
                var animation = $(this).attr('data-animation');
                $(this).removeClass(animation);
            });
            center_caption(slider)
            $(window).trigger('resize');
        },
        after: function() {
            _slider.find('.flex-active-slide .animated').each(function() {
                var animation = $(this).attr('data-animation');
                $(this).addClass('animation-done').addClass(animation);
            });
            $(window).trigger('resize');
        }
    });
});
HTML;
                        $this->view->javascript = $javascript;
                    }
                }
            }
        }
    }
}
