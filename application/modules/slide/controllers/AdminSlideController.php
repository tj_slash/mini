<?php
class Slide_AdminSlideController extends usEngine_Controller_Action
{    
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');
        $this->_model = Slide_Model_SlideTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Слайды');
            $grid->removeToolbar('list');
            $grid->removeToolbar('delete');
        } else {
            $grid->setCaption('Архив слайдов');
            $grid->removeToolbar('list-archive');
            $grid->removeToolbar('archive');
        }
        $grid->setColumns(array('id'=>'ID','title'=>'Наименование','publish'=>'Публикация'));
        $this->view->grid = $grid;
    }

    public function addAction()
    {
        $this->formAction(new Slide_Form_Slide_Add());
    }
    
    public function settingsAction()
    {
        $form = new Slide_Form_Settings();
        if ($this->getRequest()->isPost()) {
            $formdata = $this->getRequest()->getParams();
            if($form->isValid($formdata))
            {
                Index_Model_OptionsTable::getInstance()->updateOptions($form);
                usEngine_Message::getInstance()->setMessage('success_add');
                $this->_redirect($_SERVER['HTTP_REFERER']);
            }
            usEngine_Message::getInstance()->setMessage('error', 'При сохранении опций произошла ошибка!');
            $form->populate($formdata);
        }
        $this->view->form = $form;
        $this->_helper->layout->setLayout('admin_content');
        $this->renderScript('admin/add.phtml');
    }
}
