<?php
class News_Form_Settings extends usEngine_Form_Setting
{
    /**
     * Название модуля для опций 
     */
    const MODULE_NAME = 'news';
    
    /**
     * Алиасы опций (используется для метода populate)
     * 
     * @var array 
     */
    protected static $fields = array(
        'news_post_partial',
        'news_list_partial',
        'news_list_count',
        'news_widget_popular_count',
        'news_widget_last_count',
        'news_widget_partial',
        'show_author',
        'meta_title',
        'meta_desc',
        'meta_keys',
    );
    
    private static function createNewsWidgetPartial()
    {
        $item = new Zend_Form_Element_Select('news_widget_partial');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Виджет последних новостей на главной');
        $item->setDescription('Виджет последних новостей на главной для сайта');
        $item->setRequired(true);
        $item->setMultiOptions(array(
            'news-widget-1' => 'Три новости',
            'news-widget-2' => 'Две новости + Текстовый блок',
            
        ));
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createNewsPostPartial()
    {
        $item = new Zend_Form_Element_Select('news_post_partial');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Шаблон просмотра новости');
        $item->setDescription('Шаблон просмотра новости для сайта');
        $item->setRequired(true);
        $item->setMultiOptions(array(
            'news-post-1' => 'Боковая колонка справа',
            'news-post-2' => 'Боковая колонка слева'
        ));
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createNewsListPartial()
    {
        $item = new Zend_Form_Element_Select('news_list_partial');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Шаблон просмотра списка новостей');
        $item->setDescription('Шаблон просмотра списка новостей для сайта');
        $item->setRequired(true);
        $item->setMultiOptions(array(
            'news-list-page-1' => 'Боковая колонка справа, большие превью',
            'news-list-page-2' => 'Боковая колонка слева, большие превью',
            'news-list-page-3' => 'Боковая колонка справа, маленькие превью',
            'news-list-page-4' => 'Боковая колонка слева, маленькие превью',
            'news-list-page-5' => 'Три колонки'
        ));
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createNewsListCount()
    {
        $item = new Zend_Form_Element_Select('news_list_count');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Количество новостей');
        $item->setDescription('Количество новостей в списке');
        $item->setRequired(true);
        $item->setMultiOptions(array(
            6 => 6,
            9 => 9,
            12 => 12,
            15 => 15
        ));
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createNewsWidgetPopularCount()
    {
        $item = new Zend_Form_Element_Text('news_widget_popular_count');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Количество популярных новостей');
        $item->setDescription('Количество популярных новостей в боковой колонке');
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->addValidator(new Zend_Validate_StringLength(1, 2, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->addValidator(new Zend_Validate_Digits());
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createNewsWidgetLastCount()
    {
        $item = new Zend_Form_Element_Text('news_widget_last_count');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Количество последних новостей');
        $item->setDescription('Количество последних новостей в боковой колонке');
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->addValidator(new Zend_Validate_StringLength(1, 2, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->addValidator(new Zend_Validate_Digits());
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createMetaTitle()
    {
        $item = new Zend_Form_Element_Text('meta_title');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Мета заголовок');
        $item->setDescription('Мета заголовок для списка новостей сайта');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createMetaDescription()
    {
        $item = new Zend_Form_Element_Textarea('meta_desc');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Мета описание');
        $item->setDescription('Мета описание для списка новостей сайта');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        $item->setAttrib('style', 'height:100px');
        return $item;
    }
    
    private static function createMetaKeywords()
    {
        $item = new Zend_Form_Element_Text('meta_keys');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Мета ключевые слова');
        $item->setDescription('Мета ключевые слова для списка новостей сайта');
        $item->addValidator(new Zend_Validate_StringLength(2, 1024, 'UTF-8'));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->addFilter(new Zend_Filter_StringTrim());
        $item->addValidator(new Zend_Validate_NotEmpty());
        $item->setRequired(true);
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    private static function createShowAuthor()
    {
        $item = new Zend_Form_Element_Checkbox('show_author');
        $item->setBelongsTo(self::ELS_NAME);
        $item->setLabel('Отображать автора');
        $item->setDescription('Отображать автора новости');
        $item->setDecorators(self::$_elementDecorator);
        $item->setAttrib('class', 'full');
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createNewsWidgetPartial());
        $this->addElement(self::createNewsPostPartial());
        $this->addElement(self::createNewsListPartial());
        $this->addElement(self::createNewsListCount());
        $this->addElement(self::createNewsWidgetPopularCount());
        $this->addElement(self::createNewsWidgetLastCount());
        $this->addElement(self::createShowAuthor());
        $this->addElement(self::createMetaTitle());
        $this->addElement(self::createMetaDescription());
        $this->addElement(self::createMetaKeywords());
        $this->addElement(self::createSubmitInput());
        $this->setDecorators(self::$_formDecorator);
        parent::init();
    }
}