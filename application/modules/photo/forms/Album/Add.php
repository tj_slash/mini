<?php
class Photo_Form_Album_Add extends usEngine_Form_Form
{
    private static function createTitle()
    {
        $item = new Zend_Form_Element_Text('title');
        $item->setLabel('Заголовок');
        $item->setDescription('Заголовок фотоальбома');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createAlias()
    {
        $item = new Zend_Form_Element_Text('alias');
        $item->setLabel('Alias');
        $item->setDescription('Алиас фотоальбома');
        $item->addValidator(new Zend_Validate_StringLength(0, 255));
        $regex = new Zend_Validate_Regex('/^[\-a-zA-Z0-9]+$/u');
        $item->addValidator($regex);
        $item->addFilter(new Zend_Filter_StripTags);
        $item->setRequired(false);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createDescription()
    {
        $item = new Zend_Form_Element_Textarea('description');
        $item->setLabel('Описание');
        $item->setDescription('Краткое описание фотоальбома');
        $item->setRequired(true);
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    private static function createImages()
    {
        $item = new usEngine_Form_Element_Uploader('image');
        $item->setOptions(array(
            'upload' => '/photo/admin-image/upload',
            'delete' => '/photo/admin-image/delete',
            'main' => '/photo/admin-image/main',
            'path' => '/media/photo/thumb_',
            'images' => false
        ));
        $item->setLabel('Фотографии');
        $item->setDescription('Фотографии фотоальбома');
        $item->setAttrib('class', 'full');
        $item->setDecorators(self::$_elementDecorator);
        return $item;
    }
    
    public function init()
    {
        $this->addElement(self::createTitle());
        $this->addElement(self::createAlias());
        $this->addElement(self::createDescription());
        $this->addElement(self::createImages());
        parent::initMeta();
        parent::initForm();
    }
    
    public function preRender($formdata)
    {
        if (!empty($formdata['id'])) {
            $images = Doctrine_Query::create()
                ->from('Photo_Model_Photo')
                ->where('album_id = ?', $formdata['id'])
                ->orderBy('order ASC')
                ->execute();
            if (count($images) > 0) {
                $image = $this->getElement('image');
                $image->setOptions(array('images' => $images));
            }
        }   
        return $formdata;
    }
    
    public function postSave($album)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $images = $request->getParam('image', false);
        if (!empty($images) && is_array($images) && count($images) > 0) {
            foreach($images as $order => $id) {
                Doctrine_Query::create()
                    ->update('Photo_Model_Photo')
                    ->addWhere('id = ?', $id)
                    ->set('order', $order)
                    ->set('album_id', $album->id)
                    ->execute();
            }
        }
    }
}
