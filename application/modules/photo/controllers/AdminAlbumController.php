<?php
class Photo_AdminAlbumController extends usEngine_Controller_Action
{    
    public function init()
    {
        $this->_helper->layout->setLayout('admin_content');        
        $this->_model = Photo_Model_AlbumTable::getInstance();
    }
    
    public function listAction()
    {
        $archive = $this->getRequest()->getParam('archive', false);
        $grid = new usTable_Table($this->_model->adminListing()->where('archive = ?', $archive));
        if (!$archive) {
            $grid->setCaption('Фотоальбомы');
            $grid->removeToolbars(array('list-archive', 'delete'));
        } else {
            $grid->setCaption('Архив фотоальбомов');
            $grid->removeToolbars(array('list', 'archive'));
        }
        $grid->setColumns(array(
            'id'=>'ID',
            'image'=>'Изображение',
            'title'=>'Наименование',
            'alias'=>'Алиас',
            'created'=>'Дата создания',
            'publish'=>'Публикация'
        ));
        $grid->setFormat('alias', array($this, 'formatAlias'));
        $grid->setFormat('image', array($this, 'formatImage'));
        $grid->setFormat('created', 'usTable_Format_Date');
        $this->view->grid = $grid;
    }
    
    public function addAction()
    {
        $this->formAction(new Photo_Form_Album_Add());
    }
    
    public static function formatAlias($row, $key)
    {
        return "<a href='/photo/album/{$row->$key}' target='_blank'>{$row->$key}</a>";
    }
    
    public static function formatImage($row)
    {
        return "<img src='/media/photo/thumb_{$row->image}' width=40 height=40>";
    }
}