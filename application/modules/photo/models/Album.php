<?php
class Photo_Model_Album extends Photo_Model_Base_Album
{
    protected $_currentImage = null;
    
    public function getImage()
    {
        if (!$this->_currentImage) {
            $image = Doctrine_Query::create()
                ->from('Photo_Model_Photo')
                ->addWhere('album_id = ?', $this->id)
                ->orderBy('order ASC')
                ->fetchOne();
            if (!empty($image)) $image = $image->image;
            else $image = 'default.png';
            $this->_currentImage = $image;
        }
        return $this->_currentImage;
    }
}