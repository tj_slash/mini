<?php
class Photo_Model_PhotoTable extends usEngine_Doctrine_Table
{
    public static function getInstance()
    {
        return Doctrine_Core::getTable('Photo_Model_Photo');
    }
}